#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Tuning {
    /// Max speed the tee can get on ground
    pub ground_control_speed: f32,
    /// Acceleration speed on the ground
    pub ground_control_accel: f32,
    /// Friction on the ground
    pub ground_friction: f32,
    /// Impulse when jumping on ground
    pub ground_jump_impulse: f32,
    /// Impulse when jumping in air
    pub air_jump_impulse: f32,
    /// Max speed the tee can get in the air
    pub air_control_speed: f32,
    /// Acceleration speed in air
    pub air_control_accel: f32,
    /// Friction in the air
    pub air_friction: f32,
    /// Length of the hook
    pub hook_length: f32,
    /// How fast the hook is fired
    pub hook_fire_speed: f32,
    /// Acceleration when hook is stuck
    pub hook_drag_accel: f32,
    /// Drag speed of the hook
    pub hook_drag_speed: f32,
    /// Gravity of the teeworld
    pub gravity: f32,
    /// Velocity ramp start
    pub velramp_start: f32,
    /// Velocity ramp range
    pub velramp_range: f32,
    /// Velocity ramp curvature
    pub velramp_curvature: f32,
    /// Gun curvature
    pub gun_curvature: f32,
    /// Gun speed
    pub gun_speed: f32,
    /// Gun lifetime
    pub gun_lifetime: f32,
    /// Shotgun curvature
    pub shotgun_curvature: f32,
    /// Shotgun speed
    pub shotgun_speed: f32,
    /// (UNUSED) Speed difference between shotgun bullets
    pub shotgun_speeddiff: f32,
    /// (UNUSED) Shotgun lifetime
    pub shotgun_lifetime: f32,
    /// Grenade curvature
    pub grenade_curvature: f32,
    /// Grenade speed
    pub grenade_speed: f32,
    /// Grenade lifetime
    pub grenade_lifetime: f32,
    /// How long the laser can reach
    pub laser_reach: f32,
    /// When bouncing
    pub laser_bounce_delay: f32,
    /// How many times the laser can bounce
    pub laser_bounce_num: f32,
    /// Remove this much from reach when laser is bouncing
    pub laser_bounce_cost: f32,
    /// (UNUSED) Laser damage
    pub laser_damage: f32,
    /// Enable player collisions
    pub player_collision: f32,
    /// Enable player vs player hooking
    pub player_hooking: f32,
    /// Jetpack pistol strength
    pub jetpack_strength: f32,
    /// Shotgun pull strength
    pub shotgun_strength: f32,
    /// Explosion strength (grenade for example)
    pub explosion_strength: f32,
    /// Hammer strength
    pub hammer_strength: f32,
    /// Hook duration
    pub hook_duration: f32,
    /// Delay of hammering (when hitting nothing)
    pub hammer_fire_delay: f32,
    /// Delay of firing gun
    pub gun_fire_delay: f32,
    /// Delay of firing shotgun
    pub shotgun_fire_delay: f32,
    /// Delay of firing grenade
    pub grenade_fire_delay: f32,
    /// Delay of firing laser laser
    pub laser_fire_delay: f32,
    /// Delay of firing ninja
    pub ninja_fire_delay: f32,
    /// Delay of hammering (when hitting another tee)
    pub hammer_hit_fire_delay: f32,
}

impl Tuning {
    pub(crate) fn new() -> Self {
        Self {
            ground_control_speed: 10.0,
            ground_control_accel: 2.0,
            ground_friction: 0.5,
            ground_jump_impulse: 13.2,
            air_jump_impulse: 12.0,
            air_control_speed: 5.0,
            air_control_accel: 1.5,
            air_friction: 0.95,
            hook_length: 380.0,
            hook_fire_speed: 80.0,
            hook_drag_accel: 3.0,
            hook_drag_speed: 15.0,
            gravity: 0.5,
            velramp_start: 550.0,
            velramp_range: 2000.0,
            velramp_curvature: 1.4,
            gun_curvature: 1.25,
            gun_speed: 2200.0,
            gun_lifetime: 2.0,
            shotgun_curvature: 1.25,
            shotgun_speed: 2750.0,
            shotgun_speeddiff: 0.8,
            shotgun_lifetime: 0.20,
            grenade_curvature: 7.0,
            grenade_speed: 1000.0,
            grenade_lifetime: 2.0,
            laser_reach: 800.0,
            laser_bounce_delay: 150.0,
            laser_bounce_num: 1000.0,
            laser_bounce_cost: 0.0,
            laser_damage: 5.0,
            player_collision: 1.0,
            player_hooking: 1.0,
            jetpack_strength: 400.0,
            shotgun_strength: 10.0,
            explosion_strength: 6.0,
            hammer_strength: 1.0,
            hook_duration: 1.25,
            hammer_fire_delay: 125.0,
            gun_fire_delay: 125.0,
            shotgun_fire_delay: 500.0,
            grenade_fire_delay: 500.0,
            laser_fire_delay: 800.0,
            ninja_fire_delay: 800.0,
            hammer_hit_fire_delay: 320.0,
        }
    }

    pub(crate) fn new_with_ddnet_parameters() -> Self {
        let mut new = Self::new();
        new.gun_curvature = 0.0;
        new.gun_speed = 1400.0;
        new.shotgun_curvature = 0.0;
        new.shotgun_speed = 500.0;
        new.shotgun_speeddiff = 0.0;
        new
    }

    pub(crate) fn apply_from_config(&mut self, name: &str, value: f32) -> bool {
        match name {
            "ground_control_speed" => self.ground_control_speed = value,
            "ground_control_accel" => self.ground_control_accel = value,
            "ground_friction" => self.ground_friction = value,
            "ground_jump_impulse" => self.ground_jump_impulse = value,
            "air_jump_impulse" => self.air_jump_impulse = value,
            "air_control_speed" => self.air_control_speed = value,
            "air_control_accel" => self.air_control_accel = value,
            "air_friction" => self.air_friction = value,
            "hook_length" => self.hook_length = value,
            "hook_fire_speed" => self.hook_fire_speed = value,
            "hook_drag_accel" => self.hook_drag_accel = value,
            "hook_drag_speed" => self.hook_drag_speed = value,
            "gravity" => self.gravity = value,
            "velramp_start" => self.velramp_start = value,
            "velramp_range" => self.velramp_range = value,
            "velramp_curvature" => self.velramp_curvature = value,
            "gun_curvature" => self.gun_curvature = value,
            "gun_speed" => self.gun_speed = value,
            "gun_lifetime" => self.gun_lifetime = value,
            "shotgun_curvature" => self.shotgun_curvature = value,
            "shotgun_speed" => self.shotgun_speed = value,
            "shotgun_speeddiff" => self.shotgun_speeddiff = value,
            "shotgun_lifetime" => self.shotgun_lifetime = value,
            "grenade_curvature" => self.grenade_curvature = value,
            "grenade_speed" => self.grenade_speed = value,
            "grenade_lifetime" => self.grenade_lifetime = value,
            "laser_reach" => self.laser_reach = value,
            "laser_bounce_delay" => self.laser_bounce_delay = value,
            "laser_bounce_num" => self.laser_bounce_num = value,
            "laser_bounce_cost" => self.laser_bounce_cost = value,
            "laser_damage" => self.laser_damage = value,
            "player_collision" => self.player_collision = value,
            "player_hooking" => self.player_hooking = value,
            "jetpack_strength" => self.jetpack_strength = value,
            "shotgun_strength" => self.shotgun_strength = value,
            "explosion_strength" => self.explosion_strength = value,
            "hammer_strength" => self.hammer_strength = value,
            "hook_duration" => self.hook_duration = value,
            "hammer_fire_delay" => self.hammer_fire_delay = value,
            "gun_fire_delay" => self.gun_fire_delay = value,
            "shotgun_fire_delay" => self.shotgun_fire_delay = value,
            "grenade_fire_delay" => self.grenade_fire_delay = value,
            "laser_fire_delay" => self.laser_fire_delay = value,
            "ninja_fire_delay" => self.ninja_fire_delay = value,
            "hammer_hit_fire_delay" => self.hammer_hit_fire_delay = value,
            _ => return false,
        }
        true
    }
}
