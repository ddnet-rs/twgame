use crate::core::twsnap::{time::Instant, Snap};
use crate::core::{
    console::Command,
    net_msg,
    replay::{DemoChatPtr, ReplayerChecker, ReplayerTeeInfo},
    Game, Input, Snapper,
};
use crate::{Bug, DdnetWorld, Map, ThHeader};
use log::info;
use std::collections::HashMap;
use std::rc::Rc;
use std::sync::mpsc;
use twgame_core::database::{DatabaseResult, DatabaseWrite, Finishes, SaveTeam};
use twgame_core::replay::ReplayerCheckerHelper;
use twgame_core::teehistorian::Chunk;

/// Current save/load request for a team
enum SaveRequest {
    Save(SaveTeam),
    Load,
}

pub struct DdnetReplayerWorld {
    correct: bool,
    database_request_receiver: mpsc::Receiver<DatabaseWrite>,
    database_response_sender: mpsc::Sender<DatabaseResult>,
    /// allow passing expected finishes for teehistorian files before the FinishTee and FinishTeam chunks.
    /// deprecated. Only kept due to a bunch of tests already using this.
    expected_finishes: Option<Vec<Finishes>>,
    /// Finishes with FinishTee and FinishTeam from Teehistorian file
    teehistorian_finishes: Vec<(Instant, Finishes)>,
    /// Finishes from DdnetWorld
    got_finishes: Vec<(Instant, Finishes)>,
    inner: DdnetWorld,
    save_load_request: HashMap<i32, SaveRequest>,
}

impl DdnetReplayerWorld {
    pub fn new(map: Rc<Map>, compat: bool) -> Self {
        let (request_sender, request_receiver) = mpsc::channel();
        let (response_sender, response_receiver) = mpsc::channel();
        let mut twgame = DdnetWorld::new_with_map(map, request_sender, response_receiver).unwrap();
        twgame.enable_ddnet_tele_copat_mode(compat);
        if compat {
            twgame.enable_prng_compat_data_collection();
        }
        Self {
            correct: true,
            inner: twgame,
            database_response_sender: response_sender,
            database_request_receiver: request_receiver,
            expected_finishes: None,
            teehistorian_finishes: vec![],
            got_finishes: vec![],
            save_load_request: HashMap::new(),
        }
    }
    /// optionally also verify that theself.correct writes to the database were made (finishes/saves/loads)
    ///
    /// Not necessary on newer Teehistorian files, that store that have FinishTee and FininshTeam chunks.
    /// This one doesn't assert that the fininsh completes at the correct tick.
    #[deprecated]
    pub fn with_expected_finishes(&mut self, writes: Vec<Finishes>) -> &mut Self {
        self.expected_finishes = Some(writes);
        self
    }
    pub fn is_correct(&self) -> bool {
        self.correct
    }
}

// TODO: make functions non-public on DdnetGame?, move configure_with_teehistorian_header to on_teehistorian_header
// make sure on_teehistorian_header is always called by Replayer
impl DdnetReplayerWorld {
    /// Must be called before first tick. (TODO: ensure this with type system)
    pub fn supply_prng_compat_data(&mut self, compat_data: &str) {
        self.inner.supply_prng_compat_data(compat_data);
    }

    /// Must be called after last tick. (TODO: ensure this with type system)
    pub fn retrieve_prng_compat_data(&mut self) -> Option<String> {
        self.inner.retrieve_prng_compat_data()
    }

    /// Returns used game bugs detected during run
    pub fn retrieve_bugs(&mut self) -> Vec<Bug> {
        self.inner.retrieve_bugs()
    }

    pub fn configure_with_teehistorian_parameters(&mut self, header: &ThHeader) {
        self.inner.configure_with_teehistorian_parameters(header);
    }
}

impl DdnetReplayerWorld {
    // returns whether everything was correct. False, if anything unexpected occurred
    fn process_database_queries(&mut self, now: Instant) {
        for database_query in self.database_request_receiver.try_iter() {
            info!("database_query {:?}", database_query);
            match database_query {
                DatabaseWrite::FinishTee(f) => {
                    self.got_finishes.push((now, Finishes::FinishTee(f)))
                }
                DatabaseWrite::FinishTeam(f) => {
                    self.got_finishes.push((now, Finishes::FinishTeam(f)))
                }
                DatabaseWrite::Save(team_id, save_team) => {
                    if let Some(save_load_request) = self.save_load_request.get(&team_id) {
                        match save_load_request {
                            SaveRequest::Load =>
                                info!("incorrect: GameWorld requested Save for team, while Load was in progress"),
                            SaveRequest::Save(_) =>
                                info!("incorrect: GameWorld requested Save for team, while Save was in progress"),
                        }
                    }
                    self.save_load_request
                        .insert(team_id, SaveRequest::Save(save_team));
                }
                DatabaseWrite::Load(team_id) => {
                    if let Some(save_load_request) = self.save_load_request.get(&team_id) {
                        match save_load_request {
                            SaveRequest::Load =>
                                info!("incorrect: GameWorld requested Load for team, while Load was in progress"),
                            SaveRequest::Save(_) =>
                                info!("incorrect: GameWorld requested Load for team, while Save was in progress"),
                        }
                    }
                    self.save_load_request.insert(team_id, SaveRequest::Load);
                }
            }
        }
    }
}

impl Game for DdnetReplayerWorld {
    fn player_join(&mut self, id: u32) {
        self.inner.player_join(id);
    }
    fn player_ready(&mut self, id: u32) {
        self.inner.player_ready(id);
    }
    fn player_input(&mut self, id: u32, input: &Input) {
        self.inner.player_input(id, input);
    }
    fn player_leave(&mut self, id: u32) {
        self.inner.player_leave(id);
    }

    fn on_net_msg(&mut self, id: u32, msg: &net_msg::ClNetMessage) {
        self.inner.on_net_msg(id, msg);
    }
    fn on_command(&mut self, id: u32, command: &Command) {
        self.inner.on_command(id, command);
    }

    fn swap_tees(&mut self, id1: u32, id2: u32) {
        self.inner.swap_tees(id1, id2);
    }

    fn tick(&mut self, cur_time: Instant) {
        self.inner.tick(cur_time);
    }

    fn is_empty(&self) -> bool {
        self.inner.is_empty()
    }
}
impl ReplayerChecker for DdnetReplayerWorld {
    fn on_teehistorian_header(&mut self, header: &[u8]) {
        let header = ThHeader::from_buf(header);
        if let Some(version_minor) = header.version_minor.as_ref() {
            let version_minor = version_minor.parse::<i32>().unwrap();
            if version_minor >= 8 {
                if let Some(expected_finishes) = self.expected_finishes.as_ref() {
                    // still allow to set an empty vec, because that is how the test is currently written
                    assert_eq!(
                        expected_finishes,
                        &vec![],
                        "finishes are stored in Teehistorian file, no need to pass externally"
                    );
                }
                self.expected_finishes = None;
            }
        }
        self.inner.configure_with_teehistorian_parameters(&header);
    }
    fn on_teehistorian_chunk(&mut self, now: Instant, chunk: &Chunk) {
        match chunk {
            Chunk::TeamLoadFailure { team } => {
                if let Some(SaveRequest::Load) = self.save_load_request.remove(team) {
                    if self
                        .database_response_sender
                        .send(DatabaseResult::LoadFailure(*team))
                        .is_err()
                    {
                        info!("incorrect: TeamLoadFailure: GameWorld closed DatabaseReader");
                        self.correct = false;
                    }
                } else {
                    info!("incorrect: TeamLoadFailure in Teehistorian, but GameWorld didn't request load");
                    self.correct = false;
                }
            }
            Chunk::TeamLoadSuccess(ref team_save) => {
                if let Some(SaveRequest::Load) = self.save_load_request.remove(&team_save.team) {
                    if self
                        .database_response_sender
                        .send(DatabaseResult::LoadSuccess(
                            team_save.team,
                            SaveTeam::from_buf(team_save.save),
                        ))
                        .is_err()
                    {
                        info!("incorrect: TeamLoadSuccess: GameWorld closed DatabaseReader");
                        self.correct = false;
                    }
                } else {
                    info!("incorrect: TeamLoadSuccess in Teehistorian, but GameWorld didn't request load");
                    self.correct = false;
                }
            }
            Chunk::TeamSaveFailure { team } => {
                // pass save failure to team, verify that save was requested
                if let Some(SaveRequest::Save(_)) = self.save_load_request.remove(team) {
                    if self
                        .database_response_sender
                        .send(DatabaseResult::SaveFailure(*team))
                        .is_err()
                    {
                        info!("incorrect: TeamSaveFailure: GameWorld closed DatabaseReader");
                        self.correct = false;
                    }
                } else {
                    info!("incorrect: TeamSaveFailure in Teehistorian, but GameWorld didn't request save");
                    self.correct = false;
                }
            }
            Chunk::TeamSaveSuccess(ref team_save) => {
                // pass save success to team, verify that save was requested with that exact save string
                if let Some(SaveRequest::Save(requested_save)) =
                    self.save_load_request.remove(&team_save.team)
                {
                    let teehistorian_save = SaveTeam::from_buf(team_save.save);
                    // TODO: note that before https://github.com/ddnet/ddnet/pull/9226, the save codes were cut off roughly at 2000 bytes
                    if !requested_save.compat_eq(&teehistorian_save) {
                        info!("incorrect: Save game_state differs from world={requested_save:?}, teehistorian={teehistorian_save:?}");
                        self.correct = false;
                        if log::log_enabled!(log::Level::Info) {
                            requested_save.print_diff(&teehistorian_save);
                        }
                    }
                    if self
                        .database_response_sender
                        .send(DatabaseResult::SaveSuccess(team_save.team))
                        .is_err()
                    {
                        info!("incorrect: TeamSaveSuccess: GameWorld closed DatabaseReader");
                        self.correct = false;
                    }
                } else {
                    info!("incorrect: TeamSaveSuccess in Teehistorian, but GameWorld didn't request save");
                    self.correct = false;
                }
            }
            Chunk::ConsoleCommand(_) => {
                // save/load could be initiated on consolecommand.
                // finishes could be send during the tick.
                self.process_database_queries(now);
            }
            _ => {}
        }
    }
    fn on_finish(&mut self, now: Instant, finish: &Finishes) {
        self.teehistorian_finishes.push((now, finish.clone()))
    }
    fn check_tees(
        &mut self,
        cur_time: Instant,
        tees: &[Option<ReplayerTeeInfo>],
        demo: DemoChatPtr,
    ) {
        self.correct = self.inner.check_tees(cur_time, tees, demo) && self.correct;
    }
    fn finalize(&mut self) {
        if let Some(finishes) = &self.expected_finishes {
            // this is not optimized. However, deprecated anyway so I don't care.
            let got_finishes: Vec<_> = self.got_finishes.iter().map(|(_, f)| f.clone()).collect();
            if finishes != &got_finishes {
                info!(
                    "incorrect: Unexpected database writes: world={:?}, teehistorian_compat={:?}",
                    self.got_finishes, finishes
                );
                self.correct = false;
            }
        } else if self.teehistorian_finishes != self.got_finishes {
            info!(
                "incorrect: Unexpected database writes: world={:?}, teehistorian={:?}",
                self.got_finishes, self.teehistorian_finishes,
            );
            self.correct = false;
        }
    }
}

impl Snapper for DdnetReplayerWorld {
    fn snap(&self, snapshot: &mut Snap) {
        self.inner.snap(snapshot);
    }
}
