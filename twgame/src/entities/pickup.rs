use crate::entities::tee::{Tee, TEE_PROXIMITY};
use crate::entities::MapEntity;
use crate::state::{GameState, Tees};
use twgame_core::twsnap::enums::Powerup as PickupKind;
use twgame_core::twsnap::time::Instant;
use twgame_core::twsnap::vec2_from_bits;
use twgame_core::twsnap::{self, SnapId};
use vek::Vec2;

#[derive(Debug, Clone)]
pub struct Pickup {
    snap_id: SnapId,

    // all entities have the following two (TODO: create a common struct/trait?)
    pos: Vec2<f32>,

    // pickup specific
    kind: PickupKind,
}

impl MapEntity for Pickup {
    fn tick(&mut self, now: Instant, game_state: &mut GameState, tees: &mut Tees<Tee>) {
        for tee_uid in game_state.tee_order.id_order() {
            let tee_core = game_state.tee_cores.get_tee_mut(tee_uid).unwrap();
            if tee_core.pos().distance(self.pos) < 20.0 + TEE_PROXIMITY {
                tees.get_tee_mut(tee_uid).unwrap().on_pickup(
                    now,
                    tee_core,
                    &mut game_state.events,
                    self.kind,
                );
            }
        }
    }

    fn snap(&self, snapshot: &mut twsnap::Snap) {
        let pickup = snapshot.pickups.get_mut_default(self.snap_id);
        pickup.kind = self.kind;
        pickup.pos = vec2_from_bits(self.pos);
    }
}

impl Pickup {
    pub fn new(snap_id: SnapId, pos: Vec2<f32>, kind: PickupKind) -> Pickup {
        Pickup { snap_id, pos, kind }
    }
}
