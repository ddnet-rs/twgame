use crate::entities::EntityItemNew;
use crate::ids::TeamId;
use std::cell::RefCell;
use std::rc::Rc;

#[derive(Debug)]
pub struct Entities {
    /// Keep team_id here for convenience when adding new Entities
    team_id: TeamId,
    /// Holding shared EntityList if backwards compatible EntityList implementation.
    /// We are the only owner in case of separate EntityList implementation.
    /// The related
    next_tick: Rc<RefCell<Vec<(TeamId, EntityItemNew)>>>,
}

impl Entities {
    // create an EntityList that is completly separated from other `GameState`s
    pub(super) fn new(team_id: TeamId) -> Self {
        Self {
            team_id,
            next_tick: Rc::new(RefCell::new(vec![])),
        }
    }

    // create an EntityList that can share a next_entities with another `GameState`
    pub(super) fn new_compat(
        team_id: TeamId,
        next_tick: Rc<RefCell<Vec<(TeamId, EntityItemNew)>>>,
    ) -> Self {
        Self { team_id, next_tick }
    }

    pub(super) fn clone_next_tick(&self) -> Rc<RefCell<Vec<(TeamId, EntityItemNew)>>> {
        self.next_tick.clone()
    }
}

impl Entities {
    /// allow anyone with game state to add entities to the next tick
    pub fn add_next_tick(&mut self, entity: EntityItemNew) {
        self.next_tick.borrow_mut().push((self.team_id, entity));
    }
}
