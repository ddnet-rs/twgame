use crate::map::{TeleTile, Tile};
use serde::{Deserialize, Serialize};
use std::cell::RefCell;
use std::rc::Rc;
use twgame_core::twsnap::time::Instant;
use vek::Vec2;

#[derive(Clone, Copy, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum BugKind {
    SkipTele(TeleTile, u8),
    TelePowerup(Tile),
    SkipDeath,
    KillImmunity,
    Shotgun,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Bug {
    pub time: Instant,
    pub kind: BugKind,
    pub pos: Vec2<i32>,
}

#[derive(Debug)]
pub struct Bugs {
    inner: Rc<RefCell<Vec<Bug>>>,
}

impl Bugs {
    pub(super) fn new() -> Self {
        Self {
            inner: Rc::new(RefCell::new(vec![])),
        }
    }

    pub(super) fn create_new(&self) -> Self {
        Self {
            inner: self.inner.clone(),
        }
    }
}

impl Bugs {
    pub(crate) fn push(&self, bug: Bug) {
        let mut inner = self.inner.borrow_mut();
        if let Some(last) = inner.last() {
            if last.kind == bug.kind && last.pos == bug.pos {
                return; // Deduplicate e.g. KillImmunity
            }
        }
        inner.push(bug);
    }

    pub(super) fn retrieve_bugs(&self) -> Vec<Bug> {
        (*self.inner.borrow()).clone()
    }
}
