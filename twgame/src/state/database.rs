//! A way of passing database requests from the game

use std::sync::mpsc;
use twgame_core::database::DatabaseWrite;

#[derive(Debug)]
pub struct Database {
    requests: mpsc::Sender<DatabaseWrite>,
}

impl Database {
    pub(super) fn new(sender: mpsc::Sender<DatabaseWrite>) -> Self {
        Self { requests: sender }
    }
}

impl Database {
    pub(super) fn clone_sender(&self) -> mpsc::Sender<DatabaseWrite> {
        self.requests.clone()
    }
}

impl Database {
    pub fn add(&mut self, entry: DatabaseWrite) {
        if let Err(err) = self.requests.send(entry) {
            // TODO: handle error better
            println!("Database sender closed {:?}", err);
        }
    }
}
