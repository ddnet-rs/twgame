use twgame_core::twsnap;
use twgame_core::twsnap::enums::Sound;
use twgame_core::twsnap::vec2_from_bits;
use vek::Vec2;

#[derive(Debug)]
pub struct Events {
    pub(super) events: Vec<twsnap::Events>,
}

impl Events {
    pub(super) fn new() -> Self {
        Self { events: vec![] }
    }

    pub(super) fn clear(&mut self) {
        self.events.clear();
    }
}

impl Events {
    pub fn add_event(&mut self, event: twsnap::Events) {
        self.events.push(event);
    }

    pub fn create_sound(&mut self, pos: Vec2<f32>, sound: Sound) {
        self.events
            .push(twsnap::Events::Sound(twsnap::events::Sound {
                pos: vec2_from_bits(pos),
                sound,
            }))
    }
}
