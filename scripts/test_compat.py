#!/usr/bin/env python3

import subprocess
import re
import shutil
import os.path
import time

def download_ddnet(version):
    archive = f"ddnet-{version}.tar.xz"
    download = f"https://ddnet.org/downloads/DDNet-{version}-linux_x86_64.tar.xz"
    server_name = f"ddnet-server-{version}"
    if not os.path.exists(server_name) and not os.path.exists(archive):
        subprocess.run(["wget", download, "-O", archive])
    if not os.path.exists(server_name):
        subprocess.run(["tar", "--transform", f"s/DDNet-Server/{server_name}/", "--strip-components=1", "-xf", archive, "--wildcards", "*/DDNet-Server"])
    return server_name

def move_files(ths, version, desc):
    version = version.replace(".", "_")
    if len(ths) == 1:
        in_path = os.path.expanduser(f"~/.teeworlds/{ths[0]}")
        out_path = f"../replayer/res/compat/{version}_{desc}.teehistorian"
        shutil.move(in_path, out_path)
    else:
        for (i, th) in enumerate(ths):
            in_path = os.path.expanduser(f"~/.teeworlds/{th}")
            out_path = f"../replayer/res/compat/{version}_{desc}_{i}.teehistorian"
            shutil.move(in_path, out_path)

def empty(version, server_name):
    print("empty")
    res = subprocess.run(["./"+ server_name, "-f", "shutdown_instantly.cfg"], capture_output=True)
    out = res.stdout.decode()
    ths = re.findall("recording to '(.*)'", out)
    print(ths)
    move_files(ths, version, "empty")

def player(version, server_name):
    print("player")
    p = subprocess.Popen(["./"+ server_name, "-f", "physics_test_1.cfg"], stdout=subprocess.PIPE)
    time.sleep(1)
    p2 = subprocess.Popen(["./DDNet", "cl_download_skins 0;connect localhost:8303"])
    time.sleep(4)
    p2.terminate()
    time.sleep(1)
    p.terminate()
    out, _ = p.communicate()
    out = out.decode()
    ths = re.findall("recording to '(.*)'", out)
    print(ths)
    move_files(ths, version, "player")

def main():
    for version in open("versions.txt"):
        version_major = int(version.split(".")[0])
        if version_major < 15:
            continue
        version = version.strip()
        print(version)
        server_name = download_ddnet(version)
        empty(version, server_name)
        player(version, server_name)

if __name__ == '__main__':
    main()
