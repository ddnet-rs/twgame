#!/usr/bin/env python3

import subprocess
import re
import shutil

# not: hear, shields, tele, jump_giver, wall_jump, timecp, weapon_tele, kill
#      test functionality of those tiles
TESTS = ['solo', 'nohit', 'endless_hook', 'hook', 'jetpack', 'infinite_jumps', 'collision',
        'got_laser', 'got_ninja', 'got_grenade', 'got_shotgun',
        'selected_pistol', 'selected_grenade', 'selected_shotgun', 'selected_laser',
        'lock',
        'teleporter_gun', 'teleporter_grenade', 'teleporter_laser',
        'nohit_hammer', 'nohit_shotgun', 'nohit_grenade', 'nohit_laser',
        #'hit_hammer', 'hit_shotgun', 'hit_grenade', 'hit_laser',
        'jump1', 'jump3',
        'timepanelty', 'timeaward',
        'tune',
        #need_two
        'live_freeze', 'deep_freeze', 'freeze',
        'timecp',
        'heart',
        '2sfreeze'
        ]
LAYERS = ['front', 'game']
CASES = ['activate', 'stays_activated', 'deactivate', 'stays_deactivated', 'finish']

def run():
    res = subprocess.run(["../ddnet-build/DDNet-Server", "-f", "autoexec_server.cfg"], capture_output=True)
    out = res.stdout.decode()
    if "skip" in out:
        return None, ""
    p = re.compile("teehistorian: recording to '(.*)'")
    result = p.search(out)
    times = ""
    for line in out.splitlines():
        if 'record_race' in line:
            times += line
    return result.group(1), times

def main():
    print(len(TESTS)*len(LAYERS)*len(CASES))
    skip = 320
    in_dir = '../../.teeworlds/'
    out_dir = '../twgame/replayer/res/physics_5/'
    num = 0

    h = 1
    m = 0
    for test in TESTS:
        for layer in LAYERS:
            for (i, case) in enumerate(CASES):
                num += 1
                if skip > 0:
                    skip -= 1
                    continue
                if 116 <= num <= 120 or 126 <= num <= 130 or 136 <= num <= 140 or 146 <= num <= 150:
                    continue
                if 196 <= num <= 200 or 206 <= num <= 210 or 216 <= num <= 220 or 226 <= num <= 230:
                    continue
                if 236 <= num <= 240 or 246 <= num <= 250:
                    continue
                if 256 <= num <= 260 or 266 <= num <= 270:
                    continue
                if num == 313 or num == 314 or num == 318 or num == 319:
                    continue
                if num > 320 and (num % 5 == 3 or num % 5 == 4):
                    continue
                out_file = 'save_'+test+'_'+ layer+'_'+str(i)+'_'+case+'.teehistorian'
                print(num, out_file, end="; ", flush=True)
                while True:
                    subprocess.run(["sudo", "date", "-s", ""'2023-04-16 '+str(h) + ':' + str(m)])

                    th_file, finishes = run()
                    if th_file is None:
                        print("skip")
                        continue
                    shutil.copy(in_dir + th_file, out_dir + out_file)
                    if len(finishes) > 0:
                        #time = finishes[0][1]
                        #names = [f[0] for f in finishes]
                        #names ='", "'.join(names) 
                        #content = 
                        """\
#{{
	#"finishes": [
		#{{
			#"players": ["{names}"],
			#"time": {time}
		#}}
#}}
#"""
                        with open(out_dir+out_file+".raw", "w") as f:
                            print("finishes", finishes)
                            f.write(finishes)
                            f.close()
                    break
                m += 2
                if m >= 60:
                    m = 0
                    h += 1


    #print(run())
    pass

if __name__ == '__main__':
    main()
