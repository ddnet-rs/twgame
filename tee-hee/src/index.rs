//! Store teehistorian files
//!
//! Distributes the tables throughout multiple SQLite files to optimize
//! reading helper tables into memory without.

use chrono::DateTime;
use core::str;
use num_enum::IntoPrimitive;
use num_enum::TryFromPrimitive;
use rayon::iter::ParallelBridge;
use rayon::iter::ParallelIterator;
use rusqlite::Connection;
use std::str::FromStr;
use std::sync::atomic::AtomicI32;
use std::sync::atomic::Ordering;
use std::sync::mpsc;
use std::{
    collections::{HashMap, HashSet},
    path::{Path, PathBuf},
};
use teehistorian_replayer::teehistorian::Th;
use twgame::ThHeader;
use uuid::Uuid;

use crate::decompress;

static QUEUE_SIZE: AtomicI32 = AtomicI32::new(0);

// cache for writing new
pub struct MetaWriteCache {
    /// Contains helper information that is common between most teehistorian files and should fit into memory
    conn: Connection,

    // cache of map name, sha256 -> map_id
    maps: HashMap<(String, [u8; 32]), u32>,
    // cache of path -> path_id
    paths: HashMap<(String, Compression), u32>,
    // cache of version -> version_id
    server_versions: HashMap<String, u32>,
}

impl MetaWriteCache {
    fn populate(data_dir: &Path) -> rusqlite::Result<Self> {
        let conn = Self::setup_database(data_dir)?;
        let maps = Self::get_all_known_maps(&conn)?;
        let paths = Self::get_all_known_paths(&conn)?;
        let server_versions = Self::get_all_known_server_versions(&conn)?;
        Ok(Self {
            conn,
            maps,
            paths,
            server_versions,
        })
    }

    pub fn setup_database(data_dir: &Path) -> rusqlite::Result<Connection> {
        let conn = Connection::open(data_dir.join("index-meta.sqlite"))?;
        conn.execute(
            "CREATE TABLE IF NOT EXISTS paths (\
                path_id INTEGER PRIMARY KEY, \
                path TEXT NOT NULL, \
                compression INTEGER NOT NULL
            ) STRICT;",
            [],
        )?;
        conn.execute(
            "CREATE TABLE IF NOT EXISTS maps (\
                map_id INTEGER PRIMARY KEY, \
                name TEXT NOT NULL, \
                sha256 BLOB NOT NULL, \
                crc INTEGER NOT NULL, \
                size INTEGER NOT NULL\
            ) STRICT;",
            [],
        )?;
        conn.execute(
            "CREATE TABLE IF NOT EXISTS server_versions (\
                version_id INTEGER PRIMARY KEY, \
                version TEXT NOT NULL\
            ) STRICT;",
            [],
        )?;
        Ok(conn)
    }

    fn get_all_known_paths(
        conn: &Connection,
    ) -> rusqlite::Result<HashMap<(String, Compression), u32>> {
        let mut stmt = conn.prepare("SELECT path_id, path, compression FROM paths;")?;
        let paths: Result<_, _> = stmt
            .query_map([], |row| {
                let compression: u8 = row.get(2)?;
                let compression = Compression::try_from(compression).expect("unkown compression");
                Ok(((row.get(1)?, compression), row.get(0)?))
            })?
            .collect();
        paths
    }

    pub fn get_all_known_paths_inv(
        conn: &Connection,
    ) -> rusqlite::Result<HashMap<u32, (String, Compression)>> {
        let mut stmt = conn.prepare("SELECT path_id, path, compression FROM paths;")?;
        let paths: Result<_, _> = stmt
            .query_map([], |row| {
                let compression: u8 = row.get(2)?;
                let compression = Compression::try_from(compression).expect("unkown compression");
                Ok((row.get(0)?, (row.get(1)?, compression)))
            })?
            .collect();
        paths
    }

    #[allow(clippy::type_complexity)]
    fn get_all_known_maps(conn: &Connection) -> rusqlite::Result<HashMap<(String, [u8; 32]), u32>> {
        let mut stmt = conn.prepare("SELECT map_id, name, sha256 FROM maps;")?;
        let maps: Result<_, _> = stmt
            .query_map([], |row| Ok(((row.get(1)?, row.get(2)?), row.get(0)?)))?
            .collect();
        maps
    }

    fn get_all_known_server_versions(conn: &Connection) -> rusqlite::Result<HashMap<String, u32>> {
        let mut stmt = conn.prepare("SELECT version_id, version FROM server_versions;")?;
        let paths: Result<_, _> = stmt
            .query_map([], |row| Ok((row.get(1)?, row.get(0)?)))?
            .collect();
        paths
    }

    pub fn get_map_id<F: FnOnce() -> (u32, u32)>(
        &mut self,
        key: &(String, [u8; 32]),
        get_crc_size: F,
    ) -> u32 {
        if let Some(map_id) = self.maps.get(key) {
            return *map_id;
        }
        let (crc, size) = get_crc_size();
        let map_id: u32 = self
            .conn
            .query_row(
                "INSERT INTO maps (name, sha256, crc, size) \
                VALUES (?, ?, ?, ?) \
                RETURNING map_id",
                (&key.0, &key.1, crc, size),
                |row| row.get(0),
            )
            .unwrap();
        self.maps.insert(key.clone(), map_id);
        map_id
    }

    pub fn get_version_id(&mut self, key: &str) -> u32 {
        if let Some(version_id) = self.server_versions.get(key) {
            return *version_id;
        }
        let version_id: u32 = self
            .conn
            .query_row(
                "INSERT INTO server_versions (version) \
                VALUES (?) \
                RETURNING version_id",
                (&key,),
                |row| row.get(0),
            )
            .unwrap();
        self.server_versions.insert(key.to_owned(), version_id);
        version_id
    }

    pub fn get_path_id(&mut self, key: &(String, Compression)) -> u32 {
        if let Some(path_id) = self.paths.get(key) {
            return *path_id;
        }
        let path_id: u32 = self
            .conn
            .query_row(
                "INSERT INTO paths (path, compression) \
                VALUES (?, ?) \
                RETURNING path_id",
                (&key.0, &(key.1 as u8)),
                |row| row.get(0),
            )
            .unwrap();
        self.paths.insert(key.clone(), path_id);
        path_id
    }
}

/// Struct holding meta information about teehistorian files that have the header written.
pub struct Index {
    conn: Connection,
}

impl Index {
    /// Setup databases if needed in the passed directory
    pub fn new(data_dir: &Path) -> rusqlite::Result<Self> {
        // expecting a directory, not a file

        // open databases
        // 1:1 mapping between the teehistorian uuid (16 bytes) and a teehistorian_id (integer, 4 bytes)
        // all remaining tables only use the teehistorian_id, not the teehistorian uuid, and their directory
        // enough information to retrieve the file name.
        let conn = Connection::open(data_dir.join("index-files.sqlite"))?;
        // Parsed and unified information about Teehistorian files
        conn.execute(
            "ATTACH DATABASE ? AS parsed",
            [&data_dir
                .join("index-parsed.sqlite")
                .as_os_str()
                .to_str()
                .unwrap()],
        )?;
        Self::setup_database(&conn)?;
        let this = Self { conn };
        this.make_consistent()?;
        Ok(this)
    }

    fn setup_database(conn: &Connection) -> rusqlite::Result<()> {
        conn.execute(
            "CREATE TABLE IF NOT EXISTS main.files (\
                teehistorian_id INTEGER PRIMARY KEY, \
                uuid BLOB NOT NULL, \
                path_id INTEGER NOT NULL\
            ) STRICT;",
            [],
        )?;
        conn.execute(
            "CREATE TABLE IF NOT EXISTS parsed.teehistorian (\
                teehistorian_id INTEGER PRIMARY KEY, \
                date INTEGER NOT NULL, \
                map_id INTEGER NOT NULL, \
                version_id INTEGER NOT NULL\
            ) STRICT;",
            [],
        )?;
        Ok(())
    }

    /// cleanup last run, if cancelled before all tables were written
    fn make_consistent(&self) -> rusqlite::Result<()> {
        let tables = ["files", "teehistorian"];
        for table in tables {
            let query = format!(
                "DELETE FROM {} \
                WHERE teehistorian_id > (\
                    SELECT MIN(files_id, parsed_id) \
                    FROM \
                        (SELECT IFNULL(MAX(teehistorian_id), 0) files_id FROM files), \
                        (SELECT IFNULL(MAX(teehistorian_id), 0) parsed_id FROM teehistorian)\
                )",
                table
            );
            let removed = self.conn.execute(&query, ())?;
            if removed != 0 {
                println!("Removed {removed} rows from {table} from previous incompletly run");
            }
        }
        Ok(())
    }

    fn get_all_existing_uuids(&self) -> rusqlite::Result<HashSet<Uuid>> {
        let mut stmt = self.conn.prepare("SELECT uuid FROM files;")?;
        let uuids: Result<_, _> = stmt
            .query_map([], |row| Ok(Uuid::from_bytes(row.get(0)?)))?
            .collect();
        uuids
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, TryFromPrimitive, IntoPrimitive)]
#[repr(u8)]
pub enum Compression {
    None,
    Xz,
    Zstd,
}

impl Compression {
    pub fn to_file_extension(self) -> &'static str {
        match self {
            Compression::None => "",
            Compression::Xz => ".xz",
            Compression::Zstd => ".zst",
        }
    }
}

fn teehistorian_file_uuid(entry: &Path) -> Option<(Uuid, Compression)> {
    let entry = entry.file_name()?.to_str()?;
    let mut entry = entry.split('.');
    let uuid = entry.next()?;
    if entry.next() != Some("teehistorian") {
        return None;
    }
    let compression = match entry.next() {
        None => Compression::None,
        Some("xz") => Compression::Xz,
        Some("zst") => Compression::Zstd,
        _ => return None, // ignore all other files extensions
    };
    if entry.next().is_some() {
        return None; // ignore files with further extensions
    }
    Uuid::from_str(uuid).ok().map(|uuid| (uuid, compression))
}

impl Index {
    pub fn traverse_files(&self, data_dir: &Path, path: &str) {
        let mut path = PathBuf::from_str(path).unwrap();
        path = path.canonicalize().expect("Can't get canonical path");

        // current
        let uuids = self.get_all_existing_uuids().unwrap();
        let mut cache = MetaWriteCache::populate(data_dir).unwrap();
        let mut thread_index = Index::new(data_dir).unwrap();

        let (tx, rx) = mpsc::sync_channel::<(String, Uuid, Compression, ThHeader)>(1000);
        let sqlite_thread = std::thread::spawn(move || {
            let mut transaction_remaining = 1;
            let mut transaction = thread_index.conn.transaction().unwrap();
            for (index, (dir, uuid, compression, header)) in rx.iter().enumerate() {
                if transaction_remaining == 0 {
                    transaction_remaining = QUEUE_SIZE.load(Ordering::Relaxed);
                    transaction.commit().unwrap();
                    transaction = thread_index.conn.transaction().unwrap();
                } else {
                    transaction_remaining -= 1;
                }
                let current_queue = QUEUE_SIZE.fetch_sub(1, Ordering::Relaxed);
                //println!("[{index}] {dir}/{uuid}.teehistorian");
                if index % 1000 == 0 {
                    println!("[{index}] {dir}/{uuid}.teehistorian {current_queue}");
                }
                // put info into sqlite index
                let Some(sha256) = crate::th_map_sha256(&header) else {
                    // TODO: expect("unknown CRC") when finding the last two maps... :)
                    continue;
                };

                let get_crc_size = || {
                    let crc = hex::decode(&header.map_crc).expect("valid hex map_crc");
                    let crc: [u8; 4] = crc.try_into().expect("correct crc size");
                    let crc = u32::from_be_bytes(crc);
                    let size: u32 = header.map_size.parse().expect("integer in map_size");
                    (crc, size)
                };
                let map_id = cache.get_map_id(&(header.map_name.clone(), sha256), get_crc_size);
                let version_id = cache.get_version_id(&header.server_version);
                let path_id = cache.get_path_id(&(dir, compression));

                let teehistorian_id: u64 = transaction
                    .query_row(
                        "INSERT INTO files (uuid, path_id) \
                        VALUES (?, ?) \
                        RETURNING teehistorian_id",
                        (uuid.as_bytes(), path_id),
                        |row| row.get(0),
                    )
                    .unwrap();
                let date = Self::parse_teehistorian_date(&header.start_time);
                transaction
                    .execute(
                        "INSERT INTO teehistorian (teehistorian_id, date, map_id, version_id) \
                        VALUES (?, ?, ?, ?)",
                        (teehistorian_id, date, map_id, version_id),
                    )
                    .unwrap();
            }
            transaction.commit().unwrap();
        });

        walkdir::WalkDir::new(path)
            .into_iter()
            .par_bridge()
            // filter out errors
            .filter_map(|e| match e {
                Ok(f) => Some(f),
                Err(err) => {
                    println!("Error directory {}", err);
                    None
                }
            })
            // filter out non-teehistorian files
            .filter_map(|dir_entry| {
                teehistorian_file_uuid(dir_entry.path()).map(|uuid| (dir_entry, uuid))
            })
            // only index new files
            .filter(|(_, (uuid, _compression))| !uuids.contains(uuid))
            // get teehistorian header if possible, TODO: print errors
            .filter_map(|(file_name, (uuid, compression))| {
                let th = decompress::th_buf_reader(file_name.path()).ok()?;
                let mut th = Th::parse(th).ok()?;
                let header = th.header().ok()?;
                let header = str::from_utf8(header).expect("header should be valid utf-8");
                // now try to parse the header
                let th_header = serde_json::from_str::<ThHeader>(header).ok()?;

                let dir = file_name.path().parent().expect("path should have parent");
                let dir = dir.to_str().expect("utf-8 encoded path name").to_owned();

                QUEUE_SIZE.fetch_add(1, Ordering::Relaxed);
                tx.send((dir, uuid, compression, th_header)).unwrap();
                Some(())
            })
            .for_each(|_| {
                // execute the lazy function above :^)
            });
        drop(tx);
        sqlite_thread.join().unwrap()
    }

    // Date format is either
    // 2024-04-03T23:45:47+0200
    // 2023-08-27 00:47:16 +0200
    // TODO: put fallible version into teehistorian crate
    pub fn parse_teehistorian_date(inp: &str) -> i64 {
        let res = if inp.len() == 24 {
            DateTime::parse_and_remainder(inp, "%Y-%m-%dT%H:%M:%S%z").unwrap()
        } else {
            DateTime::parse_and_remainder(inp, "%Y-%m-%d %H:%M:%S %z").unwrap()
        };
        assert_eq!(res.1, "");
        res.0.timestamp()
    }
}
