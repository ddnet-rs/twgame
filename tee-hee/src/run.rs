//! General architecture:
//!
//! * main thread: iterate over index and spawns one thread for each replay.
//!    * waits on mpsc to write results and spawn new threads.
//!    * closes join handle mpsc on completing processing all files.
//! * replay thread:
//!    * catches panics or completes replay
//!    * sends result back to main thread to persist results and indicate that a new thread can be spawned

use crate::{
    app_dir,
    index::{Compression, Index, MetaWriteCache},
    map_service::MapService,
    replay::replay_inner,
    ReplayOptInner, RunOpt, WorldKind,
};
use rusqlite::OptionalExtension;
use std::{
    collections::HashMap,
    fs, mem, panic,
    path::Path,
    sync::{
        mpsc::{self, Receiver, Sender},
        Arc,
    },
};
use uuid::Uuid;

struct RunReplayThread {
    teehistorian_id: i64,
    path: String,
    /// Send complete channel to MainThread to persist result and start new thread.
    send_complete: Sender<Option<(i64, Result<(), String>)>>,
    /// load maps once into memory
    map_service: Arc<MapService>,
}

impl RunReplayThread {
    fn new(
        teehistorian_id: i64,
        path: String,
        send_complete: Sender<Option<(i64, Result<(), String>)>>,
        map_service: Arc<MapService>,
    ) -> Self {
        Self {
            teehistorian_id,
            path,
            send_complete,
            map_service,
        }
    }
    fn run(&self) {
        let replay_opt = ReplayOptInner {
            demo: None,
            twgame_demo: None,
            map: None,
            world: vec![
                WorldKind::DdnetCompat,
                WorldKind::Twgame,
                WorldKind::Bug8199,
            ],
            print_result: false,
        };
        let result = panic::catch_unwind(|| {
            let res = replay_inner(
                &replay_opt,
                Some(self.teehistorian_id),
                &self.path,
                &self.map_service,
            );
            if let Some(res) = res {
                println!("{} {}", self.teehistorian_id, res)
            }
        });
        // map panic to just the error string
        let result = result.map_err(|err| {
            err.downcast::<&str>()
                .map(|s| (*s).to_owned())
                .unwrap_or_default()
        });
        let _ = self
            .send_complete
            .send(Some((self.teehistorian_id, result)));
    }
}

struct RunMainThread {
    /// teehistorian_id -> path mapping
    conn_files: rusqlite::Connection,
    /// keep track on pending & completed
    conn_run: rusqlite::Connection,
    /// previously failed runs to retry
    retry: Vec<i64>,
    /// previously pending runs to redo
    previously_pending: Vec<i64>,
    /// last teehistorian_id to be known to be executed
    initial_max_teehistorian_id: i64,
    max_indexed_teehistorian_id: i64,
    known_paths: HashMap<u32, (String, Compression)>,
    /// initially fill with amount of None, then let replay thread respond with the result on complete replay
    send_complete: Sender<Option<(i64, Result<(), String>)>>,
    /// the amount of threads that we can still spawn
    recv_complete: Receiver<Option<(i64, Result<(), String>)>>,
    map_service: Arc<MapService>,
}

impl RunMainThread {
    fn setup_database(data_dir: &Path) -> rusqlite::Connection {
        let conn = rusqlite::Connection::open(data_dir.join("run.sqlite")).unwrap();
        conn.execute(
            "CREATE TABLE IF NOT EXISTS max_id (\
                teehistorian_id INTEGER NOT NULL\
            ) STRICT;",
            [],
        )
        .unwrap();
        conn.execute(
            "CREATE TABLE IF NOT EXISTS failed (\
                teehistorian_id INTEGER NOT NULL, \
                err TEXT NOT NULL\
            ) STRICT;",
            [],
        )
        .unwrap();
        conn.execute(
            "CREATE TABLE IF NOT EXISTS pending (\
                teehistorian_id INTEGER PRIMARY KEY\
            ) STRICT;",
            [],
        )
        .unwrap();
        conn
    }

    /// Setup database for storing progress
    fn new(data_dir: &Path, opt: RunOpt) -> Self {
        let conn_run = Self::setup_database(data_dir);
        // get currently  missing
        let retry = if opt.retry {
            // figure out which ones to retry
            let mut stmt = conn_run
                .prepare("SELECT teehistorian_id FROM failed")
                .unwrap();
            let retry: Result<_, _> = stmt.query_map([], |row| row.get(0)).unwrap().collect();
            retry.unwrap()
        } else {
            vec![]
        };
        let previously_pending = {
            // figure out which ones were pending before tee-hee got terminated last time
            let mut stmt = conn_run
                .prepare("SELECT teehistorian_id FROM pending")
                .unwrap();
            let pending: Result<_, _> = stmt.query_map([], |row| row.get(0)).unwrap().collect();
            pending.unwrap()
        };
        let (send_complete, recv_complete) = mpsc::channel();
        let max_teehistorian_id = {
            let mut stmt = conn_run
                .prepare("SELECT teehistorian_id FROM max_id")
                .unwrap();
            let id: Option<i64> = stmt.query_row([], |row| row.get(0)).optional().unwrap();
            if id.is_none() {
                conn_run
                    .execute("INSERT INTO max_id (teehistorian_id) VALUES (0)", [])
                    .unwrap();
            }
            id.unwrap_or(0)
        };
        // get remaining info for main thread
        let conn_files = rusqlite::Connection::open(data_dir.join("index-files.sqlite")).unwrap();
        let max_indexed_teehistorian_id = {
            conn_files
                .query_row("SELECT MAX(teehistorian_id) FROM files", [], |row| {
                    row.get(0)
                })
                .unwrap()
        };

        // we know all paths, due to capping replay at currently max indexed teehistorian_id
        let meta_cache = MetaWriteCache::setup_database(data_dir).unwrap();
        // TODO: move the function here, because it is only used here.
        let known_paths = MetaWriteCache::get_all_known_paths_inv(&meta_cache).unwrap();

        if let Some(num_threads) = opt.num_threads {
            for _ in 0..num_threads {
                send_complete.send(None).unwrap()
            }
        } else {
            for _ in 0..num_cpus::get() {
                send_complete.send(None).unwrap()
            }
        }

        Self {
            conn_run,
            conn_files,
            known_paths,
            max_indexed_teehistorian_id,
            retry,
            previously_pending,
            initial_max_teehistorian_id: max_teehistorian_id,
            send_complete,
            recv_complete,
            map_service: MapService::new(opt.map_service_opt),
        }
    }

    fn path(&self, teehistorian_id: i64) -> String {
        // TODO: cache statement?
        let (path_id, uuid): (u32, [u8; 16]) = self
            .conn_files
            .query_row(
                "SELECT path_id, uuid FROM files WHERE teehistorian_id = ?",
                [teehistorian_id],
                |row| Ok((row.get(0)?, row.get(1)?)),
            )
            .unwrap();
        let (path, compression) = self.known_paths.get(&path_id).unwrap();
        let compression = compression.to_file_extension();
        let uuid = Uuid::from_bytes(uuid);
        format!("{path}/{uuid}.teehistorian{compression}")
    }

    fn store_result(
        conn_run: &rusqlite::Connection,
        teehistorian_id: i64,
        result: Result<(), String>,
    ) {
        match result {
            Ok(()) => {
                // remove from failed
                let _ = conn_run
                    .execute(
                        "DELETE FROM failed WHERE teehistorian_id = ?",
                        [teehistorian_id],
                    )
                    .unwrap();
            }
            Err(err) => {
                // ignore when failing previously failed
                let _ = conn_run
                    .execute(
                        "INSERT OR IGNORE INTO failed (teehistorian_id, err) VALUES (?, ?)",
                        (teehistorian_id, err),
                    )
                    .unwrap();
            }
        }
        // remove from pending after adding failed, because we can get cancelled at any time
        // and removing from pending without adding to failed would mark as succeeded
        // when retrying failed
        let _ = conn_run
            .execute(
                "DELETE FROM pending WHERE teehistorian_id = ?",
                (teehistorian_id,),
            )
            .unwrap();
    }

    fn set_pending(&self, teehistorian_id: i64) {
        // only necessary for new teehistorian_id others are already either set to pending or failed
        if teehistorian_id > self.initial_max_teehistorian_id {
            let _ = self
                .conn_run
                .execute(
                    "INSERT OR IGNORE INTO pending (teehistorian_id) VALUES (?)",
                    (teehistorian_id,),
                )
                .unwrap();

            assert_eq!(
                self.conn_run
                    .execute("UPDATE max_id SET teehistorian_id = ?", (teehistorian_id,),)
                    .unwrap(),
                1,
                "max_id should have exectly one row"
            );
        }
    }

    fn replay_teehistorian_id(&self, teehistorian_id: i64) {
        let path = self.path(teehistorian_id);

        // wait until we got a signal that a thread completed, so we can start a new one.
        if let Some((res_id, result)) = self.recv_complete.recv().unwrap() {
            // store result from replay thread
            Self::store_result(&self.conn_run, res_id, result);
        }

        self.set_pending(teehistorian_id);

        let send_complete = self.send_complete.clone();
        let map_service = self.map_service.clone();

        println!("replaying {teehistorian_id} {path}");
        std::thread::spawn(move || {
            RunReplayThread::new(teehistorian_id, path, send_complete, map_service).run()
        });
    }

    /// Replay all failed and remaining Teehistorian files
    fn replay_all(mut self) {
        let mut retry = Vec::new();
        // go through all previously failed
        mem::swap(&mut retry, &mut self.retry);
        for teehistorian_id in retry.drain(..) {
            self.replay_teehistorian_id(teehistorian_id);
        }
        retry.clear();
        // go through all previously pending
        mem::swap(&mut retry, &mut self.previously_pending);
        for teehistorian_id in retry.drain(..) {
            self.replay_teehistorian_id(teehistorian_id);
        }
        for teehistorian_id in
            (self.initial_max_teehistorian_id + 1)..=self.max_indexed_teehistorian_id
        {
            self.replay_teehistorian_id(teehistorian_id);
        }
        // drop our sender so that so that the mpsc terminates when no further threads exist
        drop(self.send_complete);
        for (teehistorian_id, result) in self.recv_complete.into_iter().flatten() {
            Self::store_result(&self.conn_run, teehistorian_id, result);
        }
    }
}

pub fn run(opt: RunOpt) {
    assert_ne!(opt.num_threads, Some(0), "Need at least one replay thread");
    let data_dir = app_dir();
    if let Some(path) = opt.index.as_ref() {
        // create data directory for the index if not exist
        fs::create_dir_all(data_dir).expect("unable to create data dir");

        // create database files
        let index = Index::new(data_dir).unwrap();

        // traverse files and store a parsed header and the raw header in separate databases.
        index.traverse_files(data_dir, path);
    }

    let run = RunMainThread::new(data_dir, opt);
    run.replay_all();
}

pub fn status() {
    let data_dir = app_dir();

    let mut conn = rusqlite::Connection::open(data_dir.join("index-files.sqlite")).unwrap();
    let max_indexed_teehistorian_id: i64 = conn
        .query_row("SELECT MAX(teehistorian_id) FROM files", [], |row| {
            row.get(0)
        })
        .unwrap();
    println!("Num indexed: {max_indexed_teehistorian_id}");

    conn = rusqlite::Connection::open(data_dir.join("run.sqlite")).unwrap();
    let max_replayed_teehistorian_id: i64 = conn
        .query_row("SELECT teehistorian_id FROM max_id", [], |row| row.get(0))
        .unwrap();
    let num_pending: i64 = conn
        .query_row("SELECT COUNT(*) FROM pending", [], |row| row.get(0))
        .unwrap();
    let num_failed: i64 = conn
        .query_row("SELECT COUNT(*) FROM failed", [], |row| row.get(0))
        .unwrap();

    println!(
        "Num replayed: {}",
        max_replayed_teehistorian_id - num_pending - num_failed
    );
    println!("Num pending: {num_pending}");
    println!("Num failed: {num_failed}");
}
