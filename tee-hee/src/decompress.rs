//! Decompress on demand for teehistorian.xz and teehistorian.zstd files

use std::ffi::OsStr;
use std::fs::File;
use std::io;
use std::io::BufReader;
use std::path::Path;
use teehistorian_replayer::teehistorian::ThBufReader;
use xz2::read::XzDecoder;

pub enum ThCompressConverter {
    Plain(File),
    Xz(XzDecoder<File>),
    Zstd(zstd::stream::Decoder<'static, BufReader<File>>),
}

impl io::Read for ThCompressConverter {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        match self {
            Self::Plain(f) => f.read(buf),
            Self::Xz(f) => f.read(buf),
            Self::Zstd(f) => f.read(buf),
        }
    }
}

pub fn th_buf_reader<P: AsRef<Path>>(file_name: P) -> io::Result<ThBufReader<ThCompressConverter>> {
    let extension = file_name.as_ref().extension();
    let file = File::open(file_name.as_ref())?;
    let c = if extension == Some(OsStr::new("xz")) {
        let xz = XzDecoder::new(file);
        ThCompressConverter::Xz(xz)
    } else if extension == Some(OsStr::new("zst")) {
        let z = zstd::stream::Decoder::new(file)?;
        ThCompressConverter::Zstd(z)
    } else {
        ThCompressConverter::Plain(file)
    };
    Ok(ThBufReader::new(c))
}
