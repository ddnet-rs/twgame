use app_dirs2::{get_data_root, AppDataType};
use clap::{Args, CommandFactory, Parser, Subcommand};
use hex_literal::hex;
use index::Index;
use map_service::MapService;
use select::Select;
use serde::Serialize;
use std::{fs, path::PathBuf, sync::OnceLock};
use teehistorian_replayer::teehistorian;
use teehistorian_replayer::teehistorian::Th;
use twgame::core::DisplayChunk;
use twgame::ThHeader;
use uuid::Uuid;

fn app_dir() -> &'static PathBuf {
    static APP_DIR: OnceLock<PathBuf> = OnceLock::new();
    APP_DIR.get_or_init(|| {
        get_data_root(AppDataType::UserData)
            .unwrap()
            .join("tee-hee")
    })
}

// helpers (could be put into lib instead if necessary)
mod crcs;
mod decompress;
mod distributor;
mod map_service;

mod worlds {
    pub mod bug5121;
    pub mod bug7685;
    pub mod bug8199;
    pub mod bug8220;
    pub mod bug8486;
    pub mod bug8642;
    pub mod info;
    pub mod physics_twgame;
}

// subcommands
mod index;
mod replay;
mod run;
mod select;

fn th_map_sha256(header: &ThHeader) -> Option<[u8; 32]> {
    match header.map_sha256.as_deref() {
        // 0.7 teehistorian server have the map_sha256 field, but that contains an
        // empty string.
        Some("") | None => {
            let crc = hex::decode(&header.map_crc).expect("valid hex map_crc");
            let crc: [u8; 4] = crc.try_into().expect("correct crc size");
            let crc = u32::from_be_bytes(crc);
            // we assume that we know all sha256 from crcs for old teehistorian files
            crate::crcs::crc_to_sha256(crc)
        }
        Some(sha256) => {
            let sha256 = hex::decode(sha256).expect("valid hex map_sha256 checksum");
            if let Ok(sha256) = sha256[..].try_into() {
                Some(sha256)
            } else {
                // there are a 8 of 0.7 teehistorian files with sha256s cut down to the first 5 bytes
                let short_sha256: [u8; 5] = sha256[..].try_into().expect("trimmed down sha256sum");
                match short_sha256 {
                    [0x16, 0x83, 0x39, 0xf6, 0x96] => Some(hex!(
                        "168339f6963156758a1a0584d4454dde66125693bcc22476b5068fafd8b41d28"
                    )),
                    _ => None,
                }
            }
        }
    }
}

/// Replay, index and detect bugs in DDNet Teehistorian files.
///
/// Tee-Hee transparently decompresses (.xz, .zst) Teehistorian files.
/// Some commands require an index to efficiently go through all or a subset of Teehistorian files.
/// To index Teehistorian files they need to be named in the form "{game_uuid}.teehistorian{,.xz,.zst}".
/// After a Teehistorian file got indexed, Tee-Hee expects the file to stay at its location.
///
/// Teehistorian documentation: https://ddnet.org/docs/libtw2/teehistorian/
#[derive(Parser, Clone)]
#[command(author, version, about, long_about)]
#[command(propagate_version = true)]
struct Cli {
    #[command(subcommand)]
    commands: Commands,
}

#[derive(Subcommand, Clone)]
enum Commands {
    /// Replays Teehistorian file, generates DDNet demo file
    Replay(ReplayOpt),
    /// Prints Teehistorian header and all Teehistorian chunks
    Cat {
        /// Path to Teehistorian file
        teehistorian: String,
    },
    /// Prints Teehistorian header
    Header {
        /// Path to Teehistorian file
        teehistorian: String,
    },
    /// Indexes new files from passed directory and runs all world implementations on all indexed Teehistorian files
    Run(RunOpt),
    /// Indexes new files from passed directory
    Index {
        /// Path to recursivly traverse
        path: String,
    },
    /// Selects Teehistorian files from the index based on specified criteria
    Select(SelectOpt),
    /// Replays Teehistorian files selected from the index based on specified criteria
    SelectReplay(SelectReplayOpt),
    /// Prints current status of replayed runs via `tee-hee run`
    Status,
    /// Helper command to create a map cache compatible with Tee-Hee
    MapsRenameToSha256 {
        /// Path to directory to traverse recursively for .map files
        in_path: String,
        /// Output path to copy maps to with name {sha256}.map
        out_path: String,
    },
}

#[derive(clap::ValueEnum, Clone, Copy, Debug, Serialize, PartialEq, Eq, Hash)]
#[serde(rename_all = "kebab-case")]
pub enum WorldKind {
    /// Use TwGame with ddnet-compat option to accurately replay teleporter.
    DdnetCompat,
    /// Use TwGame with less compat guarantees for replay, but with cleaner separation of teams
    Twgame,
    /// Run detection for enabling team0mode in practice: https://github.com/ddnet/ddnet/pull/8199
    Bug8199,
}

#[derive(Args, Clone, Default)]
struct MapServiceOpt {
    #[arg(long)]
    /// Path to map cache directory. Maps are required to be named "{sha256}.map"
    maps: Option<String>,
    /// Try to find and download the map from external sources when not found locally or in map_archive.
    /// Known supported URLS (listing of directories generated with nginx that contain urls with maps containing the sha256sum):
    /// "https://maps.ddnet.org", and
    /// "https://heinrich5991.de/teeworlds/maps/maps"
    #[arg(long)]
    maps_url: Vec<String>,
    /// Disable downloading of maps from external servers.
    #[arg(long)]
    offline: bool,
}

#[derive(Args, Clone)]
struct RunOpt {
    /// Path to recursivly traverse to index new Teehistorian files
    #[arg(long)]
    index: Option<String>,
    /// Retry previously failed replays
    #[arg(long)]
    retry: bool,
    /// Number of threads to spawn for replaying Teehistorian files.
    /// The number of cpu-threads is used is not specified.
    #[arg(long)]
    num_threads: Option<u16>,
    #[command(flatten)]
    map_service_opt: MapServiceOpt,
}

#[derive(Args, Clone)]
struct ReplayOpt {
    /// Path to Teehistorian file
    teehistorian: String,
    #[command(flatten)]
    verbose: clap_verbosity_flag::Verbosity,
    #[command(flatten)]
    outer: ReplayOptOuter,
}

#[derive(Args, Clone)]
struct ReplayOptOuter {
    #[command(flatten)]
    map_service_opt: MapServiceOpt,
    #[command(flatten)]
    inner: ReplayOptInner,
}

#[derive(Args, Clone)]
struct ReplayOptInner {
    /// Path to DDNet demo output file/directory. Always uses the ddnet-compat world
    #[arg(long, help_heading = Some("Replay World Options"))]
    demo: Option<String>,
    /// Path to DDNet demo output file/directory. Always uses the twgame world
    #[arg(long, help_heading = Some("Replay World Options"))]
    twgame_demo: Option<String>,
    /// Path to map to use for replaying. Retrieved from map archive if not specified.
    #[arg(long, help_heading = Some("Replay World Options"))]
    map: Option<String>,
    /// Worlds to use for replay. Note that the --demo and --twgame-demo also automatically
    /// imply some worlds here. May select multiple to run simultaneously.
    #[arg(long, help_heading = Some("Replay World Options"))]
    world: Vec<WorldKind>,
    #[arg(long)]
    print_result: bool,
}

#[derive(clap::ValueEnum, Clone, Debug, Serialize)]
#[serde(rename_all = "kebab-case")]
enum SelectOrderBy {
    Id,
    IdDesc,
    Uuid,
    UuidDesc,
    MapName,
    MapNameDesc,
    MapSha256,
    MapSha256Desc,
    Time,
    TimeDesc,
    DdnetVersion,
    DdnetVersionDesc,
}

#[derive(Args, Clone)]
struct SelectOpt {
    /// Select by exact map name: Note this is the map name at server time. There have been map renames not captured here
    #[arg(short = 'm', long, help_heading = Some("Select Options"))]
    map_name: Option<String>,

    /// Output the map name
    #[arg(short = 'M', long, help_heading = Some("Select Options"))]
    output_map_name: bool,

    /// Select by exact map_sha256
    #[arg(short = 's', long, help_heading = Some("Select Options"))]
    map_sha256: Option<String>,

    /// Output the map sha256
    #[arg(short = 'S', long, help_heading = Some("Select Options"))]
    output_map_sha256: bool,

    /// Select after this day inclusive (format: YYYY-MM-DD)
    #[arg(short, long, help_heading = Some("Select Options"))]
    after: Option<String>,

    /// Select before this day inclusive (format: YYYY-MM-DD)
    #[arg(short, long, help_heading = Some("Select Options"))]
    before: Option<String>,

    /// Output the timestamp
    #[arg(short = 'T', long, help_heading = Some("Select Options"))]
    output_time: bool,

    /// Select by exact teehistorian_uuid. This matches the game_uuid in the ddnet rank database
    #[arg(short = 'u', long, help_heading = Some("Select Options"))]
    teehistorian_uuid: Option<Uuid>,

    /// Output teehistorian_uuid
    #[arg(short = 'U', long, help_heading = Some("Select Options"))]
    output_teehistorian_uuid: bool,

    /// Select teehistorian_id greater or equal than passed (inclusive)
    #[arg(short = 'f', long, help_heading = Some("Select Options"))]
    teehistorian_id_from: Option<i64>,

    /// Select teehistorian_id less than passed or equal (inclusive)
    #[arg(short = 't', long, help_heading = Some("Select Options"))]
    teehistorian_id_to: Option<i64>,

    /// Select by exact teehistorian_id
    #[arg(short = 'i', long, help_heading = Some("Select Options"))]
    teehistorian_id: Option<i64>,

    /// Output teehistorian_id
    #[arg(short = 'I', long, help_heading = Some("Select Options"))]
    output_teehistorian_id: bool,

    /// fuzzy match on ddnet version. Can use fuzzy '%' to match (Example "15.0.2", sha1 commit hash)
    #[arg(short = 'd', long, help_heading = Some("Select Options"))]
    ddnet_version: Option<String>,

    /// Output ddnet version
    #[arg(short = 'D', long, help_heading = Some("Select Options"))]
    output_ddnet_version: bool,

    /// Select on panicked on replay
    #[arg(long, help_heading = Some("Select Options"))]
    panicked: bool,

    /// Select on currently pending tee-hee runs
    #[arg(long, help_heading = Some("Select Options"))]
    pending: bool,

    /// Output full teehistorian header
    #[arg(short = 'H', long, help_heading = Some("Select Options"))]
    output_teehistorian_header: bool,

    /// Output order. May be passed multiple times
    #[arg(long, help_heading = Some("Select Options"))]
    order_by: Vec<SelectOrderBy>,
}

#[derive(Args, Clone)]
struct SelectReplayOpt {
    /// Store results from the replayer in sqlite tables for later queries
    #[arg(long)]
    persist_results: bool,

    #[command(flatten)]
    select: SelectOpt,

    #[command(flatten)]
    replay: ReplayOptOuter,
}

fn main() {
    // parse arguments
    let args = Cli::parse();

    match args.commands {
        Commands::Index { path } => {
            // create data directory for the index if not exist
            let data_dir = app_dir();
            fs::create_dir_all(data_dir).expect("unable to create data dir");

            // create database files
            let index = Index::new(data_dir).unwrap();

            // traverse files and store a parsed header and the raw header in separate databases.
            index.traverse_files(data_dir, &path);
        }
        Commands::Run(opt) => run::run(opt),
        Commands::Replay(opt) => replay::replay(opt),
        Commands::Select(opt) => {
            let matches = Cli::command().get_matches();
            let select_matches = matches.subcommand_matches("select").unwrap();
            let s = Select::new(app_dir()).unwrap();
            s.teehistorian_files(opt, select_matches, None, false);
        }
        Commands::SelectReplay(opt) => {
            let matches = Cli::command().get_matches();
            let select_matches = matches.subcommand_matches("select-replay").unwrap();

            let s = Select::new(app_dir()).unwrap();
            s.teehistorian_files(
                opt.select,
                select_matches,
                Some(opt.replay),
                opt.persist_results,
            );
        }
        Commands::Cat { teehistorian } => {
            let mut th = Th::parse(decompress::th_buf_reader(&teehistorian).unwrap()).unwrap();
            println!("{}", String::from_utf8_lossy(th.header().unwrap()));

            loop {
                match th.next_chunk() {
                    Ok(chunk) => println!("{}", DisplayChunk(&chunk)),
                    Err(teehistorian::Error::Eof) => break,
                    Err(err) => {
                        println!("Err: {}", err);
                        break;
                    }
                }
            }
        }
        Commands::Header { teehistorian } => {
            let th = decompress::th_buf_reader(&teehistorian).unwrap();
            let mut th = Th::parse(th).unwrap();
            println!("{}", String::from_utf8_lossy(th.header().unwrap()));
        }
        Commands::Status => run::status(),
        Commands::MapsRenameToSha256 { in_path, out_path } => {
            MapService::rename_to_sha256(&in_path, &out_path);
        }
    }
}
