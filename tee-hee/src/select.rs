//! Run various costly checks against teehistorian files.

use crate::index::Compression;
use crate::map_service::MapService;
use crate::replay::replay_inner;
use crate::{MapServiceOpt, ReplayOptOuter};
use crate::{SelectOpt, SelectOrderBy};
use chrono::{DateTime, Days, NaiveDate, NaiveTime, Utc};
use clap::ArgMatches;
use rusqlite::{types::Value, Connection};
use std::path::Path;
use uuid::Uuid;

pub struct Select {
    conn: Connection,
}

struct OutputColumnOrder<'a> {
    idx: usize,
    order: Vec<(&'a str, Option<usize>)>,
}

impl<'a> OutputColumnOrder<'a> {
    fn new(output_order: &'a ArgMatches) -> Self {
        let order = output_order
            .ids()
            .filter(|id| id.as_str().starts_with("output_"))
            .filter(|id| output_order.get_one(id.as_str()) == Some(&true))
            .map(|id| (id.as_str(), None))
            .collect();
        Self {
            idx: 3, // 0 and 1 are for the path only remaining ones are generated dynamically
            order,
        }
    }

    fn next(&mut self, output: &str) {
        self.order
            .iter_mut()
            .find(|(key, _)| *key == output)
            .unwrap()
            .1 = Some(self.idx);
        self.idx += 1;
    }

    fn next_or_append(&mut self, output: &'static str) {
        if let Some(o) = self.order.iter_mut().find(|(key, _)| *key == output) {
            o.1 = Some(self.idx);
        } else {
            // If output of teehistorian_id wasn't requested, but do select-replay,
            // we still need it for persisting results
            self.order.push((output, Some(self.idx)));
        }
        self.idx += 1;
    }

    fn push_teehistorian_uuid(&mut self, output: &str) {
        self.order
            .iter_mut()
            .find(|(key, _)| *key == output)
            .unwrap()
            .1 = Some(2);
    }

    fn complete(&self) -> Vec<(&str, usize)> {
        self.order
            .iter()
            .map(|(output, idx)| (*output, idx.unwrap()))
            .collect()
    }
}

impl Select {
    pub fn new(data_dir: &Path) -> rusqlite::Result<Self> {
        let conn = Connection::open(data_dir.join("index-files.sqlite"))?;
        // Parsed and unified information about Teehistorian files
        conn.execute(
            "ATTACH DATABASE ? AS parsed",
            [&data_dir
                .join("index-parsed.sqlite")
                .as_os_str()
                .to_str()
                .unwrap()],
        )?;
        // meta information containing actual strings keeping parsed
        // table small
        conn.execute(
            "ATTACH DATABASE ? AS meta",
            [&data_dir
                .join("index-meta.sqlite")
                .as_os_str()
                .to_str()
                .unwrap()],
        )?;
        conn.execute(
            "ATTACH DATABASE ? AS run",
            [&data_dir.join("run.sqlite").as_os_str().to_str().unwrap()],
        )?;

        Ok(Self { conn })
    }

    pub fn teehistorian_files(
        &self,
        opt: SelectOpt,
        output_order: &ArgMatches,
        replay: Option<ReplayOptOuter>,
        persist_result: bool,
    ) {
        // store idx for of each column for output order
        let mut output_column_order = OutputColumnOrder::new(output_order);

        // build sqlite query
        let mut select = vec!["path", "compression", "uuid"];
        let mut from = "files JOIN meta.paths ON files.path_id = paths.path_id".to_owned();
        let mut need_teehistorian = false;
        let mut need_server_version = false;
        let mut need_map = false;
        let mut need_teehistorian_header = false;

        let mut conditions = vec![];
        let mut params = vec![];
        if let Some(from) = opt.teehistorian_id_from {
            conditions.push("files.teehistorian_id >= ?");
            params.push(Value::Integer(from));
        }
        if let Some(to) = opt.teehistorian_id_to {
            conditions.push("files.teehistorian_id <= ?");
            params.push(Value::Integer(to));
        }
        if let Some(id) = opt.teehistorian_id {
            conditions.push("files.teehistorian_id = ?");
            params.push(Value::Integer(id));
        }
        if let Some(after) = opt.after {
            need_teehistorian = true;
            let after = NaiveDate::parse_from_str(&after, "%Y-%m-%d").unwrap();
            let after = after.and_time(NaiveTime::MIN).and_utc();
            conditions.push("date >= ?");
            params.push(Value::Integer(after.timestamp()));
        }
        if let Some(before) = opt.before {
            need_teehistorian = true;
            let mut before = NaiveDate::parse_from_str(&before, "%Y-%m-%d").unwrap();
            // make this date inclusive
            before = before.checked_add_days(Days::new(1)).unwrap();
            let before = before.and_time(NaiveTime::MIN).and_utc();
            conditions.push("date < ?");
            params.push(Value::Integer(before.timestamp()));
        }
        if let Some(uuid) = opt.teehistorian_uuid {
            conditions.push("uuid = ?");
            params.push(Value::Blob(uuid.as_bytes().to_vec()));
        }
        if let Some(map_name) = opt.map_name {
            need_map = true;
            conditions.push("maps.name = ?");
            params.push(Value::Text(map_name));
        }
        if let Some(map_sha256) = opt.map_sha256 {
            assert_eq!(
                map_sha256.len(),
                64,
                "sha256 must be exact 64 characters long"
            );
            let map_sha256 = hex::decode(map_sha256).expect("valid sha256");
            params.push(Value::Blob(map_sha256));
        }
        if let Some(server_version) = opt.ddnet_version {
            need_server_version = true;
            conditions.push("version LIKE ?");
            params.push(Value::Text(server_version));
        }

        // configure output
        if opt.output_teehistorian_id || persist_result {
            select.push("files.teehistorian_id");
            output_column_order.next_or_append("output_teehistorian_id");
        }
        if opt.output_teehistorian_uuid {
            // teehistorian_uuid is always selected
            output_column_order.push_teehistorian_uuid("output_teehistorian_uuid");
        }
        if opt.output_time {
            need_teehistorian = true;
            select.push("date");
            output_column_order.next("output_time");
        }
        if opt.output_map_name {
            need_map = true;
            select.push("maps.name");
            output_column_order.next("output_map_name");
        }
        if opt.output_map_sha256 {
            need_map = true;
            select.push("maps.sha256");
            output_column_order.next("output_map_sha");
        }
        if opt.output_ddnet_version {
            need_server_version = true;
            select.push("version");
            output_column_order.next("output_ddnet_version");
        }
        if opt.output_teehistorian_header {
            need_teehistorian_header = true;
            select.push("header");
            output_column_order.next("output_teehistorian_header");
        }

        let output_column_order = output_column_order.complete();

        // add ORDER BY statements
        let order_by: Vec<_> = opt
            .order_by
            .iter()
            .map(|o| match o {
                SelectOrderBy::Id => "files.teehistorian_id",
                SelectOrderBy::IdDesc => "files.teehistorian_id DESC",
                SelectOrderBy::Uuid => "uuid",
                SelectOrderBy::UuidDesc => "uuid DESC",
                SelectOrderBy::MapName => {
                    need_map = true;
                    "maps.name"
                }
                SelectOrderBy::MapNameDesc => {
                    need_map = true;
                    "maps.name DESC"
                }
                SelectOrderBy::MapSha256 => {
                    need_map = true;
                    "maps.sha256"
                }
                SelectOrderBy::MapSha256Desc => {
                    need_map = true;
                    "maps.sha256 DESC"
                }
                SelectOrderBy::Time => {
                    need_teehistorian = true;
                    "date"
                }
                SelectOrderBy::TimeDesc => {
                    need_teehistorian = true;
                    "date DESC"
                }
                SelectOrderBy::DdnetVersion => {
                    need_server_version = true;
                    "version"
                }
                SelectOrderBy::DdnetVersionDesc => {
                    need_server_version = true;
                    "version DESC"
                }
            })
            .collect();

        // query necessary tables
        if need_teehistorian || need_map || need_server_version {
            from.push_str(
                " JOIN teehistorian ON files.teehistorian_id = teehistorian.teehistorian_id",
            );
        }
        if need_map {
            from.push_str(" JOIN maps ON teehistorian.map_id = maps.map_id");
        }
        if need_server_version {
            from.push_str(
                " JOIN server_versions ON teehistorian.version_id = server_versions.version_id",
            );
        }
        if need_teehistorian_header {
            from.push_str(" JOIN headers ON files.teehistorian_id = headers.teehistorian_id");
        }
        if opt.panicked {
            from.push_str(" JOIN run.failed ON files.teehistorian_id = run.failed.teehistorian_id");
        }
        if opt.pending {
            from.push_str(
                " JOIN run.pending ON files.teehistorian_id = run.pending.teehistorian_id",
            );
        }

        let select = select.join(", ");
        let mut query = format!("SELECT {select} FROM {from}");

        if !conditions.is_empty() {
            query.push_str(" WHERE ");
            query.push_str(&conditions.join(" AND "));
        }

        if !order_by.is_empty() {
            query.push_str(" ORDER BY ");
            query.push_str(&order_by.join(", "));
        }

        println!("{query};");

        let (map_service, replay_opt_inner) = if let Some(replay) = replay {
            (MapService::new(replay.map_service_opt), Some(replay.inner))
        } else {
            (MapService::new(MapServiceOpt::default()), None)
        };

        let mut stmt = self.conn.prepare(&query).unwrap();
        let mut results = stmt
            .query(rusqlite::params_from_iter(params.iter()))
            .unwrap();

        while let Some(row) = results.next().unwrap() {
            let mut persist_teehistorian_id = None;
            let path: String = row.get(0).unwrap();
            let compression: u8 = row.get(1).unwrap();
            let compression = Compression::try_from(compression).expect("unexpected compression");
            let compression = compression.to_file_extension();
            let uuid: [u8; 16] = row.get(2).unwrap();
            let uuid = Uuid::from_bytes(uuid);
            let teehistorian_file = format!("{path}/{uuid}.teehistorian{compression}");
            let mut output_line = teehistorian_file.to_string();

            for (output_name, idx) in output_column_order.iter() {
                match *output_name {
                    "output_teehistorian_id" => {
                        let teehistorian_id: i64 = row.get(*idx).unwrap();
                        if opt.output_teehistorian_id {
                            output_line.push('\t');
                            output_line.push_str(&teehistorian_id.to_string());
                        }
                        if persist_result {
                            persist_teehistorian_id = Some(teehistorian_id);
                        }
                    }
                    "output_teehistorian_uuid" => {
                        output_line.push('\t');
                        output_line.push_str(&uuid.to_string());
                    }
                    "output_time" => {
                        let timestamp: i64 = row.get(*idx).unwrap();
                        // Create a normal DateTime from the NaiveDateTime
                        let datetime: DateTime<Utc> =
                            DateTime::from_timestamp(timestamp, 0).unwrap();

                        // Format the datetime how you want
                        output_line.push('\t');
                        output_line.push_str(&datetime.format("%Y-%m-%d %H:%M:%S").to_string());
                    }
                    "output_map_name" => {
                        let map_name: String = row.get(*idx).unwrap();
                        output_line.push('\t');
                        output_line.push_str(&map_name);
                    }
                    "output_map_sha" => {
                        let map_sha: Vec<u8> = row.get(*idx).unwrap();
                        let map_sha = hex::encode(map_sha);
                        output_line.push('\t');
                        output_line.push_str(&map_sha);
                    }
                    "output_ddnet_version" => {
                        let ddnet_version: String = row.get(*idx).unwrap();
                        output_line.push('\t');
                        output_line.push_str(&ddnet_version);
                    }
                    "output_teehistorian_header" => {
                        // TODO: make header optional and don't always index the header
                        let header: String = row.get(*idx).unwrap();
                        output_line.push('\t');
                        output_line.push_str(&header);
                    }
                    _ => unimplemented!("unimplemented output {}", output_name),
                }
            }
            println!("{output_line}");
            if let Some(replay) = replay_opt_inner.as_ref() {
                replay_inner(
                    replay,
                    persist_teehistorian_id,
                    &teehistorian_file,
                    &map_service,
                );
            }
        }
    }
}
