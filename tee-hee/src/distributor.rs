use crate::index::Index;
use crate::map_service::MapService;
use crate::{ReplayOptInner, WorldKind};
use chrono::{DateTime, Utc};
use log::error;
use std::collections::HashMap;
use std::fs::{self, File};
use std::io::BufWriter;
use std::path::PathBuf;
use std::rc::Rc;
use teehistorian_replayer::teehistorian::Chunk;
use twgame::core::console::Command;
use twgame::core::database::Finishes;
use twgame::core::replay::{DemoChatWrite, DemoWrite};
use twgame::core::twsnap;
use twgame::core::twsnap::compat::ddnet::{DemoKind, DemoMapHash};
use twgame::core::twsnap::{compat::ddnet::DemoWriter, time::Instant};
use twgame::core::{
    net_msg,
    replay::{GameReplayerAll, ReplayerChecker},
    Game, Input, Snapper,
};
use twgame::{DdnetReplayerWorld, Map, ThHeader};

pub trait ReplayerResult: GameReplayerAll {
    /// called after all chunks are replayed.
    fn get_result(&mut self) -> Option<String>;
    fn persist_result(&self, world: WorldKind, teehistorian_id: i64);
}

pub struct DistributorWorld {
    // TODO: allow world implementations that don't require be called for all chunks/replay events, but stop early
    worlds: Vec<AnyWorld>,
}

impl DistributorWorld {
    fn get_map(
        map_service: &MapService,
        opt: &ReplayOptInner,
        header: &ThHeader,
    ) -> (Map, [u8; 32]) {
        if let Some(map) = &opt.map {
            map_service.get_path(map)
        } else {
            let sha256 = crate::th_map_sha256(header).expect("unkown sha256");
            map_service.get_sha256(sha256)
        }
    }

    pub fn new(
        map_service: &MapService,
        opt: &ReplayOptInner,
        header: &ThHeader,
    ) -> (DistributorWorld, Option<DistributorDemo>) {
        let mut worlds = Self { worlds: vec![] };
        let ddnet_compat = opt.world.iter().any(|w| *w == WorldKind::DdnetCompat);
        let twgame = opt.world.iter().any(|w| *w == WorldKind::Twgame);

        let mut demos = DistributorDemo::new();

        if ddnet_compat || opt.demo.is_some() {
            let (map, map_sha256) = Self::get_map(map_service, opt, header);

            // initialize world
            let mut world = twgame::DdnetReplayerWorld::new(Rc::new(map), true);
            world.configure_with_teehistorian_parameters(header);
            worlds.worlds.push(AnyWorld::DdnetCompat(world));

            // adding new demo always. If not necessary, will be removed with `finalize` call
            let demo_recorder = demos.add_new();

            if let Some(demo_path) = opt.demo.as_ref() {
                demo_recorder.init(demo_path, &header.start_time, &header.map_name, map_sha256);
            }
        }
        if twgame || opt.twgame_demo.is_some() {
            // TODO: deduplicte with above function!
            let (map, map_sha256) = Self::get_map(map_service, opt, header);

            // initialize world
            let mut world = twgame::DdnetReplayerWorld::new(Rc::new(map), false);
            world.configure_with_teehistorian_parameters(header);
            worlds.worlds.push(AnyWorld::TwGame(world));

            // adding new demo always. If not necessary, will be removed with `finalize` call
            let demo_recorder = demos.add_new();

            if let Some(demo_path) = opt.twgame_demo.as_ref() {
                demo_recorder.init(demo_path, &header.start_time, &header.map_name, map_sha256);
            }
        }
        (worlds, demos.finalize())
    }
}

enum AnyWorld {
    TwGame(DdnetReplayerWorld),
    DdnetCompat(DdnetReplayerWorld),
}

impl AnyWorld {
    fn kind(&self) -> WorldKind {
        match self {
            AnyWorld::DdnetCompat(_) => WorldKind::DdnetCompat,
            AnyWorld::TwGame(_) => WorldKind::Twgame,
        }
    }

    fn world_ref(&self) -> &dyn ReplayerResult {
        match self {
            AnyWorld::TwGame(w) => w,
            AnyWorld::DdnetCompat(w) => w,
        }
    }

    fn world_mut(&mut self) -> &mut dyn ReplayerResult {
        match self {
            AnyWorld::TwGame(w) => w,
            AnyWorld::DdnetCompat(w) => w,
        }
    }
}

impl Snapper for AnyWorld {
    fn snap(&self, snapshot: &mut twsnap::Snap) {
        match self {
            AnyWorld::DdnetCompat(w) => w.snap(snapshot),
            AnyWorld::TwGame(w) => w.snap(snapshot),
        }
    }
}

impl Game for DistributorWorld {
    fn player_join(&mut self, id: u32) {
        for w in self.worlds.iter_mut() {
            w.world_mut().player_join(id);
        }
    }
    fn player_ready(&mut self, id: u32) {
        for w in self.worlds.iter_mut() {
            w.world_mut().player_ready(id);
        }
    }
    fn player_input(&mut self, id: u32, input: &Input) {
        for w in self.worlds.iter_mut() {
            w.world_mut().player_input(id, input);
        }
    }
    fn player_leave(&mut self, id: u32) {
        for w in self.worlds.iter_mut() {
            w.world_mut().player_leave(id);
        }
    }

    fn on_net_msg(&mut self, id: u32, msg: &net_msg::ClNetMessage) {
        for w in self.worlds.iter_mut() {
            w.world_mut().on_net_msg(id, msg);
        }
    }
    fn on_command(&mut self, id: u32, command: &Command) {
        for w in self.worlds.iter_mut() {
            w.world_mut().on_command(id, command);
        }
    }

    fn swap_tees(&mut self, id1: u32, id2: u32) {
        for w in self.worlds.iter_mut() {
            w.world_mut().swap_tees(id1, id2);
        }
    }

    // actions before player position check in teehistorian
    fn tick(&mut self, cur_time: Instant) {
        for w in self.worlds.iter_mut() {
            w.world_mut().tick(cur_time);
        }
    }

    fn is_empty(&self) -> bool {
        self.worlds.iter().all(|w| w.world_ref().is_empty())
    }
}

impl ReplayerChecker for DistributorWorld {
    fn on_teehistorian_header(&mut self, header: &[u8]) {
        for w in self.worlds.iter_mut() {
            w.world_mut().on_teehistorian_header(header);
        }
    }
    fn on_teehistorian_chunk(&mut self, now: Instant, chunk: &Chunk) {
        for w in self.worlds.iter_mut() {
            w.world_mut().on_teehistorian_chunk(now, chunk);
        }
    }
    fn on_finish(&mut self, now: Instant, finish: &Finishes) {
        match finish {
            Finishes::FinishTee(tee) => error!("{now}: name={:?} time={}", tee.name, tee.time),
            Finishes::FinishTeam(team) => error!(
                "{now}: FinishTeam team={}, players={:?}, time={}",
                team.team, team.names, team.time
            ),
        }
        for w in self.worlds.iter_mut() {
            w.world_mut().on_finish(now, finish);
        }
    }
    fn check_tees(
        &mut self,
        cur_time: twsnap::time::Instant,
        tees: &[Option<twgame::core::replay::ReplayerTeeInfo>],
        demo: twgame::core::replay::DemoChatPtr,
    ) {
        let mut demo = demo;
        for w in self.worlds.iter_mut() {
            w.world_mut()
                .check_tees(cur_time, tees, demo.as_deref_mut())
        }
    }
    fn finalize(&mut self) {
        for w in self.worlds.iter_mut() {
            w.world_mut().finalize();
        }
    }
}

impl ReplayerResult for DistributorWorld {
    fn get_result(&mut self) -> Option<String> {
        let mut result = HashMap::new();
        for w in self.worlds.iter_mut() {
            if let Some(r) = w.world_mut().get_result() {
                assert_eq!(result.insert(w.kind(), r), None);
            }
        }
        if result.is_empty() {
            None
        } else {
            Some(serde_json::to_string(&result).unwrap())
        }
    }
    fn persist_result(&self, _world: WorldKind, teehistorian_id: i64) {
        for w in self.worlds.iter() {
            w.world_ref().persist_result(w.kind(), teehistorian_id);
        }
    }
}

struct MultiDemo {
    path: PathBuf,
    start_time: i64,
    map_name: String,
    map_sha256: [u8; 32],
}

struct DemoRecorder {
    /// If no demo is recorded during empty servers, put path where to put next demo
    /// file here.
    multi: Option<MultiDemo>,
    writer: Option<Box<DemoWriter>>,
}

impl DemoRecorder {
    fn new() -> Self {
        Self {
            multi: None,
            writer: None,
        }
    }

    fn init(&mut self, path: &str, start_time: &str, map_name: &str, map_sha256: [u8; 32]) {
        let start_time = Index::parse_teehistorian_date(start_time);

        if path.ends_with(".demo") {
            // one complete demo expected
            self.writer = Some(Self::initialize_demo(
                path,
                false,
                start_time,
                Instant::zero(),
                map_name,
                map_sha256,
            ));
        } else {
            // multiple demos expected
            fs::create_dir_all(path).expect("failed to create demo target dir");
            self.multi = Some(MultiDemo {
                path: PathBuf::from(&path),
                start_time,
                map_name: map_name.to_owned(),
                map_sha256,
            })
        }
    }

    fn is_disabled(&self) -> bool {
        self.multi.is_none() && self.writer.is_none()
    }

    fn initialize_demo(
        file: &str,
        is_dir: bool,
        time: i64,
        ticks: Instant,
        map_name: &str,
        map_sha256: [u8; 32],
    ) -> Box<DemoWriter> {
        let time = time + ticks.seconds() as i64;
        let datetime: DateTime<Utc> = DateTime::from_timestamp(time, 0).unwrap();
        let file_name = if is_dir {
            let snap_ticks = ticks.snap_tick();
            let timestamp = &datetime.format("%Y-%m-%dT%H-%M-%S").to_string();
            format!(
                "{file}/{timestamp}_{:02}-{:02}-{:02}.{:02}.demo",
                snap_ticks / 50 / 60 / 24,
                snap_ticks / 50 / 60 % 24,
                snap_ticks / 50 % 60,
                (snap_ticks % 50) * 2,
            )
        } else {
            file.to_owned()
        };
        error!("Writing demo to {file_name}");
        let to = BufWriter::new(File::create(file_name).unwrap());
        let time = time + ticks.seconds() as i64;
        let datetime: DateTime<Utc> = DateTime::from_timestamp(time, 0).unwrap();
        let start_time = &datetime.format("%Y-%m-%dT%H:%M:%S+00:00").to_string();
        let mut demo = Box::new(
            DemoWriter::new(
                to,
                DemoKind::Server,
                start_time,
                "0.6 TwGame", // TODO
                map_name,
                None,
                DemoMapHash::Sha256(map_sha256),
                0,
            )
            .unwrap(),
        );
        demo.write_chat(
            "Demo created by Teehistorian replayer TwGame https://gitlab.com/ddnet-rs/TwGame",
        )
        .unwrap();
        demo
    }

    fn on_empty(&mut self, cur_time: Instant) {
        // TODO: add future snap to continue demo for a bit?
        if self.multi.is_some() {
            // drop demo and assert that we drop it
            self.writer.take().expect("should have demo");
            error!("{}: Stopped demo due to empty server", cur_time)
        }
    }
    fn on_non_empty(&mut self, cur_time: Instant) {
        if let Some(multi) = self.multi.as_ref() {
            assert!(self.writer.is_none());
            // one complete demo expected
            self.writer = Some(Self::initialize_demo(
                multi.path.as_os_str().to_str().unwrap(),
                true,
                multi.start_time,
                cur_time,
                &multi.map_name,
                multi.map_sha256,
            ));
        }
    }
}

pub struct DistributorDemo {
    /// The index must match up with DistributorWorld:
    /// The first DemoRecorder corresponds to the first DistributorWorld
    demos: Vec<DemoRecorder>,
}

impl DistributorDemo {
    fn new() -> Self {
        Self { demos: vec![] }
    }

    fn add_new(&mut self) -> &mut DemoRecorder {
        self.demos.push(DemoRecorder::new());
        self.demos.last_mut().unwrap()
    }

    /// removes disabled from the end and returns None if no demos are being recorded
    fn finalize(mut self) -> Option<Self> {
        while let Some(last) = self.demos.last() {
            if last.is_disabled() {
                self.demos.pop().unwrap();
            } else {
                break;
            }
        }
        if self.demos.is_empty() {
            None
        } else {
            Some(self)
        }
    }

    pub fn on_empty(&mut self, cur_time: Instant) {
        for d in self.demos.iter_mut() {
            d.on_empty(cur_time)
        }
    }
    pub fn on_non_empty(&mut self, cur_time: Instant) {
        for d in self.demos.iter_mut() {
            d.on_non_empty(cur_time)
        }
    }
}

impl DemoWrite<DistributorWorld> for DistributorDemo {
    fn snap_and_write(
        &mut self,
        tick: twsnap::time::Instant,
        world: &DistributorWorld,
        snap_buf: &mut twsnap::Snap,
    ) -> Result<(), twsnap::compat::ddnet::WriteError> {
        for (demo, w) in self.demos.iter_mut().zip(world.worlds.iter()) {
            if let Some(demo) = demo.writer.as_mut() {
                demo.snap_and_write(tick, w, snap_buf)?
            }
        }
        Ok(())
    }

    fn chat(&mut self) -> &mut (dyn DemoChatWrite + 'static) {
        self
    }
}
impl DemoChatWrite for DistributorDemo {
    fn write_chat(&mut self, msg: &str) -> Result<(), twsnap::compat::ddnet::WriteError> {
        for demo in self.demos.iter_mut() {
            if let Some(demo) = demo.writer.as_mut() {
                demo.write_chat(msg)?
            }
        }
        Ok(())
    }
    fn write_player_chat(
        &mut self,
        player_id: i32,
        msg: &str,
    ) -> Result<(), twsnap::compat::ddnet::WriteError> {
        for demo in self.demos.iter_mut() {
            if let Some(demo) = demo.writer.as_mut() {
                demo.write_player_chat(player_id, msg)?
            }
        }
        Ok(())
    }
}
