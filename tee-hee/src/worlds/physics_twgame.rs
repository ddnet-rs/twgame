use crate::{app_dir, distributor::ReplayerResult, WorldKind};
use rusqlite::Connection;
use twgame::DdnetReplayerWorld;

fn setup_database(compat: bool) -> Connection {
    let file_name = if compat {
        "world_ddnet_compat.sqlite"
    } else {
        "world_twgame.sqlite"
    };
    let path = app_dir().join(file_name);
    let conn = rusqlite::Connection::open(path).unwrap();
    conn.execute(
        "CREATE TABLE IF NOT EXISTS result (\
                teehistorian_id INTEGER NOT NULL, \
                success INTEGER NOT NULL\
            ) STRICT;",
        [],
    )
    .unwrap();
    conn.execute(
        "CREATE TABLE IF NOT EXISTS saves (\
                teehistorian_id INTEGER NOT NULL, \
                save_code TEXT NOT NULL, \
                save_uuid BLOB NOT NULL, \
                tick INTEGER NOT NULL, \
                success INTEGER NOT NULL\
            ) STRICT;",
        [],
    )
    .unwrap();
    conn.execute(
        "CREATE TABLE IF NOT EXISTS loads (\
                teehistorian_id INTEGER NOT NULL, \
                code TEXT NOT NULL, \
                uuid TEXT NOT NULL, \
                tick INTEGER NOT NULL\
            ) STRICT;",
        [],
    )
    .unwrap();

    conn.execute(
        "CREATE TABLE IF NOT EXISTS finishes (\
                teehistorian_id INTEGER NOT NULL, \
                player TEXT NOT NULL, \
                player_id TEXT NOT NULL, \
                start_tick INTEGER NOT NULL, \
                time INTEGER NOT NULL\
            ) STRICT;",
        [],
    )
    .unwrap();
    conn.execute(
        "CREATE TABLE IF NOT EXISTS team_finishes (\
                teehistorian_id INTEGER NOT NULL, \
                players TEXT NOT NULL, \
                team_id INTEGER NOT NULL, \
                start_tick INTEGER NOT NULL, \
                time INTEGER NOT NULL\
            ) STRICT;",
        [],
    )
    .unwrap();
    conn.execute(
        "CREATE TABLE IF NOT EXISTS bug_use (\
                teehistorian_id INTEGER PRIMARY KEY, \
                bug_use TEXT\
            ) STRICT;",
        [],
    )
    .unwrap();
    if compat {
        conn.execute(
            "CREATE TABLE IF NOT EXISTS tele_data (\
                    teehistorian_id INTEGER PRIMARY KEY, \
                    tele_compat TEXT\
                ) STRICT;",
            [],
        )
        .unwrap();
    }
    conn
}
impl ReplayerResult for DdnetReplayerWorld {
    fn get_result(&mut self) -> Option<String> {
        let correct = self.is_correct();
        Some(format!("{{ \"correct\": {correct} }}"))
    }

    fn persist_result(&self, _world: WorldKind, _teehistorian_id: i64) {
        let _conn = setup_database(true); // TODO: get compat from TwGameReplayer
    }
}
