#![allow(dead_code)]
use crate::distributor::ReplayerResult;
use core::str;
use std::collections::HashMap;
use teehistorian_replayer::teehistorian;
use twgame::core::twsnap::time::Instant;
use twgame::core::{
    console::Command,
    database::{Finishes, SaveTeam},
    net_msg,
    replay::ReplayerChecker,
    Game, Input,
};

/// world gathering information about teehistorian chunks
#[derive(Default)]
struct InfoWorld {
    header: String,
    /// number of bytes per chunk type
    chunk_size_histogram: HashMap<u8, HashMap<u32, u32>>,
    // non-zero InputDiff fields
    input_diff: HashMap<u16, i64>,
    finishes: Vec<Finishes>,
    saves: Vec<SaveTeam>,
    loads: Vec<SaveTeam>,
    /// number of ticks on this teehistorian file
    length: i64,
    /// in ticks
    play_time: i64,
    /// Players on the server, client version
    players: Vec<(String, String, Instant)>,
    // buf reuse for serializing chunk
    buf: Vec<u8>,
}

impl InfoWorld {
    fn new() -> Self {
        Self::default()
    }
}

impl Game for InfoWorld {
    fn player_join(&mut self, _id: u32) {}
    fn player_ready(&mut self, _id: u32) {}
    fn player_input(&mut self, _id: u32, _input: &Input) {}
    fn player_leave(&mut self, _id: u32) {}

    fn on_net_msg(&mut self, _id: u32, _msg: &net_msg::ClNetMessage) {}
    fn on_command(&mut self, _id: u32, _command: &Command) {}

    fn swap_tees(&mut self, _id1: u32, _id2: u32) {}

    fn tick(&mut self, _cur_time: Instant) {}

    fn is_empty(&self) -> bool {
        true
    }
}

impl ReplayerChecker for InfoWorld {
    fn on_teehistorian_header(&mut self, header: &[u8]) {
        self.header = str::from_utf8(header)
            .expect("header should be valid utf-8")
            .to_owned();
    }
    fn on_teehistorian_chunk(&mut self, _now: Instant, chunk: &teehistorian::Chunk) {
        let c = chunk.discriminant();
        self.buf.clear();
        chunk
            .serialize_into(&mut self.buf)
            .expect("failed to serialize");
        let len = self.buf.len() as u32;
        *self
            .chunk_size_histogram
            .entry(c)
            .or_default()
            .entry(len)
            .or_default() += 1;
        if let teehistorian::Chunk::InputDiff(input) = chunk {
            let mut non_zeros = 0;
            for (i, inp) in input.dinput.into_iter().enumerate() {
                if inp != 0 {
                    non_zeros |= 1 << i;
                }
            }
            *self.input_diff.entry(non_zeros).or_default() += 1;
        }
    }
    fn on_finish(&mut self, _now: Instant, _finish: &Finishes) {}
    fn check_tees(
        &mut self,
        _cur_time: Instant,
        _tees: &[Option<twgame::core::replay::ReplayerTeeInfo>],
        _demo: twgame::core::replay::DemoChatPtr,
    ) {
    }
    fn finalize(&mut self) {}
}

impl ReplayerResult for InfoWorld {
    fn get_result(&mut self) -> Option<String> {
        todo!()
    }
    fn persist_result(&self, _world: crate::WorldKind, _teehistorian_id: i64) {}
}
