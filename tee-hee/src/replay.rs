use crate::distributor::{DistributorWorld, ReplayerResult};
use crate::map_service::MapService;
use crate::{decompress, ReplayOpt, ReplayOptInner, WorldKind};
use log::{debug, info, log_enabled, warn};
use teehistorian_replayer::teehistorian::{self, Chunk, ThCompat, ThStream};
use teehistorian_replayer::ThReplayer;
use twgame::core::Game;
use twgame::core::{replay::DemoWrite, DisplayChunk};
use twgame::ThHeader;

pub fn replay(opt: ReplayOpt) {
    env_logger::Builder::new()
        .filter_level(opt.verbose.log_level_filter())
        .target(env_logger::Target::Stdout)
        .format_level(false)
        .format_target(false)
        .format_timestamp(None)
        .init();
    let map_service = MapService::new(opt.outer.map_service_opt);
    replay_inner(&opt.outer.inner, None, &opt.teehistorian, &map_service);
}

pub fn replay_inner(
    opt: &ReplayOptInner,
    persist_results: Option<i64>,
    teehistorian_file: &str,
    map_service: &MapService,
) -> Option<String> {
    let th = decompress::th_buf_reader(teehistorian_file).unwrap();
    let mut th = ThCompat::parse(th).unwrap();
    let header_raw = th.header().unwrap();
    let header = ThHeader::from_buf(header_raw);

    let (mut world, mut demo) = DistributorWorld::new(map_service, opt, &header);

    let mut replayer = ThReplayer::new(header_raw, &mut world);

    let mut empty = true;

    loop {
        let demo_trait_obj: Option<&mut dyn DemoWrite<DistributorWorld>> = demo
            .as_mut()
            .map(|d| d as &mut dyn DemoWrite<DistributorWorld>);
        match th.next_chunk() {
            Ok(chunk) => {
                if log_enabled!(log::Level::Debug) {
                    let pretty_chunk = DisplayChunk(&chunk);
                    debug!("{}: {pretty_chunk}", replayer.cur_time());
                } else if log_enabled!(log::Level::Warn) && matches!(chunk, Chunk::PlayerName(_)) {
                    let pretty_chunk = DisplayChunk(&chunk);
                    warn!("{}: {pretty_chunk}", replayer.cur_time());
                }
                replayer.replay_next_chunk(&mut world, Some(chunk), demo_trait_obj);
            }
            Err(teehistorian::Error::Eof) => {
                // Eos chunk missing?, don't panic. It's fine.
                replayer.replay_next_chunk(&mut world, None, demo_trait_obj);
                break;
            }
            Err(err) => {
                info!("teehistorian_chunk_err {err}");
                replayer.replay_next_chunk(&mut world, None, demo_trait_obj);
                break;
            }
        }

        // start/stop demo on multi-file recordings
        if let Some(demo) = demo.as_mut() {
            let replayer_empty = replayer.is_empty() && world.is_empty();
            if empty != replayer_empty {
                empty = replayer_empty;
                if empty {
                    demo.on_empty(replayer.cur_time());
                } else {
                    demo.on_non_empty(replayer.cur_time());
                }
            }
        }
    }
    let res = world.get_result();
    if let Some(res) = &res {
        if opt.print_result {
            println!("{res}");
        }
    }
    if let Some(teehistorian_id) = persist_results {
        // World argument is ignored by `DistributorWorld`, only relevant for
        // underlying worlds
        world.persist_result(WorldKind::Twgame, teehistorian_id);
    }
    res
}
