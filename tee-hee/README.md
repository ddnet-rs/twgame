# Tee-Hee

Replay, index and detect bugs in DDNet Teehistorian files.

Tee-Hee transparently decompresses (.xz, .zst) Teehistorian files. Some commands require an index to efficiently go through all or a subset of Teehistorian files. To index Teehistorian files they need to be named in the form
"{game_uuid}.teehistorian{,.xz,.zst}". After a Teehistorian file got indexed, Tee-Hee expects the file to stay at its location.

```
Usage: tee-hee <COMMAND>

Commands:
  replay                 Replays Teehistorian file, generates DDNet demo file
  cat                    Prints Teehistorian header and all Teehistorian chunks
  header                 Prints Teehistorian header
  run                    Indexes new files from passed directory and runs all world implementations on all indexed Teehistorian files
  index                  Indexes new files from passed directory
  select                 Selects Teehistorian files from the index based on specified criteria
  select-replay          Replays Teehistorian files selected from the index based on specified criteria
  status                 Prints current status of replayed runs via `tee-hee run`
  maps-rename-to-sha256  Helper command to create a map cache compatible with Tee-Hee
  help                   Print this message or the help of the given subcommand(s)
```

## Install

On Debian 12:

1. Install dependencies: `sudo apt install pkg-config libssl-dev cmake rustc-web`
2. Install tee-hee either
    1. from [crates.io](https://crates.io/crate/tee-hee): `cargo install --locked tee-hee`
    2. from [master](https://gitlab.com/ddnet-rs/twgame) (clone of the repository required):
        * `git clone https://gitlab.com/ddnet-rs/twgame.git`
        * `cd twgame`
        * `cargo install --locked --path tee-hee/`
3. Make sure `~/.cargo/bin` is in your path.

## Uninstall

Removing the binary and the data directory:

```
rm ~/.cargo/bin/tee-hee
rm -r ~/.local/share/tee-hee
```
