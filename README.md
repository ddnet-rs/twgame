# TwGame

This repository contains multiple [DDNet](https://ddnet.org) related libraries and tools.
It is based on [libtw2](https://github.com/heinrich5991/libtw2), [twmap](https://gitlab.com/Patiga/TwMap) and [twsnap](https://gitlab.com/ddnet-rs/twsnap).

## Components

- [Tee-Hee](tee-hee/): command line tool for working with teehistorian files including converting them to demo.
- [DDNet physics](twgame/)
- [Unit tests for physics](replayer/res/)
- [Teehistorian replayer library](replayer/)

## Future plans

- Allow using DDNet C++ physics via Rust bindings
- Python binding to work with teehistorian files
- Demo reader allowing conversion to teehistorian
- Make a server wrapper around the game implementation using libtw2 network library ([tickle](tickle/))
- Allow rendering teehistorian files with [twgpu](https://gitlab.com/Patiga/twgpu) without intermediate demo step

## MSRV-Policy

- MSRV can be bumped up to the latest version available on debian: https://packages.debian.org/stable/rustc-web
- Current MSRV: 1.74.1
