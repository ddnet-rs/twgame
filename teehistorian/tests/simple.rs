use teehistorian::{Chunk, ErrorKind, Th, ThBufReader};
use uuid::Uuid;

fn buf_read_th(slice: &[u8]) -> Th<ThBufReader<&[u8]>> {
    let br = ThBufReader::with_capacity(8, slice);
    Th::parse(br).unwrap()
}

#[test]
fn two_header_requests() {
    let input = b"\x69\x9d\xb1\x7b\x8e\xfb\x34\xff\xb1\xd8\xda\x6f\x60\xc1\x5d\xd1\
               {\"version\":\"2\"}\x00\
               \x40";
    let mut th = buf_read_th(input);
    assert_eq!(th.header().unwrap(), br#"{"version":"2"}"#);
    assert_eq!(th.header().unwrap(), br#"{"version":"2"}"#);
    assert_eq!(th.next_chunk().unwrap(), Chunk::Eos);
    assert!(th.next_chunk().unwrap_err().is_eof());
}

#[test]
fn without_header_requests() {
    let input = b"\x69\x9d\xb1\x7b\x8e\xfb\x34\xff\xb1\xd8\xda\x6f\x60\xc1\x5d\xd1\
               {\"version\":\"2\"}\x00\
               \x40";
    let mut th = Th::parse(&input[..]).unwrap();
    assert_eq!(th.next_chunk().unwrap(), Chunk::Eos);
    assert!(th.next_chunk().unwrap_err().is_eof());
}

#[test]
fn multiple_chunks() {
    let input = b"\x69\x9d\xb1\x7b\x8e\xfb\x34\xff\xb1\xd8\xda\x6f\x60\xc1\x5d\xd1\
               {\"version\":\"2\"}\x00\
               \x40\x40\x40";
    let mut th = Th::parse(&input[..]).unwrap();
    assert_eq!(th.header().unwrap(), br#"{"version":"2"}"#);
    assert_eq!(th.next_chunk().unwrap(), Chunk::Eos);
    assert_eq!(th.next_chunk().unwrap(), Chunk::Eos);
    assert_eq!(th.next_chunk().unwrap(), Chunk::Eos);
    assert!(th.next_chunk().unwrap_err().is_eof());
}

#[test]
fn reset_chunk() {
    let input = b"\x69\x9d\xb1\x7b\x8e\xfb\x34\xff\xb1\xd8\xda\x6f\x60\xc1\x5d\xd1\
               {\"version\":\"2\"}\x00\
               \x40";
    let mut th = Th::parse(&input[..]).unwrap();
    assert_eq!(th.next_chunk().unwrap(), Chunk::Eos);
    th.reset_chunk();
    assert_eq!(th.next_chunk().unwrap(), Chunk::Eos);
    th.reset_chunk();
    assert_eq!(th.next_chunk().unwrap(), Chunk::Eos);
    assert!(th.next_chunk().unwrap_err().is_eof());
}

#[test]
fn too_small() {
    assert_eq!(
        Th::parse(&b"."[..]).unwrap_err().parse_error(),
        Some(&ErrorKind::NotTeehistorian(None))
    );
}

#[test]
fn empty() {
    assert_eq!(
        Th::parse(&b""[..]).unwrap_err().parse_error(),
        Some(&ErrorKind::NotTeehistorian(None))
    );
}

#[test]
fn invalid_uuid() {
    assert_eq!(
        Th::parse(&b"........................................"[..])
            .unwrap_err()
            .parse_error(),
        Some(&ErrorKind::NotTeehistorian(Some(Uuid::from_bytes(
            *b"................"
        ))))
    );
}
