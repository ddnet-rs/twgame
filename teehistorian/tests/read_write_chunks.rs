use core::str;

use teehistorian::{
    chunks::{
        Antibot, Auth, ConsoleCommand, DdnetVersion, DdnetVersionOld, Drop, InputDiff, InputNew,
        NetMessage, PlayerDiff, PlayerName, PlayerNew, TeamSave, UnknownEx,
    },
    Chunk, Error, Th, ThWriter,
};
use uuid::Uuid;

fn test_write_read(chunk: &Chunk<'static>) {
    let mut th_buf = vec![];
    // write chunk
    {
        let mut writer = ThWriter::new(&mut th_buf, "test").unwrap();
        writer.add_chunk(chunk).unwrap();
    }
    // read chunk
    {
        let mut reader = Th::parse(&th_buf[..]).unwrap();
        let header = str::from_utf8(reader.header().unwrap()).unwrap();
        assert_eq!(header, "test");
        let read_chunk = reader.next_chunk().unwrap();
        assert_eq!(chunk, &read_chunk);
        // try if resetting chunk still yields the same result
        reader.reset_chunk();
        let read_chunk = reader.next_chunk().unwrap();
        assert_eq!(chunk, &read_chunk);
        let read_error = reader.next_chunk().expect_err("Expect Eof");
        assert!(matches!(read_error, Error::Eof), "Expect Eof");
    }
}

#[test]
fn player_diff_0() {
    let chunk = Chunk::PlayerDiff(PlayerDiff {
        cid: 0,
        dx: 11,
        dy: 17,
    });
    test_write_read(&chunk);
}

#[test]
#[should_panic]
fn player_diff_minus1() {
    let chunk = Chunk::PlayerDiff(PlayerDiff {
        cid: -1,
        dx: 11,
        dy: 17,
    });
    test_write_read(&chunk);
}

#[test]
fn player_diff_63() {
    let chunk = Chunk::PlayerDiff(PlayerDiff {
        cid: 63,
        dx: 0,
        dy: 23,
    });
    test_write_read(&chunk);
}

#[test]
fn player_diff_1000() {
    let chunk = Chunk::PlayerDiff(PlayerDiff {
        cid: 1000,
        dx: 41,
        dy: -12,
    });
    test_write_read(&chunk);
}

#[test]
fn eos() {
    let chunk = Chunk::Eos;
    test_write_read(&chunk);
}

#[test]
fn tick_skip_0() {
    let chunk = Chunk::TickSkip { dt: 0 };
    test_write_read(&chunk);
}

#[test]
fn tick_skip_11() {
    let chunk = Chunk::TickSkip { dt: 11 };
    test_write_read(&chunk);
}

#[test]
fn player_new() {
    let chunk = Chunk::PlayerNew(PlayerNew {
        cid: 10,
        x: -100,
        y: 100,
    });
    test_write_read(&chunk);
}

#[test]
fn player_old() {
    let chunk = Chunk::PlayerOld { cid: 63 };
    test_write_read(&chunk);
}

#[test]
fn input_diff() {
    let chunk = Chunk::InputDiff(InputDiff {
        cid: 55,
        dinput: [1, -2, 4, -8, 16, -32, 64, -128, 256, -512],
    });
    test_write_read(&chunk);
}

#[test]
fn input_new() {
    let chunk = Chunk::InputNew(InputNew {
        cid: 56,
        input: [-511, 255, -127, 63, -31, 15, -7, 3, -1, 0],
    });
    test_write_read(&chunk);
}

#[test]
fn net_msg() {
    let chunk = Chunk::NetMessage(NetMessage {
        cid: -199,
        msg: b"abcdefghijklmnopqrstuvwxyz",
    });
    test_write_read(&chunk);
}

#[test]
fn join() {
    let chunk = Chunk::Join { cid: 10000000 };
    test_write_read(&chunk);
}

#[test]
fn drop() {
    let chunk = Chunk::Drop(Drop {
        cid: 10000000,
        reason: b":-)",
    });
    test_write_read(&chunk);
}

#[test]
fn console_command() {
    let chunk = Chunk::ConsoleCommand(ConsoleCommand {
        cid: 21,
        flags: 1,
        cmd: b"tee-hee",
        args: vec![b"index", b"everything"],
    });
    test_write_read(&chunk);
}

#[test]
fn unknown_ex() {
    let chunk = Chunk::UnknownEx(UnknownEx {
        uuid: Uuid::from_u128(0xb7df595f_f6f1_4a5e_8320_64699ccf602d),
        data: b"here we go",
    });
    test_write_read(&chunk);
}

#[test]
fn test() {
    let chunk = Chunk::Test;
    test_write_read(&chunk);
}

#[test]
fn ddnet_version_old() {
    let chunk = Chunk::DdnetVersionOld(DdnetVersionOld {
        cid: 101,
        version: 20002,
    });
    test_write_read(&chunk);
}

#[test]
fn ddnet_version() {
    let chunk = Chunk::DdnetVersion(DdnetVersion {
        cid: 63,
        connection_id: Uuid::from_u128(0x026e8910_fa52_4b2d_90e9_43f773c05b2b),
        version: 30003,
        version_str: b"30.0.3",
    });
    test_write_read(&chunk);
}

#[test]
fn auth_init() {
    let chunk = Chunk::AuthInit(Auth {
        cid: 404,
        level: 1001,
        auth_name: b"tee",
    });
    test_write_read(&chunk);
}

#[test]
fn auth_login() {
    let chunk = Chunk::AuthLogin(Auth {
        cid: -99,
        level: -9999,
        auth_name: b"hee",
    });
    test_write_read(&chunk);
}

#[test]
fn auth_logout() {
    let chunk = Chunk::AuthLogout { cid: 1234 };
    test_write_read(&chunk);
}

#[test]
fn join_ver_6() {
    let chunk = Chunk::JoinVer6 { cid: 54321 };
    test_write_read(&chunk);
}

#[test]
fn join_ver_7() {
    let chunk = Chunk::JoinVer7 { cid: 12345 };
    test_write_read(&chunk);
}

#[test]
fn rejoin_ver_6() {
    let chunk = Chunk::RejoinVer6 { cid: 65536 };
    test_write_read(&chunk);
}

#[test]
fn team_save_success() {
    let chunk = Chunk::TeamSaveSuccess(TeamSave {
        team: -1000000,
        save_id: Uuid::from_u128(0x026e8910_fa52_4b2d_90e9_43f773c05b2b),
        save: b"realllllllllllllllllllllllllllllllllllllllllllllly long game state",
    });
    test_write_read(&chunk);
}

#[test]
fn team_save_failure() {
    let chunk = Chunk::TeamSaveFailure { team: -1000000 };
    test_write_read(&chunk);
}

#[test]
fn team_load_success() {
    let chunk = Chunk::TeamLoadSuccess(TeamSave {
        team: -1000000,
        save_id: Uuid::from_u128(0x026e8910_fa52_4b2d_90e9_43f773c05b2b),
        save: b"realllllllllllllllllllllllllllllllllllllllllllllly long game state",
    });
    test_write_read(&chunk);
}

#[test]
fn team_load_failure() {
    let chunk = Chunk::TeamLoadFailure { team: -1000000 };
    test_write_read(&chunk);
}

#[test]
fn player_team() {
    let chunk = Chunk::PlayerTeam { cid: -11, team: 8 };
    test_write_read(&chunk);
}

#[test]
fn team_practice() {
    let chunk = Chunk::TeamPractice {
        team: -21,
        practice: 1,
    };
    test_write_read(&chunk);
}

#[test]
fn player_ready() {
    let chunk = Chunk::PlayerReady { cid: 2 };
    test_write_read(&chunk);
}

#[test]
fn player_swap() {
    let chunk = Chunk::PlayerSwap { cid1: 0, cid2: 1 };
    test_write_read(&chunk);
}

#[test]
fn antibot() {
    let chunk = Chunk::Antibot(Antibot { data: b"hehe" });
    test_write_read(&chunk);
}

#[test]
fn player_name() {
    let chunk = Chunk::PlayerName(PlayerName {
        cid: -128,
        name: b"nameless tee",
    });
    test_write_read(&chunk);
}

#[test]
fn player_finish() {
    let chunk = Chunk::PlayerFinish {
        cid: 22,
        time: i32::MAX,
    };
    test_write_read(&chunk);
}

#[test]
fn team_finish() {
    let chunk = Chunk::TeamFinish {
        team: i32::MIN,
        time: 1,
    };
    test_write_read(&chunk);
}
