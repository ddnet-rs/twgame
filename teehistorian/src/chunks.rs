use crate::error::ErrorKind;
use crate::parser::{cstring, twint, uuid};
use crate::ser::Packer;
use nom::bytes::streaming::take;
use nom::multi::count;
use nom::Err::Failure;
use nom::IResult;
use serde::Serialize;
use uuid::Uuid;

fn player_input(input: &[u8]) -> IResult<&[u8], [i32; 10], ErrorKind> {
    let (input, i0) = twint(input)?;
    let (input, i1) = twint(input)?;
    let (input, i2) = twint(input)?;
    let (input, i3) = twint(input)?;
    let (input, i4) = twint(input)?;
    let (input, i5) = twint(input)?;
    let (input, i6) = twint(input)?;
    let (input, i7) = twint(input)?;
    let (input, i8) = twint(input)?;
    let (input, i9) = twint(input)?;
    Ok((input, [i0, i1, i2, i3, i4, i5, i6, i7, i8, i9]))
}

#[derive(Debug, PartialEq, Serialize)]
pub struct PlayerDiff {
    /// integer in range 0..
    pub cid: i32,
    pub dx: i32,
    pub dy: i32,
}

#[derive(Debug, PartialEq, Serialize)]
pub struct PlayerNew {
    pub cid: i32,
    pub x: i32,
    pub y: i32,
}

#[derive(Debug, PartialEq, Serialize)]
pub struct InputDiff {
    pub cid: i32,
    /// 10 integers
    pub dinput: [i32; 10],
}

#[derive(Debug, PartialEq, Serialize)]
pub struct InputNew {
    pub cid: i32,
    /// 10 integers
    pub input: [i32; 10],
}

#[derive(Debug, PartialEq, Serialize)]
pub struct NetMessage<'a> {
    pub cid: i32,
    #[serde(serialize_with = "crate::ser::write_size_first")]
    pub msg: &'a [u8],
}

#[derive(Debug, PartialEq, Serialize)]
pub struct Drop<'a> {
    pub cid: i32,
    #[serde(serialize_with = "crate::ser::null_terminator")]
    pub reason: &'a [u8],
}

#[derive(Debug, PartialEq, Serialize)]
pub struct ConsoleCommand<'a> {
    pub cid: i32,
    pub flags: i32,
    #[serde(serialize_with = "crate::ser::null_terminator")]
    pub cmd: &'a [u8],
    #[serde(serialize_with = "crate::ser::write_size_first_and_inner_null_terminator")]
    pub args: Vec<&'a [u8]>,
}

#[derive(Debug, PartialEq, Serialize)]
pub struct UnknownEx<'a> {
    #[serde(with = "uuid::serde::compact")]
    pub uuid: Uuid,
    #[serde(serialize_with = "crate::ser::write_size_first")]
    pub data: &'a [u8],
}

#[derive(Debug, PartialEq, Serialize)]
pub struct DdnetVersionOld {
    pub cid: i32,
    pub version: i32,
}

#[derive(Debug, PartialEq, Serialize)]
pub struct DdnetVersion<'a> {
    pub cid: i32,
    #[serde(with = "uuid::serde::compact")]
    pub connection_id: Uuid,
    pub version: i32,
    #[serde(serialize_with = "crate::ser::null_terminator")]
    pub version_str: &'a [u8],
}

#[derive(Debug, PartialEq, Serialize)]
pub struct TeamSave<'a> {
    pub team: i32,
    #[serde(with = "uuid::serde::compact")]
    pub save_id: Uuid,
    #[serde(serialize_with = "crate::ser::null_terminator")]
    pub save: &'a [u8],
}

#[derive(Debug, PartialEq, Serialize)]
pub struct Auth<'a> {
    pub cid: i32,
    pub level: i32,
    #[serde(serialize_with = "crate::ser::null_terminator")]
    pub auth_name: &'a [u8],
}

#[derive(Debug, PartialEq, Serialize)]
pub struct Antibot<'a> {
    pub data: &'a [u8],
}

#[derive(Debug, PartialEq, Serialize)]
pub struct PlayerName<'a> {
    pub cid: i32,
    #[serde(serialize_with = "crate::ser::null_terminator")]
    pub name: &'a [u8],
}

#[derive(Debug, PartialEq)]
// due to discriminant being public, new chunks should be only added to the end of the list
// or tee-hee statistics about chunks needs to be taken care of.
#[repr(u8)]
pub enum Chunk<'a> {
    /// for file size optimization in the binary representation the tag is left out.
    /// Instead, the cid is tag (0-63). In the Teeworlds variable-width integer all these numbers
    /// have a width of one byte
    PlayerDiff(PlayerDiff),
    /// End of teehistorian chunk stream (tag -1)
    Eos,
    /// tag: -2: records that there were `dt` ticks in which nothing happened,
    /// i.e. `next_tick = last_tick + dt + 1`
    /// Should be >= 0
    TickSkip { dt: i32 },
    /// tag: -3
    PlayerNew(PlayerNew),
    /// tag: -4
    PlayerOld { cid: i32 },
    /// tag -5
    InputDiff(InputDiff),
    /// tag: -6
    InputNew(InputNew),
    /// tag: -7
    NetMessage(NetMessage<'a>),
    /// tag: -8, contains cid
    Join { cid: i32 },
    /// tag: -9
    Drop(Drop<'a>),
    /// tag: -10
    ConsoleCommand(ConsoleCommand<'a>),
    /// tag: -11
    UnknownEx(UnknownEx<'a>),
    /// `teehistorian-test@ddnet.tw` test extension used in debug mode
    Test,
    /// `teehistorian-ddnetver-old@ddnet.tw`
    DdnetVersionOld(DdnetVersionOld),
    /// `teehistorian-ddnetver@ddnet.tw`
    DdnetVersion(DdnetVersion<'a>),
    /// `teehistorian-auth-init@ddnet.tw`
    AuthInit(Auth<'a>),
    /// `teehistorian-auth-login@ddnet.tw`
    AuthLogin(Auth<'a>),
    /// `teehistorian-auth-logout@ddnet.tw`
    AuthLogout { cid: i32 },
    /// `teehistorian-joinver6@ddnet.tw`
    JoinVer6 { cid: i32 },
    /// `teehistorian-joinver7@ddnet.tw`
    JoinVer7 { cid: i32 },
    /// `teehistorian-rejoinver6@ddnet.org`
    RejoinVer6 { cid: i32 },
    /// `teehistorian-save-success@ddnet.tw`
    TeamSaveSuccess(TeamSave<'a>),
    /// `teehistorian-save-failure@ddnet.tw`
    TeamSaveFailure { team: i32 },
    /// `teehistorian-load-success@ddnet.tw`
    TeamLoadSuccess(TeamSave<'a>),
    /// `teehistorian-load-failure@ddnet.tw`
    TeamLoadFailure { team: i32 },
    /// `teehistorian-player-team@ddnet.tw`
    PlayerTeam { cid: i32, team: i32 },
    /// `teehistorian-team-practice@ddnet.tw`
    TeamPractice { team: i32, practice: i32 },
    /// `teehistorian-player-ready@ddnet.tw`
    PlayerReady { cid: i32 },
    /// `teehistorian-player-swap@ddnet.tw`
    PlayerSwap { cid1: i32, cid2: i32 },
    /// `teehistorian-antibot@ddnet.org`
    Antibot(Antibot<'a>),
    /// `teehistorian-player-name@ddnet.org`
    PlayerName(PlayerName<'a>),
    /// `teehistorian-player-finish@ddnet.org`
    PlayerFinish { cid: i32, time: i32 },
    /// `teehistorian-team-finish@ddnet.org`
    TeamFinish { team: i32, time: i32 },
}

impl Chunk<'_> {
    /// Returns the player_id associated to the chunk. None, if Chunk isn't by a player (e.g. Eos).
    /// `PlayerSwap` is dependent on two tees and currently returns None.
    pub fn cid(&self) -> Option<i32> {
        match self {
            Chunk::PlayerDiff(p) => Some(p.cid),
            Chunk::Eos => None,
            Chunk::TickSkip { dt: _ } => None,
            Chunk::PlayerNew(p) => Some(p.cid),
            Chunk::PlayerOld { cid } => Some(*cid),
            Chunk::InputDiff(p) => Some(p.cid),
            Chunk::InputNew(p) => Some(p.cid),
            Chunk::NetMessage(p) => Some(p.cid),
            Chunk::Join { cid } => Some(*cid),
            Chunk::Drop(p) => Some(p.cid),
            Chunk::ConsoleCommand(p) => Some(p.cid),
            Chunk::UnknownEx(_) => None,
            Chunk::Test => None,
            Chunk::DdnetVersionOld(p) => Some(p.cid),
            Chunk::DdnetVersion(p) => Some(p.cid),
            Chunk::AuthInit(p) => Some(p.cid),
            Chunk::AuthLogin(p) => Some(p.cid),
            Chunk::AuthLogout { cid } => Some(*cid),
            Chunk::JoinVer6 { cid } => Some(*cid),
            Chunk::JoinVer7 { cid } => Some(*cid),
            Chunk::RejoinVer6 { cid } => Some(*cid),
            Chunk::TeamSaveSuccess(_) => None,
            Chunk::TeamSaveFailure { team: _ } => None,
            Chunk::TeamLoadSuccess(_) => None,
            Chunk::TeamLoadFailure { team: _ } => None,
            Chunk::PlayerTeam { cid, team: _ } => Some(*cid),
            Chunk::TeamPractice {
                team: _,
                practice: _,
            } => None,
            Chunk::PlayerReady { cid } => Some(*cid),
            Chunk::PlayerSwap { cid1: _, cid2: _ } => None,
            Chunk::Antibot(_) => None,
            Chunk::PlayerName(p) => Some(p.cid),
            Chunk::PlayerFinish { cid, time: _ } => Some(*cid),
            Chunk::TeamFinish { team: _, time: _ } => None,
        }
    }

    pub fn discriminant(&self) -> u8 {
        // SAFETY: Because `Self` is marked `repr(u8)`, its layout is a `repr(C)` `union`
        // between `repr(C)` structs, each of which has the `u8` discriminant as its first
        // field, so we can read the discriminant without offsetting the pointer.
        // See https://doc.rust-lang.org/std/mem/fn.discriminant.html
        unsafe { *<*const _>::from(self).cast::<u8>() }
    }

    pub fn serialize_into(&self, buf: &mut Vec<u8>) -> crate::error::Result<()> {
        let mut packer = Packer::new(buf);
        self.serialize(&mut packer)
    }
}

fn chunk_player_diff(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, cid) = twint(input)?;
    let (input, dx) = twint(input)?;
    let (input, dy) = twint(input)?;
    Ok((input, Chunk::PlayerDiff(PlayerDiff { cid, dx, dy })))
}
fn chunk_tick_skip(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, dt) = twint(input)?;
    Ok((input, Chunk::TickSkip { dt }))
}
fn chunk_player_new(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, cid) = twint(input)?;
    let (input, x) = twint(input)?;
    let (input, y) = twint(input)?;
    Ok((input, Chunk::PlayerNew(PlayerNew { cid, x, y })))
}
fn chunk_player_old(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, cid) = twint(input)?;
    Ok((input, Chunk::PlayerOld { cid }))
}
fn chunk_input_diff(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, cid) = twint(input)?;
    let (input, dinput) = player_input(input)?;
    Ok((input, Chunk::InputDiff(InputDiff { cid, dinput })))
}
fn chunk_input_new(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, cid) = twint(input)?;
    let (input, player_input) = player_input(input)?;
    Ok((
        input,
        Chunk::InputNew(InputNew {
            cid,
            input: player_input,
        }),
    ))
}
fn chunk_net_message(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, cid) = twint(input)?;
    let (input, msg_size) = twint(input)?;
    if msg_size < 0 {
        return Err(Failure(ErrorKind::NegativeBufLen(msg_size)));
    }
    let (input, msg) = take(msg_size as usize)(input)?;
    Ok((input, Chunk::NetMessage(NetMessage { cid, msg })))
}

fn chunk_join(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, cid) = twint(input)?;
    Ok((input, Chunk::Join { cid }))
}
fn chunk_drop(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, cid) = twint(input)?;
    let (input, reason) = cstring(input)?;
    Ok((input, Chunk::Drop(Drop { cid, reason })))
}
fn chunk_console_command(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, cid) = twint(input)?;
    let (input, flags) = twint(input)?;
    let (input, cmd) = cstring(input)?;
    let (input, num_args) = twint(input)?;
    if num_args < 0 {
        return Err(Failure(ErrorKind::NegativeBufLen(num_args)));
    }
    let (input, args) = count(cstring, num_args as usize)(input)?;
    Ok((
        input,
        Chunk::ConsoleCommand(ConsoleCommand {
            cid,
            flags,
            cmd,
            args,
        }),
    ))
}

/// `teehistorian-test@ddnet.tw`
pub const TH_TEST: Uuid = Uuid::from_u128(0x6bb8ba88_0f0b_382e_8dae_dbf4052b8b7d);

/// `teehistorian-ddnetver-old@ddnet.tw`
pub const TH_DDNETVER_OLD: Uuid = Uuid::from_u128(0x41b49541_f26f_325d_8715_9baf4b544ef9);
/// `teehistorian-ddnetver@ddnet.tw`
pub const TH_DDNETVER: Uuid = Uuid::from_u128(0x1397b63e_ee4e_3919_b86a_b058887fcaf5);

/// `teehistorian-auth-init@ddnet.tw`
pub const TH_AUTH_INIT: Uuid = Uuid::from_u128(0x60daba5c_52c4_3aeb_b8ba_b2953fb55a17);
/// `teehistorian-auth-login@ddnet.tw`
pub const TH_AUTH_LOGIN: Uuid = Uuid::from_u128(0x37ecd3b8_9218_3bb9_a71b_a935b86f6a81);
/// `teehistorian-auth-logout@ddnet.tw`
pub const TH_AUTH_LOGOUT: Uuid = Uuid::from_u128(0xd4f5abe8_edd2_3fb9_abd8_1c8bb84f4a63);

/// `teehistorian-joinver6@ddnet.tw`
pub const TH_JOINVER6: Uuid = Uuid::from_u128(0x1899a382_71e3_36da_937d_c9de6bb95b1d);
/// `teehistorian-joinver7@ddnet.tw`
pub const TH_JOINVER7: Uuid = Uuid::from_u128(0x59239b05_0540_318d_bea4_9aa1e80e7d2b);
/// `teehistorian-rejoinver6@ddnet.org`
pub const TH_REJOINVER6: Uuid = Uuid::from_u128(0xc1e921d5_96f5_37bb_8a45_7a06f163d27e);

/// `teehistorian-save-success@ddnet.tw`
pub const TH_SAVE_SUCCESS: Uuid = Uuid::from_u128(0x4560c756_da29_3036_81d4_90a50f0182cd);
/// `teehistorian-save-failure@ddnet.tw`
pub const TH_SAVE_FAILURE: Uuid = Uuid::from_u128(0xb29901d5_1244_3bd0_bbde_23d04b1f7ba9);
/// `teehistorian-load-success@ddnet.tw`
pub const TH_LOAD_SUCCESS: Uuid = Uuid::from_u128(0xe05408d3_a313_33df_9eb3_ddb990ab954a);
/// `teehistorian-load-failure@ddnet.tw`
pub const TH_LOAD_FAILURE: Uuid = Uuid::from_u128(0xef8905a2_c695_3591_a1cd_53d2015992dd);

/// `teehistorian-player-team@ddnet.tw` player changing team
pub const TH_PLAYER_TEAM: Uuid = Uuid::from_u128(0xa111c04e_1ea8_38e0_90b1_d7f993ca0da9);

/// `teehistorian-team-practice@ddnet.tw`
pub const TH_TEAM_PRACTICE: Uuid = Uuid::from_u128(0x5792834e_81d1_34c9_a29b_b5ff25dac3bc);

/// `teehistorian-player-ready@ddnet.tw`
pub const TH_PLAYER_READY: Uuid = Uuid::from_u128(0x638587c9_3f75_3887_918e_a3c2614ffaa0);

/// `teehistorian-player-swap@ddnet.tw`
pub const TH_PLAYER_SWAP: Uuid = Uuid::from_u128(0x5de9b633_49cf_3e99_9a25_d4a78e9717d7);

/// `teehistorian-antibot@ddnet.org`
pub const TH_ANTIBOT: Uuid = Uuid::from_u128(0x866bfdac_fb49_3c0b_a887_5fe1f3ea00b8);

/// `teehistorian-player-name@ddnet.org`
pub const TH_PLAYER_NAME: Uuid = Uuid::from_u128(0xd016f9b9_4151_3b87_87e5_3a6087eb5f26);

/// `teehistorian-player-finish@ddnet.org`
pub const TH_PLAYER_FINISH: Uuid = Uuid::from_u128(0x68943c01_2348_3e01_9490_3f27f8269d94);

/// `teehistorian-team-finish@ddnet.org`
pub const TH_TEAM_FINISH: Uuid = Uuid::from_u128(0x9588b9af_3fdc_3760_8043_82deeee317a5);

fn extension_test(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    if !input.is_empty() {
        Err(Failure(ErrorKind::TrailingBytes(
            "EX_TEST",
            input.len() as u32,
        )))
    } else {
        Ok((input, Chunk::Test))
    }
}
fn extension_ddnetver_old(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, cid) = twint(input)?;
    let (input, version) = twint(input)?;
    if input != b"" {
        Err(Failure(ErrorKind::TrailingBytes(
            "EX_DDNETVER_OLD",
            input.len() as u32,
        )))
    } else {
        Ok((
            input,
            Chunk::DdnetVersionOld(DdnetVersionOld { cid, version }),
        ))
    }
}
fn extension_ddnetver(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, cid) = twint(input)?;
    let (input, connection_id) = uuid(input)?;
    let (input, version) = twint(input)?;
    let (input, version_str) = cstring(input)?;
    if input != b"" {
        Err(Failure(ErrorKind::TrailingBytes(
            "EX_DDNETVER",
            input.len() as u32,
        )))
    } else {
        Ok((
            input,
            Chunk::DdnetVersion(DdnetVersion {
                cid,
                connection_id,
                version,
                version_str,
            }),
        ))
    }
}
fn extension_auth_init(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, cid) = twint(input)?;
    let (input, level) = twint(input)?;
    let (input, auth_name) = cstring(input)?;
    if input != b"" {
        Err(Failure(ErrorKind::TrailingBytes(
            "EX_AUTH_INIT",
            input.len() as u32,
        )))
    } else {
        Ok((
            input,
            Chunk::AuthInit(Auth {
                cid,
                level,
                auth_name,
            }),
        ))
    }
}
fn extension_auth_login(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, cid) = twint(input)?;
    let (input, level) = twint(input)?;
    let (input, auth_name) = cstring(input)?;
    if input != b"" {
        Err(Failure(ErrorKind::TrailingBytes(
            "EX_AUTH_INIT",
            input.len() as u32,
        )))
    } else {
        Ok((
            input,
            Chunk::AuthLogin(Auth {
                cid,
                level,
                auth_name,
            }),
        ))
    }
}
fn extension_auth_logout(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, cid) = twint(input)?;
    if input != b"" {
        Err(Failure(ErrorKind::TrailingBytes(
            "EX_AUTH_LOGOUT",
            input.len() as u32,
        )))
    } else {
        Ok((input, Chunk::AuthLogout { cid }))
    }
}
fn extension_join_ver6(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, cid) = twint(input)?;
    if input != b"" {
        Err(Failure(ErrorKind::TrailingBytes(
            "EX_JOINVER6",
            input.len() as u32,
        )))
    } else {
        Ok((input, Chunk::JoinVer6 { cid }))
    }
}
fn extension_join_ver7(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, cid) = twint(input)?;
    if input != b"" {
        Err(Failure(ErrorKind::TrailingBytes(
            "EX_JOINVER7",
            input.len() as u32,
        )))
    } else {
        Ok((input, Chunk::JoinVer7 { cid }))
    }
}
fn extension_rejoin_ver6(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, cid) = twint(input)?;
    if input != b"" {
        Err(Failure(ErrorKind::TrailingBytes(
            "EX_REJOINVER6",
            input.len() as u32,
        )))
    } else {
        Ok((input, Chunk::RejoinVer6 { cid }))
    }
}
fn extension_save_success(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, team) = twint(input)?;
    let (input, save_id) = uuid(input)?;
    let (input, save) = cstring(input)?;
    if input != b"" {
        Err(Failure(ErrorKind::TrailingBytes(
            "EX_SAVE_SUCCESS",
            input.len() as u32,
        )))
    } else {
        Ok((
            input,
            Chunk::TeamSaveSuccess(TeamSave {
                team,
                save_id,
                save,
            }),
        ))
    }
}
fn extension_save_failure(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, team) = twint(input)?;
    if input != b"" {
        Err(Failure(ErrorKind::TrailingBytes(
            "EX_SAVE_FAILURE",
            input.len() as u32,
        )))
    } else {
        Ok((input, Chunk::TeamSaveFailure { team }))
    }
}
fn extension_load_success(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, team) = twint(input)?;
    let (input, save_id) = uuid(input)?;
    let (input, save) = cstring(input)?;
    if input != b"" {
        Err(Failure(ErrorKind::TrailingBytes(
            "EX_LOAD_SUCCESS",
            input.len() as u32,
        )))
    } else {
        Ok((
            input,
            Chunk::TeamLoadSuccess(TeamSave {
                team,
                save_id,
                save,
            }),
        ))
    }
}
fn extension_load_failure(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, team) = twint(input)?;
    if input != b"" {
        Err(Failure(ErrorKind::TrailingBytes(
            "EX_LOAD_FAILURE",
            input.len() as u32,
        )))
    } else {
        Ok((input, Chunk::TeamLoadFailure { team }))
    }
}
fn extension_player_team(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, cid) = twint(input)?;
    let (input, team) = twint(input)?;
    if input != b"" {
        Err(Failure(ErrorKind::TrailingBytes(
            "EX_PLAYER_TEAM",
            input.len() as u32,
        )))
    } else {
        Ok((input, Chunk::PlayerTeam { cid, team }))
    }
}
fn extension_team_practice(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, team) = twint(input)?;
    let (input, practice) = twint(input)?;
    if input != b"" {
        Err(Failure(ErrorKind::TrailingBytes(
            "EX_TEAM_PRACTICE",
            input.len() as u32,
        )))
    } else {
        Ok((input, Chunk::TeamPractice { team, practice }))
    }
}
fn extension_player_ready(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, cid) = twint(input)?;
    if input != b"" {
        Err(Failure(ErrorKind::TrailingBytes(
            "EX_PLAYER_READY",
            input.len() as u32,
        )))
    } else {
        Ok((input, Chunk::PlayerReady { cid }))
    }
}
fn extension_player_swap(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, cid1) = twint(input)?;
    let (input, cid2) = twint(input)?;
    if input != b"" {
        Err(Failure(ErrorKind::TrailingBytes(
            "EX_PLAYER_SWAP",
            input.len() as u32,
        )))
    } else {
        Ok((input, Chunk::PlayerSwap { cid1, cid2 }))
    }
}

fn extension_antibot(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    Ok((&[], Chunk::Antibot(Antibot { data: input })))
}

fn extension_player_name(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, cid) = twint(input)?;
    let (input, name) = cstring(input)?;
    if input != b"" {
        Err(Failure(ErrorKind::TrailingBytes(
            "EX_PLAYER_NAME",
            input.len() as u32,
        )))
    } else {
        Ok((input, Chunk::PlayerName(PlayerName { cid, name })))
    }
}

fn extension_player_finish(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, cid) = twint(input)?;
    let (input, time) = twint(input)?;
    if input != b"" {
        Err(Failure(ErrorKind::TrailingBytes(
            "EX_PLAYER_FINISH",
            input.len() as u32,
        )))
    } else {
        Ok((input, Chunk::PlayerFinish { cid, time }))
    }
}

fn extension_team_finish(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, team) = twint(input)?;
    let (input, time) = twint(input)?;
    if input != b"" {
        Err(Failure(ErrorKind::TrailingBytes(
            "EX_TEAM_FINISH",
            input.len() as u32,
        )))
    } else {
        Ok((input, Chunk::TeamFinish { team, time }))
    }
}

fn chunk_unknown_ex(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    let (input, uuid) = uuid(input)?;
    let (input, size) = twint(input)?;
    if size < 0 {
        return Err(Failure(ErrorKind::NegativeBufLen(size)));
    }
    let (input, data) = take(size as usize)(input)?;
    let (_, c) = match uuid {
        TH_TEST => extension_test(data)?,
        TH_DDNETVER_OLD => extension_ddnetver_old(data)?,
        TH_DDNETVER => extension_ddnetver(data)?,
        TH_AUTH_INIT => extension_auth_init(data)?,
        TH_AUTH_LOGIN => extension_auth_login(data)?,
        TH_AUTH_LOGOUT => extension_auth_logout(data)?,
        TH_JOINVER6 => extension_join_ver6(data)?,
        TH_JOINVER7 => extension_join_ver7(data)?,
        TH_REJOINVER6 => extension_rejoin_ver6(data)?,
        TH_SAVE_SUCCESS => { extension_save_success(data) }?,
        TH_SAVE_FAILURE => { extension_save_failure(data) }?,
        TH_LOAD_SUCCESS => { extension_load_success(data) }?,
        TH_LOAD_FAILURE => { extension_load_failure(data) }?,
        TH_PLAYER_TEAM => { extension_player_team(data) }?,
        TH_TEAM_PRACTICE => { extension_team_practice(data) }?,
        TH_PLAYER_READY => { extension_player_ready(data) }?,
        TH_PLAYER_SWAP => { extension_player_swap(data) }?,
        TH_ANTIBOT => extension_antibot(data)?,
        TH_PLAYER_NAME => extension_player_name(data)?,
        TH_PLAYER_FINISH => extension_player_finish(data)?,
        TH_TEAM_FINISH => extension_team_finish(data)?,
        _ => (input, Chunk::UnknownEx(UnknownEx { uuid, data })),
    };
    Ok((input, c))
}
pub fn chunk(input: &[u8]) -> IResult<&[u8], Chunk, ErrorKind> {
    // get input without (i) and with (input) the tag
    let (i, tag) = twint(input)?;
    match tag {
        0.. => chunk_player_diff(input), // tag is the cid
        -1 => Ok((i, Chunk::Eos)),
        -2 => chunk_tick_skip(i),
        -3 => chunk_player_new(i),
        -4 => chunk_player_old(i),
        -5 => chunk_input_diff(i),
        -6 => chunk_input_new(i),
        -7 => chunk_net_message(i),
        -8 => chunk_join(i),
        -9 => chunk_drop(i),
        -10 => chunk_console_command(i),
        -11 => chunk_unknown_ex(i),
        other => Err(Failure(ErrorKind::UnknownTag(other))),
    }
}
