use arrayvec::ArrayVec;
use nom::AsBytes;
use serde::ser::SerializeSeq;
use serde::{ser, Serialize, Serializer};
use std::io;
use uuid::Uuid;

use crate::chunks::{
    TH_ANTIBOT, TH_AUTH_INIT, TH_AUTH_LOGIN, TH_AUTH_LOGOUT, TH_DDNETVER, TH_DDNETVER_OLD,
    TH_JOINVER6, TH_JOINVER7, TH_LOAD_FAILURE, TH_LOAD_SUCCESS, TH_PLAYER_FINISH, TH_PLAYER_NAME,
    TH_PLAYER_READY, TH_PLAYER_SWAP, TH_PLAYER_TEAM, TH_REJOINVER6, TH_SAVE_FAILURE,
    TH_SAVE_SUCCESS, TH_TEAM_FINISH, TH_TEAM_PRACTICE, TH_TEST,
};
use crate::{error, Chunk};
use error::{Error, Result};

pub(crate) struct Packer<W: io::Write> {
    writer: W,
}

impl<W: io::Write> Packer<W> {
    pub(crate) fn new(writer: W) -> Self {
        Self { writer }
    }
}

pub fn serialize_into<W, T>(writer: W, value: &T) -> Result<()>
where
    W: io::Write,
    T: Serialize,
{
    let mut p = Packer::new(writer);
    value.serialize(&mut p)
}

pub struct ThWriter<W: io::Write> {
    packer: Packer<W>,
}

/// adds size information to the beginning
pub fn write_size_first<T, S>(arr: &[T], serializer: S) -> std::result::Result<S::Ok, S::Error>
where
    T: Serialize,
    S: Serializer,
{
    let mut s = serializer.serialize_seq(None)?;
    s.serialize_element(&(arr.len() as i32))?;
    s.serialize_element(arr)?;
    s.end()
}

/// adds a null terminator at the end of the byte sequence
pub fn null_terminator<T, S>(bytes: &T, serializer: S) -> std::result::Result<S::Ok, S::Error>
where
    T: ?Sized + Serialize,
    S: Serializer,
{
    let mut s = serializer.serialize_seq(None)?;
    s.serialize_element(bytes)?;
    s.serialize_element(&[0u8])?;
    s.end()
}

/// allows to have a vector of null terminated strings with the vector not being null terminated
pub fn write_size_first_and_inner_null_terminator<T, S>(
    bytes_slice: &[T],
    serializer: S,
) -> std::result::Result<S::Ok, S::Error>
where
    T: Serialize,
    S: Serializer,
{
    let mut s = serializer.serialize_seq(None)?;
    s.serialize_element(&(bytes_slice.len() as i32))?;
    for bytes in bytes_slice {
        s.serialize_element(bytes)?;
        s.serialize_element(&[0u8])?;
    }
    s.end()
}

impl<W: io::Write> ThWriter<W> {
    pub fn new(mut writer: W, header: &str) -> Result<ThWriter<W>> {
        writer.write_all(Uuid::from_u128(0x699db17b_8efb_34ff_b1d8_da6f60c15dd1).as_bytes())?;
        writer.write_all(header.as_bytes())?;
        writer.write_all(b"\0")?;
        Ok(ThWriter {
            packer: Packer::new(writer),
        })
    }

    /// Returns a reference to the underlying `Write` object.
    pub fn inner(&self) -> &W {
        &self.packer.writer
    }

    /// Drops the `ThWriter` and returns the underlying `Write` object.
    pub fn into_inner(self) -> W {
        self.packer.writer
    }

    pub fn add_chunk(&mut self, c: &Chunk) -> Result<()> {
        c.serialize(&mut self.packer)
    }
}

fn serialize_small_extra<T>(value: &T) -> Result<ArrayVec<u8, 2048>>
where
    T: Serialize,
{
    let mut serializer = Packer {
        writer: ArrayVec::<u8, 2048>::new(),
    };
    value.serialize(&mut serializer)?;
    Ok(serializer.writer)
}

fn serialize_large_extra<T>(value: &T) -> Result<ArrayVec<u8, 65536>>
where
    T: Serialize,
{
    let mut serializer = Packer {
        writer: ArrayVec::<u8, 65536>::new(),
    };
    value.serialize(&mut serializer)?;
    Ok(serializer.writer)
}

impl<W> ser::Serializer for &'_ mut Packer<W>
where
    W: io::Write,
{
    // The output type produced by this `Serializer` during successful
    // serialization. Most serializers that produce text or binary output should
    // set `Ok = ()` and serialize into an `io::Write` or buffer contained
    // within the `Serializer` instance, as happens here. Serializers that build
    // in-memory data structures may be simplified by using `Ok` to propagate
    // the data structure around.
    type Ok = ();

    // The error type when some error occurs during serialization.
    type Error = Error;

    type SerializeSeq = Self;
    type SerializeTuple = Self;
    type SerializeTupleStruct = Self;
    type SerializeTupleVariant = Self;
    type SerializeMap = Self;
    type SerializeStruct = Self;
    type SerializeStructVariant = Self;

    fn serialize_bool(self, _v: bool) -> Result<Self::Ok> {
        todo!()
    }

    fn serialize_i8(self, _v: i8) -> Result<Self::Ok> {
        todo!()
    }

    fn serialize_i16(self, _v: i16) -> Result<Self::Ok> {
        todo!()
    }

    // adapted from https://github.com/heinrich5991/libtw2/blob/444e3e0160dec74afcf82e8e42398fe2027a76a2/packer/src/lib.rs#L97-L111
    fn serialize_i32(self, int: i32) -> Result<Self::Ok> {
        let sign = if int < 0 { 1 } else { 0 };
        let mut int = (int ^ -sign) as u32;
        let mut next = (int & 0b0011_1111) as u8;
        int >>= 6;
        if int != 0 {
            next |= 0b1000_0000;
        }
        if sign != 0 {
            next |= 0b0100_0000;
        }
        self.writer.write_all(&[next])?;
        while int != 0 {
            let mut next = (int & 0b0111_1111) as u8;
            int >>= 7;
            if int != 0 {
                next |= 0b1000_0000;
            }
            self.writer.write_all(&[next])?;
        }
        Ok(())
    }

    fn serialize_i64(self, _v: i64) -> Result<Self::Ok> {
        todo!()
    }

    fn serialize_u8(self, v: u8) -> Result<Self::Ok> {
        self.writer.write_all(&[v])?;
        Ok(())
    }

    fn serialize_u16(self, _v: u16) -> Result<Self::Ok> {
        todo!()
    }

    fn serialize_u32(self, _v: u32) -> Result<Self::Ok> {
        todo!()
    }

    fn serialize_u64(self, _v: u64) -> Result<Self::Ok> {
        todo!()
    }

    fn serialize_f32(self, _v: f32) -> Result<Self::Ok> {
        todo!()
    }

    fn serialize_f64(self, _v: f64) -> Result<Self::Ok> {
        todo!()
    }

    fn serialize_char(self, _v: char) -> Result<Self::Ok> {
        todo!()
    }

    fn serialize_str(self, v: &str) -> Result<Self::Ok> {
        self.writer.write_all(v.as_bytes())?;
        self.writer.write_all(b"\0")?;
        Ok(())
    }

    fn serialize_bytes(self, v: &[u8]) -> Result<Self::Ok> {
        self.writer.write_all(v)?;
        Ok(())
    }

    fn serialize_none(self) -> Result<Self::Ok> {
        todo!()
    }

    fn serialize_some<T>(self, _value: &T) -> Result<Self::Ok>
    where
        T: ?Sized + Serialize,
    {
        todo!()
    }

    fn serialize_unit(self) -> Result<Self::Ok> {
        Ok(())
    }

    fn serialize_unit_struct(self, _name: &'static str) -> Result<Self::Ok> {
        todo!()
    }

    fn serialize_unit_variant(
        self,
        _name: &'static str,
        _variant_index: u32,
        _variant: &'static str,
    ) -> Result<Self::Ok> {
        todo!()
    }

    fn serialize_newtype_struct<T>(self, _name: &'static str, _value: &T) -> Result<Self::Ok>
    where
        T: ?Sized + Serialize,
    {
        todo!()
    }

    fn serialize_newtype_variant<T>(
        self,
        _name: &'static str,
        _variant_index: u32,
        _variant: &'static str,
        _value: &T,
    ) -> Result<Self::Ok>
    where
        T: ?Sized + Serialize,
    {
        todo!()
    }

    fn serialize_seq(self, _len: Option<usize>) -> Result<Self::SerializeSeq> {
        Ok(self)
    }

    fn serialize_tuple(self, _len: usize) -> Result<Self::SerializeTuple> {
        Ok(self)
    }

    fn serialize_tuple_struct(
        self,
        _name: &'static str,
        _len: usize,
    ) -> Result<Self::SerializeTupleStruct> {
        todo!()
    }

    fn serialize_tuple_variant(
        self,
        _name: &'static str,
        _variant_index: u32,
        _variant: &'static str,
        _len: usize,
    ) -> Result<Self::SerializeTupleVariant> {
        todo!()
    }

    fn serialize_map(self, _len: Option<usize>) -> Result<Self::SerializeMap> {
        todo!()
    }

    fn serialize_struct(self, _name: &'static str, _len: usize) -> Result<Self::SerializeStruct> {
        Ok(self)
    }

    fn serialize_struct_variant(
        self,
        _name: &'static str,
        _variant_index: u32,
        _variant: &'static str,
        _len: usize,
    ) -> Result<Self::SerializeStructVariant> {
        todo!()
    }
}

// The following 7 impls deal with the serialization of compound types like
// sequences and maps. Serialization of such types is begun by a Serializer
// method and followed by zero or more calls to serialize individual elements of
// the compound type and one call to end the compound type.
//
// This impl is SerializeSeq so these methods are called after `serialize_seq`
// is called on the Serializer.
impl<W: io::Write> ser::SerializeSeq for &'_ mut Packer<W> {
    // Must match the `Ok` type of the serializer.
    type Ok = ();
    // Must match the `Error` type of the serializer.
    type Error = Error;

    // Serialize a single element of the sequence.
    fn serialize_element<T>(&mut self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        value.serialize(&mut **self)
    }

    // Close the sequence.
    fn end(self) -> Result<()> {
        Ok(())
    }
}

// Same thing but for tuples.
impl<W: io::Write> ser::SerializeTuple for &'_ mut Packer<W> {
    type Ok = ();
    type Error = Error;

    fn serialize_element<T>(&mut self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        value.serialize(&mut **self)
    }

    fn end(self) -> Result<()> {
        Ok(())
    }
}

// Same thing but for tuple structs.
impl<W: io::Write> ser::SerializeTupleStruct for &'_ mut Packer<W> {
    type Ok = ();
    type Error = Error;

    fn serialize_field<T>(&mut self, _value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        todo!()
    }

    fn end(self) -> Result<()> {
        todo!()
    }
}

// Tuple variants are a little different. Refer back to the
// `serialize_tuple_variant` method above:
//
//    self.output += "{";
//    variant.serialize(&mut *self)?;
//    self.output += ":[";
//
// So the `end` method in this impl is responsible for closing both the `]` and
// the `}`.
impl<W: io::Write> ser::SerializeTupleVariant for &'_ mut Packer<W> {
    type Ok = ();
    type Error = Error;

    fn serialize_field<T>(&mut self, _value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        todo!()
    }

    fn end(self) -> Result<()> {
        todo!()
    }
}

// Some `Serialize` types are not able to hold a key and value in memory at the
// same time so `SerializeMap` implementations are required to support
// `serialize_key` and `serialize_value` individually.
//
// There is a third optional method on the `SerializeMap` trait. The
// `serialize_entry` method allows serializers to optimize for the case where
// key and value are both available simultaneously. In JSON it doesn't make a
// difference so the default behavior for `serialize_entry` is fine.
impl<W: io::Write> ser::SerializeMap for &'_ mut Packer<W> {
    type Ok = ();
    type Error = Error;

    // The Serde data model allows map keys to be any serializable type. JSON
    // only allows string keys so the implementation below will produce invalid
    // JSON if the key serializes as something other than a string.
    //
    // A real JSON serializer would need to validate that map keys are strings.
    // This can be done by using a different Serializer to serialize the key
    // (instead of `&mut **self`) and having that other serializer only
    // implement `serialize_str` and return an error on any other data type.
    fn serialize_key<T>(&mut self, _key: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        todo!()
    }

    // It doesn't make a difference whether the colon is printed at the end of
    // `serialize_key` or at the beginning of `serialize_value`. In this case
    // the code is a bit simpler having it here.
    fn serialize_value<T>(&mut self, _value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        todo!()
    }

    fn end(self) -> Result<()> {
        todo!()
    }
}

// Structs are like maps in which the keys are constrained to be compile-time
// constant strings.
impl<W: io::Write> ser::SerializeStruct for &'_ mut Packer<W> {
    type Ok = ();
    type Error = Error;

    fn serialize_field<T>(&mut self, _key: &'static str, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        value.serialize(&mut **self)
    }

    fn end(self) -> Result<()> {
        Ok(())
    }
}

// Similar to `SerializeTupleVariant`, here the `end` method is responsible for
// closing both of the curly braces opened by `serialize_struct_variant`.
impl<W: io::Write> ser::SerializeStructVariant for &'_ mut Packer<W> {
    type Ok = ();
    type Error = Error;

    fn serialize_field<T>(&mut self, _key: &'static str, _value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        todo!()
    }

    fn end(self) -> Result<()> {
        todo!()
    }
}

fn serialize_chunk_normal<S, T>(
    serializer: S,
    id: i32,
    value: &T,
) -> std::result::Result<S::Ok, S::Error>
where
    S: serde::Serializer,
    T: Serialize,
{
    assert!(id < 0);
    let mut seq = serializer.serialize_seq(None)?;
    seq.serialize_element::<i32>(&id)?;
    seq.serialize_element(value)?;
    seq.end()
}

fn serialize_chunk_ex_small<S, T>(
    serializer: S,
    uuid: Uuid,
    value: &T,
) -> std::result::Result<S::Ok, S::Error>
where
    S: serde::Serializer,
    T: Serialize,
{
    let mut seq = serializer.serialize_seq(None)?;
    seq.serialize_element::<i32>(&-11)?;
    seq.serialize_element(&uuid.as_bytes())?;
    let ex = serialize_small_extra(value).map_err(ser::Error::custom)?;
    seq.serialize_element(&(ex.len() as i32))?; // len
    seq.serialize_element(&ex.as_bytes())?;
    seq.end()
}

fn serialize_chunk_ex_large<S, T>(
    serializer: S,
    uuid: Uuid,
    value: &T,
) -> std::result::Result<S::Ok, S::Error>
where
    S: serde::Serializer,
    T: Serialize,
{
    let mut seq = serializer.serialize_seq(None)?;
    seq.serialize_element::<i32>(&-11)?;
    seq.serialize_element(uuid.as_bytes())?;
    let ex = serialize_large_extra(value).map_err(ser::Error::custom)?;
    seq.serialize_element(&(ex.len() as i32))?; // len
    seq.serialize_element(&ex.as_bytes())?;
    seq.end()
}

impl Serialize for crate::chunks::Chunk<'_> {
    fn serialize<S>(&self, serializer: S) -> std::result::Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        match self {
            Chunk::PlayerDiff(c) => {
                assert!(c.cid >= 0);
                c.serialize(serializer)
            }
            Chunk::Eos => serializer.serialize_i32(-1),
            Chunk::TickSkip { dt } => serialize_chunk_normal(serializer, -2, dt),
            Chunk::PlayerNew(c) => serialize_chunk_normal(serializer, -3, c),
            Chunk::PlayerOld { cid } => serialize_chunk_normal(serializer, -4, cid),
            Chunk::InputDiff(c) => serialize_chunk_normal(serializer, -5, c),
            Chunk::InputNew(c) => serialize_chunk_normal(serializer, -6, c),
            Chunk::NetMessage(c) => serialize_chunk_normal(serializer, -7, c),
            Chunk::Join { cid } => serialize_chunk_normal(serializer, -8, cid),
            Chunk::Drop(c) => serialize_chunk_normal(serializer, -9, c),
            Chunk::ConsoleCommand(c) => serialize_chunk_normal(serializer, -10, c),
            // serialize teehistorian extensions
            Chunk::UnknownEx(c) => {
                let mut seq = serializer.serialize_seq(None)?;
                seq.serialize_element::<i32>(&-11)?;
                seq.serialize_element(c)?;
                seq.end()
            }
            Chunk::Test => serialize_chunk_ex_small(serializer, TH_TEST, &()),
            Chunk::DdnetVersionOld(c) => serialize_chunk_ex_small(serializer, TH_DDNETVER_OLD, c),
            Chunk::DdnetVersion(c) => serialize_chunk_ex_small(serializer, TH_DDNETVER, c),
            Chunk::AuthInit(c) => serialize_chunk_ex_small(serializer, TH_AUTH_INIT, c),
            Chunk::AuthLogin(c) => serialize_chunk_ex_small(serializer, TH_AUTH_LOGIN, c),
            Chunk::AuthLogout { cid } => serialize_chunk_ex_small(serializer, TH_AUTH_LOGOUT, cid),
            Chunk::JoinVer6 { cid } => serialize_chunk_ex_small(serializer, TH_JOINVER6, cid),
            Chunk::JoinVer7 { cid } => serialize_chunk_ex_small(serializer, TH_JOINVER7, cid),
            Chunk::RejoinVer6 { cid } => serialize_chunk_ex_small(serializer, TH_REJOINVER6, cid),
            Chunk::TeamSaveSuccess(c) => serialize_chunk_ex_large(serializer, TH_SAVE_SUCCESS, c),
            Chunk::TeamSaveFailure { team } => {
                serialize_chunk_ex_small(serializer, TH_SAVE_FAILURE, team)
            }
            Chunk::TeamLoadSuccess(c) => serialize_chunk_ex_large(serializer, TH_LOAD_SUCCESS, c),
            Chunk::TeamLoadFailure { team } => {
                serialize_chunk_ex_small(serializer, TH_LOAD_FAILURE, team)
            }
            Chunk::PlayerTeam { cid, team } => {
                serialize_chunk_ex_small(serializer, TH_PLAYER_TEAM, &(cid, team))
            }
            Chunk::TeamPractice { team, practice } => {
                serialize_chunk_ex_small(serializer, TH_TEAM_PRACTICE, &(team, practice))
            }
            Chunk::PlayerReady { cid } => {
                serialize_chunk_ex_small(serializer, TH_PLAYER_READY, cid)
            }
            Chunk::PlayerSwap { cid1, cid2 } => {
                serialize_chunk_ex_small(serializer, TH_PLAYER_SWAP, &(cid1, cid2))
            }
            Chunk::Antibot(c) => serialize_chunk_ex_small(serializer, TH_ANTIBOT, c),
            Chunk::PlayerName(c) => serialize_chunk_ex_small(serializer, TH_PLAYER_NAME, c),
            Chunk::PlayerFinish { cid, time } => {
                serialize_chunk_ex_small(serializer, TH_PLAYER_FINISH, &(cid, time))
            }
            Chunk::TeamFinish { team, time } => {
                serialize_chunk_ex_small(serializer, TH_TEAM_FINISH, &(team, time))
            }
        }
    }
}
