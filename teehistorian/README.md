# Teehistorian parser

This is a Teehistorian parser, a data format for DDNet servers saving all input to reproduce it faithfully.

## License

[LGPL-3.0](LICENSE)
