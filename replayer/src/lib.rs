use core::str;
use log::{debug, info};
use std::collections::HashMap;
use teehistorian::{Chunk, ThStream};
use twgame_core::console::Command;
use twgame_core::database::{FinishTeam, FinishTee, Finishes};
use twgame_core::net_msg::NetVersion;
use twgame_core::replay::{DemoPtr, GameReplayerAll, ReplayerTeeInfo};
use twgame_core::teehistorian::chunks::ConsoleCommand;
use twgame_core::twsnap::time::{Duration, Instant};
use twgame_core::twsnap::Snap;
use twgame_core::{info_demo, DisplayChunk, Input};
use vek::Vec2;

/// [TwGame-Core](https://crates.io/crate/twgame-core) crate
pub use twgame_core;
/// [Teehistorian](https://crates.io/crate/teehistorian) crate
pub use twgame_core::teehistorian;

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
enum ThPlayerEvent {
    /// optional event specifying protocol version for NetMessages
    JoinVer,
    Join,
    // PlayerInfo,
    // DDNetVersion,
    // finally joined the game
    InGame,
    Drop,
}

#[derive(Clone, Debug, PartialEq, Eq)]
struct ThPlayer {
    event: ThPlayerEvent,
    version: NetVersion,
    team: i32,
    name: String,
}

impl ThPlayer {
    fn new(version: NetVersion, event: ThPlayerEvent) -> Self {
        Self {
            event,
            version,
            team: 0,
            name: String::new(),
        }
    }
}

struct ThTeam {
    practice: bool,
}

impl ThTeam {
    fn new() -> Self {
        Self { practice: false }
    }
}

pub struct ThReplayer {
    cur_time: Instant,
    players: Vec<Option<ThPlayer>>,
    tees: Vec<Option<ReplayerTeeInfo>>,
    // count players to generate `Empty` and `NonEmpty` events
    num_player: u32,
    teams: HashMap<i32, ThTeam>,
    // store last input per player not in player struct, since it survives rejoins
    last_input: Vec<Option<Input>>,
    // store last player_id of messages PLAYER_DIFF, PLAYER_NEW, PLAYER_OLD to execute implicit ticks
    implicit_tick_player_id: Option<u32>,
    // the tick has to get validated after all Player{New,Diff,Old} are parsed and before the new
    // input from the next tick is passed to the Game world.
    last_tick_validated: bool,
    // after a TickSkip, a Load/Save result can return. This has to be passed to the game world,
    // before the tick is executed. Using this variable to dalay executing the tick until after the
    // database results
    delayed_tick: bool,
    snap: Snap,
}

impl ThReplayer {
    pub fn new<T: GameReplayerAll>(header: &[u8], world: &mut T) -> Self {
        world.on_teehistorian_header(header);
        let mut teams = HashMap::new();
        teams.insert(0, ThTeam::new());
        ThReplayer {
            cur_time: Instant::zero(),
            players: vec![],
            tees: vec![],
            teams,
            last_input: vec![],
            num_player: 0,
            implicit_tick_player_id: None, // first tick is always implicit
            last_tick_validated: false,
            delayed_tick: false,
            snap: Snap::default(),
        }
    }

    pub fn is_empty(&self) -> bool {
        self.num_player == 0
    }

    pub fn cur_time(&self) -> Instant {
        self.cur_time
    }

    fn player_name(&self, cid: i32) -> &str {
        assert!(0 <= cid && cid <= self.players.len() as i32);
        let player = self.players[cid as usize]
            .as_ref()
            .expect("PlayerName for non-existing player");
        player.name.as_str()
    }

    fn team_names(&self, team: i32) -> Vec<&str> {
        let mut players = vec![];
        for player in self.players.iter() {
            if let Some(player) = player.as_ref() {
                if player.team == team {
                    players.push(player.name.as_str())
                }
            }
        }
        players
    }

    fn format_console_command(cmd: &ConsoleCommand) -> Option<String> {
        let mut command = "/".to_owned();
        command.push_str(std::str::from_utf8(cmd.cmd).ok()?);
        for arg in cmd.args.iter() {
            command.push(' ');
            command.push_str(std::str::from_utf8(arg).ok()?);
        }
        Some(command)
    }

    /// returns false if the validations fails
    fn maybe_validate_last_tick<T: GameReplayerAll>(
        &mut self,
        world: &mut T,
        mut demo: DemoPtr<T>,
    ) {
        if !self.last_tick_validated {
            self.last_tick_validated = true;

            world.check_tees(self.cur_time, &self.tees, demo.as_mut().map(|d| d.chat()));

            // snap after setting tees to position from teehistorian file
            if let Some(demo) = demo {
                demo.snap_and_write(self.cur_time, world, &mut self.snap)
                    .unwrap();
            }
        }
    }

    fn tick<T: GameReplayerAll>(&mut self, world: &mut T, demo: DemoPtr<T>) {
        self.maybe_validate_last_tick(world, demo);

        self.cur_time = self.cur_time.advance();
        world.tick(self.cur_time);

        self.last_tick_validated = false;
    }

    // returns whether PlayerNew, PlayerOld, PlayerDiff with given cid would result into a implicit
    // tick
    fn is_implicid_tick(&self, cid: u32) -> bool {
        if let Some(implicit_tick_player_id) = self.implicit_tick_player_id {
            if cid <= implicit_tick_player_id {
                return true;
            }
        }
        false
    }

    /// executes implicit tick(), if necessary
    fn implicit_tick<T: GameReplayerAll>(&mut self, world: &mut T, cid: u32, demo: DemoPtr<T>) {
        if self.is_implicid_tick(cid) {
            self.tick(world, demo);
        }
        self.implicit_tick_player_id = Some(cid);
    }

    pub fn validate<T: GameReplayerAll>(
        mut self,
        world: &mut T,
        th: &mut dyn ThStream,
        mut demo: DemoPtr<T>,
    ) {
        if let Some(demo) = &mut demo {
            demo.write_chat(
                "Demo created by Teehistorian replayer TwGame https://gitlab.com/ddnet-rs/TwGame",
            )
            .unwrap();
        }

        loop {
            match th.next_chunk() {
                Ok(chunk) => {
                    debug!("{}: {}", self.cur_time, DisplayChunk(&chunk));
                    self.replay_next_chunk(world, Some(chunk), demo.as_deref_mut());
                }
                Err(teehistorian::Error::Eof) => {
                    // Eos chunk missing?, don't panic. It's fine.
                    self.replay_next_chunk(world, None, demo.as_deref_mut());
                    break;
                }
                Err(err) => {
                    info!("teehistorian_chunk_err {err}");
                    self.replay_next_chunk(world, None, demo.as_deref_mut());
                    break;
                }
            }
        }
    }

    fn join_ver(&mut self, cid: i32, net_version: NetVersion) {
        assert!(0 <= cid);
        let cid = cid as usize;
        if cid >= self.players.len() {
            self.players.resize(cid + 1, None);
            self.last_input.resize(cid + 1, None);
        } else if self.players[cid]
            .as_ref()
            .map(|p| p.event != ThPlayerEvent::Drop)
            .unwrap_or(false)
        {
            return;
        }

        self.players[cid] = Some(ThPlayer::new(net_version, ThPlayerEvent::JoinVer));
    }

    fn join<T: GameReplayerAll>(&mut self, world: &mut T, cid: i32) {
        self.num_player = self.num_player.checked_add(1).unwrap();
        // player joined on engine level (via 0.6 if not previously specified via JoinVer6/JoinVer7)
        assert!(0 <= cid);
        let cid = cid as usize;
        if cid >= self.players.len() {
            self.players.resize(cid + 1, None);
            self.last_input.resize(cid + 1, None);
        }
        if let Some(player) = self.players[cid].as_mut() {
            // Join must be preceded by JoinVer
            assert_eq!(player.event, ThPlayerEvent::JoinVer);
            player.event = ThPlayerEvent::Join;
        } else {
            self.players[cid] = Some(ThPlayer::new(NetVersion::Unknown, ThPlayerEvent::Join));
        }
        world.player_join(cid as u32);
    }

    /// Return value contains chunk and no chunk if no further chunks exist.
    pub fn replay_next_chunk<T: GameReplayerAll>(
        &mut self,
        world: &mut T,
        chunk: Option<Chunk<'_>>,
        mut demo: DemoPtr<T>,
    ) {
        let Some(chunk) = chunk else {
            self.maybe_validate_last_tick(world, demo);
            world.finalize();
            return;
        };

        if self.delayed_tick
            && !matches!(
                chunk,
                Chunk::TeamLoadSuccess(_)
                    | Chunk::TeamLoadFailure { team: _ }
                    | Chunk::TeamSaveSuccess(_)
                    | Chunk::TeamSaveFailure { team: _ }
            )
        {
            self.delayed_tick = false;
            self.tick(world, demo.as_deref_mut());
        }

        if !matches!(
            chunk,
            Chunk::PlayerNew(_)
                | Chunk::PlayerOld { cid: _ }
                | Chunk::PlayerDiff(_)
                | Chunk::TeamLoadFailure { team: _ }
                | Chunk::TeamLoadSuccess(_)
                | Chunk::TeamSaveFailure { team: _ }
                | Chunk::TeamSaveSuccess(_)
                | Chunk::TeamPractice {
                    team: _,
                    practice: _
                }
                | Chunk::PlayerTeam { cid: _, team: _ }
                | Chunk::UnknownEx(_)
                | Chunk::Eos
        ) {
            self.maybe_validate_last_tick(world, demo.as_deref_mut());
        }

        match chunk {
            Chunk::Eos => {}
            // getting new input
            // next player to join joins via 0.6 protocol
            Chunk::JoinVer6 { cid } => self.join_ver(cid, NetVersion::V06),
            // next player to join joins via 0.7 vanilla protocol
            Chunk::JoinVer7 { cid } => self.join_ver(cid, NetVersion::V07),
            // ignore rejoins
            Chunk::RejoinVer6 { cid: _ } => {}
            Chunk::Join { cid } => self.join(world, cid),
            Chunk::Drop(ref p) => {
                // player left on engine level
                assert!(0 <= p.cid && p.cid < self.players.len() as i32);
                if let Some(player) = self.players[p.cid as usize].as_mut() {
                    self.num_player = self.num_player.checked_sub(1).unwrap();
                    // check if only joined players can drop
                    assert_ne!(player.event, ThPlayerEvent::Drop);
                    player.event = ThPlayerEvent::Drop;
                    world.player_leave(p.cid as u32);
                } else {
                    //TODO: figure out why I can't panic!("Non-existing player dropped from engine");
                }
            }

            Chunk::InputNew(ref inp) => {
                assert!(0 <= inp.cid && inp.cid < self.players.len() as i32);
                //assert_eq!(self.last_input[inp.cid as usize], None);
                let input = Input::from(inp.input);
                world.player_input(inp.cid as u32, &input);
                self.last_input[inp.cid as usize] = Some(input);
            }
            Chunk::InputDiff(ref inp) => {
                assert!(0 <= inp.cid && inp.cid < self.players.len() as i32);
                assert_ne!(self.last_input[inp.cid as usize], None);
                if let Some(cur_inp) = self.last_input[inp.cid as usize].as_mut() {
                    cur_inp.add_input_diff(inp.dinput);
                    world.player_input(inp.cid as u32, cur_inp);
                }
            }

            Chunk::NetMessage(ref msg) => {
                assert!(0 <= msg.cid && msg.cid < self.players.len() as i32);
                if let Some(p) = self.players[msg.cid as usize].as_mut() {
                    match twgame_core::net_msg::parse_net_msg(msg.msg, &mut p.version) {
                        Ok(net_msg) => world.on_net_msg(msg.cid as u32, &net_msg),
                        Err(err) => info_demo!(
                            demo.as_deref_mut(),
                            self.cur_time,
                            "Error on NetMessage ({})",
                            err,
                        ),
                    }
                } else {
                    panic!(
                        "player_id {}: NetMessage event for non-existing player",
                        msg.cid
                    );
                }
            }
            Chunk::ConsoleCommand(ref cmd) => {
                assert!(-1 <= cmd.cid && cmd.cid < self.players.len() as i32);
                if cmd.cid == -1 {
                    // initiated by server, ignore for now.
                } else {
                    let cid = cmd.cid as u32;

                    if let Some(command) = Self::format_console_command(cmd) {
                        if let Some(demo) = demo.as_deref_mut() {
                            if command.starts_with("/timeout") {
                                // do not add timeouts to demo
                            } else if command.starts_with("/save") {
                                demo.write_player_chat(cmd.cid, "/save **hidden**").unwrap();
                            } else if command == "/load" {
                                demo.write_player_chat(cmd.cid, "/load").unwrap();
                            } else if command.starts_with("/load") {
                                demo.write_player_chat(cmd.cid, "/load **hidden**").unwrap();
                            } else {
                                demo.write_player_chat(cmd.cid, &command).unwrap();
                            }
                        }
                    }
                    match Command::from_teehistorian(cmd) {
                        None => {
                            info_demo!(
                                demo.as_deref_mut(),
                                self.cur_time,
                                "Ignoring console command"
                            );
                        }
                        Some(cmd) => {
                            // ignore team command in the first three seconds. Hacky workaround to
                            // tests running for a behavior that might change in future ddnet versions
                            if matches!(cmd, Command::Team(_))
                                && !self
                                    .cur_time
                                    .duration_passed_since(Instant::zero(), Duration::from_secs(3))
                            {
                                // ignore command for now
                            } else {
                                world.on_command(cid, &cmd);
                            }
                        }
                    }
                }
            }

            // checking input
            Chunk::PlayerReady { cid } => {
                assert!(0 <= cid && cid < self.players.len() as i32);
                if let Some(player) = self.players[cid as usize].as_mut() {
                    assert_eq!(player.event, ThPlayerEvent::Join);
                    player.event = ThPlayerEvent::InGame;
                    world.player_ready(cid as u32);
                } else {
                    panic!(
                        "player_id {}: PlayerReady event for non-existing player",
                        cid
                    );
                }
            }
            Chunk::PlayerNew(ref p) => {
                assert!(0 <= p.cid && p.cid < self.players.len() as i32);
                self.implicit_tick(world, p.cid as u32, demo.as_deref_mut());

                if self.tees.len() as i32 <= p.cid {
                    self.tees.resize(p.cid as usize + 1, None);
                }

                if let Some(player) = self.players[p.cid as usize].as_mut() {
                    // ThPlayerEvent::Join only possible as long as teehistorian doesn't record player_ready messages+
                    assert!(matches!(player.event, ThPlayerEvent::InGame));
                    player.event = ThPlayerEvent::InGame;

                    assert_eq!(self.tees[p.cid as usize], None);
                    self.tees[p.cid as usize] = Some(ReplayerTeeInfo {
                        team: player.team,
                        pos: Vec2::new(p.x, p.y),
                        practice: self.teams[&player.team].practice,
                    });
                } else {
                    panic!(
                        "player_id {}: PlayerNew event for non-existing player",
                        p.cid
                    );
                }
                assert_ne!(self.players[p.cid as usize], None);
            }
            Chunk::PlayerDiff(ref p) => {
                assert!(0 <= p.cid && p.cid < self.players.len() as i32);
                self.implicit_tick(world, p.cid as u32, demo.as_deref_mut());
                if let Some(player) = self.players[p.cid as usize].as_mut() {
                    assert_eq!(player.event, ThPlayerEvent::InGame);
                    if let Some(tee) = self.tees[p.cid as usize].as_mut() {
                        tee.pos += Vec2::new(p.dx, p.dy);
                    } else {
                        panic!(
                            "player_id {}: PlayerDiff event before PlayerNew event",
                            p.cid
                        );
                    }
                } else {
                    panic!(
                        "player_id {}: PlayerDiff event for non-existing player",
                        p.cid
                    );
                }
            }
            Chunk::PlayerOld { cid } => {
                assert!(0 <= cid && cid < self.players.len() as i32);
                self.implicit_tick(world, cid as u32, demo.as_deref_mut());

                if let Some(player) = self.players[cid as usize].as_mut() {
                    // TODO: figure out why I can't do the following two checks
                    /*
                    if player.event != ThPlayerEvent::InGame && player.event != ThPlayerEvent::Drop
                    {
                        panic!("player_id {}: PlayerOld event in wrong state found {:?}, expected InGame or Drop", cid, player.event)
                    }
                    assert_ne!(self.tees[cid as usize], None); // Tee must exist
                    */
                    self.tees[cid as usize] = None;
                    if player.event == ThPlayerEvent::Drop {
                        self.players[cid as usize] = None;
                    }
                } else {
                    panic!("player_id {}: PlayerOld event for non-existing player", cid)
                }
            }

            Chunk::PlayerTeam { cid, team } => {
                assert!(0 <= cid && cid <= self.players.len() as i32);
                if let Some(player) = self.players[cid as usize].as_mut() {
                    player.team = team;
                }
                self.teams.entry(team).or_insert_with(ThTeam::new);
                if let Some(Some(tee)) = self.tees.get_mut(cid as usize).as_mut() {
                    tee.team = team;
                    tee.practice = self.teams[&team].practice;
                }
            }

            Chunk::TickSkip { dt } => {
                // explicits n+1 ticks (calling tick n times and setting delayed_tick to true)
                assert!(dt >= 0);
                if self.num_player == 0 {
                    // don't call tick() and validate() function on empty world + empty teehistorian file
                    let mut skip_ticks = false;
                    for i in (1..=dt).rev() {
                        // skip from the point when there are no entities remaining
                        skip_ticks = skip_ticks || world.is_empty();
                        if skip_ticks {
                            self.cur_time = self.cur_time + Duration::from_ticks(i);
                            self.last_tick_validated = false;
                            break;
                        } else {
                            self.tick(world, demo.as_deref_mut());
                        }
                    }
                } else {
                    for _ in 0..dt {
                        self.tick(world, demo.as_deref_mut());
                    }
                }
                self.implicit_tick_player_id = None;
                self.delayed_tick = true;
            }
            Chunk::PlayerSwap { cid1, cid2 } => {
                let id1: u32 = cid1.try_into().unwrap();
                let id2: u32 = cid2.try_into().unwrap();
                world.swap_tees(id1, id2);
            }

            Chunk::TeamPractice { team, practice } => {
                let practice = practice != 0;
                self.teams.get_mut(&team).unwrap().practice = practice;
                for tee in self.tees.iter_mut().flatten() {
                    if tee.team == team {
                        tee.practice = practice;
                    }
                }
            }
            Chunk::PlayerName(ref p) => {
                assert!(0 <= p.cid && p.cid <= self.players.len() as i32);
                let player = self.players[p.cid as usize]
                    .as_mut()
                    .expect("PlayerName for non-existing player");
                player.name = str::from_utf8(p.name).unwrap().to_owned();
            }
            Chunk::PlayerFinish { cid, time } => {
                let finish = Finishes::FinishTee(FinishTee {
                    name: self.player_name(cid).to_owned(),
                    time: Duration::from_ticks(time),
                });
                world.on_finish(self.cur_time, &finish);
            }
            Chunk::TeamFinish { team, time } => {
                let finish = Finishes::FinishTeam(FinishTeam {
                    team,
                    names: self
                        .team_names(team)
                        .iter()
                        .map(|&s| s.to_owned())
                        .collect(),
                    time: Duration::from_ticks(time),
                });
                world.on_finish(self.cur_time, &finish);
            }
            // TBD
            _ => {}
        }
        world.on_teehistorian_chunk(self.cur_time, &chunk);
    }
}
