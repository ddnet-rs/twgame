use serde::Deserialize;
use sha2::{Digest, Sha256};
use std::collections::HashSet;
use std::fs;
use std::fs::File;
use std::path::PathBuf;
use std::rc::Rc;
use teehistorian_replayer::ThReplayer;
use test_generator::test_resources;
use twgame::{Bug, Map, ThHeader};
use twgame_core::database;
use twgame_core::teehistorian::{ThBufReader, ThCompat, ThStream};
use twgame_core::twsnap::compat::ddnet::DemoKind;
use twgame_core::twsnap::compat::ddnet::DemoMapHash;
use twgame_core::twsnap::compat::ddnet::DemoWriter;
use twgame_core::twsnap::time::Duration;
use xdg::BaseDirectories;

const OVERWRITE_EXPECTED_RESULT_FILES: bool = false;

#[macro_use]
extern crate lazy_static;

lazy_static! {
    static ref EXPECT_FAILURES: HashSet<String> = "
        teehistorian_replayer_res_aipgores_noweapons-2min30_66s_teehistorian

        teehistorian_replayer_res_physics_1_spectator_mode_from_team12_to_team0_delay_teehistorian
        teehistorian_replayer_res_physics_2_finish_team0_1p_2x_teehistorian
        teehistorian_replayer_res_physics_2_finish_team0_1p_fast_teehistorian
        teehistorian_replayer_res_physics_2_finish_team0_1p_restart_1p_teehistorian
        teehistorian_replayer_res_physics_2_finish_team0_1p_teehistorian
        teehistorian_replayer_res_physics_2_finish_team_2p_restart_team0_1p_teehistorian
        teehistorian_replayer_res_physics_2_finish_team_2p_teehistorian
        teehistorian_replayer_res_physics_2_finish_team_3p_teehistorian
        teehistorian_replayer_res_physics_2_kill_protection_slash_active_after_20min_teehistorian
        teehistorian_replayer_res_physics_2_load_1p_teehistorian
        teehistorian_replayer_res_physics_2_multi_ninja_weapon_death_shields_teehistorian
        teehistorian_replayer_res_physics_2_save_2p_teehistorian
        teehistorian_replayer_res_physics_2_stopper_hook_inside_tee_teehistorian
        teehistorian_replayer_res_physics_2_stopper_inside_hook_tee_teehistorian
        teehistorian_replayer_res_physics_2_stopper_right_teehistorian
        teehistorian_replayer_res_physics_2_stopper_up_teehistorian
        teehistorian_replayer_res_physics_2_stopper_weapon_shotgun_teehistorian
        teehistorian_replayer_res_physics_2_tele_cp_4_random_out_of_many_teehistorian
        teehistorian_replayer_res_physics_4_tune_17_gun_curvature_6_teehistorian
        teehistorian_replayer_res_physics_4_tune_18_gun_speed_1100_teehistorian
        teehistorian_replayer_res_physics_4_tune_19_gun_lifetime_0_1_teehistorian
        teehistorian_replayer_res_physics_4_tune_21_grenade_speed_500_teehistorian
        teehistorian_replayer_res_physics_4_tune_35_gun_fire_delay_300_teehistorian
        teehistorian_replayer_res_physics_5_endless_hook_hold_save_load_teehistorian
        teehistorian_replayer_res_physics_5_endless_hook_hold_while_disable_and_solo_teehistorian
        teehistorian_replayer_res_physics_5_endless_hook_hold_while_disable_save_load_teehistorian
        teehistorian_replayer_res_physics_5_finish_on_player_disconnect_teehistorian
        teehistorian_replayer_res_physics_5_finish_on_player_kill_teehistorian
        teehistorian_replayer_res_physics_5_hook_into_death_id0_strong_teehistorian
        teehistorian_replayer_res_physics_5_hook_into_death_id0_weak_teehistorian
        teehistorian_replayer_res_physics_5_hook_into_death_id1_strong_teehistorian
        teehistorian_replayer_res_physics_5_hook_into_death_id1_weak_teehistorian
        teehistorian_replayer_res_physics_5_load_hook_wall_teehistorian
        teehistorian_replayer_res_physics_5_new_ninja_load_dash_teehistorian
        teehistorian_replayer_res_physics_5_new_ninja_load_finish_teehistorian
        teehistorian_replayer_res_physics_5_new_ninja_save_dash_teehistorian
        teehistorian_replayer_res_physics_5_new_ninja_save_load_teehistorian
        teehistorian_replayer_res_physics_5_new_ninja_save_teehistorian
        teehistorian_replayer_res_physics_5_no_finish_on_unstarted_left_team_teehistorian
        teehistorian_replayer_res_physics_5_powerup_nohit_grenade_teehistorian
        teehistorian_replayer_res_physics_5_powerup_nohit_laser_teehistorian
        teehistorian_replayer_res_physics_5_save_2sfreeze_front_0_activate_teehistorian
        teehistorian_replayer_res_physics_5_save_collision_game_1_stays_activated_teehistorian
        teehistorian_replayer_res_physics_5_save_deep_freeze_front_0_activate_teehistorian
        teehistorian_replayer_res_physics_5_save_deep_freeze_front_1_stays_activated_teehistorian
        teehistorian_replayer_res_physics_5_save_deep_freeze_game_0_activate_teehistorian
        teehistorian_replayer_res_physics_5_save_deep_freeze_game_1_stays_activated_teehistorian
        teehistorian_replayer_res_physics_5_save_got_grenade_front_2_deactivate_teehistorian
        teehistorian_replayer_res_physics_5_save_got_grenade_game_2_deactivate_teehistorian
        teehistorian_replayer_res_physics_5_save_got_laser_front_2_deactivate_teehistorian
        teehistorian_replayer_res_physics_5_save_got_laser_game_2_deactivate_teehistorian
        teehistorian_replayer_res_physics_5_save_got_ninja_front_0_activate_teehistorian
        teehistorian_replayer_res_physics_5_save_got_ninja_front_1_stays_activated_teehistorian
        teehistorian_replayer_res_physics_5_save_got_ninja_front_2_deactivate_teehistorian
        teehistorian_replayer_res_physics_5_save_got_ninja_game_0_activate_teehistorian
        teehistorian_replayer_res_physics_5_save_got_ninja_game_1_stays_activated_teehistorian
        teehistorian_replayer_res_physics_5_save_got_ninja_game_2_deactivate_teehistorian
        teehistorian_replayer_res_physics_5_save_got_shotgun_front_2_deactivate_teehistorian
        teehistorian_replayer_res_physics_5_save_got_shotgun_front_4_finish_teehistorian
        teehistorian_replayer_res_physics_5_save_got_shotgun_game_2_deactivate_teehistorian
        teehistorian_replayer_res_physics_5_save_heart_front_1_stays_activated_teehistorian
        teehistorian_replayer_res_physics_5_save_heart_front_4_finish_teehistorian
        teehistorian_replayer_res_physics_5_save_hook_wall_teehistorian
        teehistorian_replayer_res_physics_5_save_jump1_front_0_activate_teehistorian
        teehistorian_replayer_res_physics_5_save_jump1_front_1_stays_activated_teehistorian
        teehistorian_replayer_res_physics_5_save_lock_front_4_finish_teehistorian
        teehistorian_replayer_res_physics_5_save_nohit_game_1_stays_activated_teehistorian
        teehistorian_replayer_res_physics_5_save_nohit_grenade_front_0_activate_teehistorian
        teehistorian_replayer_res_physics_5_save_nohit_grenade_front_2_deactivate_teehistorian
        teehistorian_replayer_res_physics_5_save_nohit_hammer_front_0_activate_teehistorian
        teehistorian_replayer_res_physics_5_save_nohit_hammer_front_2_deactivate_teehistorian
        teehistorian_replayer_res_physics_5_save_nohit_laser_front_0_activate_teehistorian
        teehistorian_replayer_res_physics_5_save_nohit_laser_front_2_deactivate_teehistorian
        teehistorian_replayer_res_physics_5_save_nohit_shotgun_front_0_activate_teehistorian
        teehistorian_replayer_res_physics_5_save_nohit_shotgun_front_2_deactivate_teehistorian
        teehistorian_replayer_res_physics_5_save_selected_pistol_front_1_stays_activated_teehistorian
        teehistorian_replayer_res_physics_5_save_timeaward_front_0_activate_teehistorian
        teehistorian_replayer_res_physics_5_save_timeaward_front_1_stays_activated_teehistorian
        teehistorian_replayer_res_physics_5_save_timeaward_front_2_deactivate_teehistorian
        teehistorian_replayer_res_physics_5_save_timeaward_front_3_stays_deactivated_teehistorian
        teehistorian_replayer_res_physics_5_save_timeaward_front_4_finish_teehistorian
        teehistorian_replayer_res_physics_5_save_timecp_front_0_activate_teehistorian
        teehistorian_replayer_res_physics_5_save_timecp_front_1_stays_activated_teehistorian
        teehistorian_replayer_res_physics_5_save_timecp_game_0_activate_teehistorian
        teehistorian_replayer_res_physics_5_save_timecp_game_1_stays_activated_teehistorian
        teehistorian_replayer_res_physics_5_save_timepanelty_front_0_activate_teehistorian
        teehistorian_replayer_res_physics_5_save_timepanelty_front_1_stays_activated_teehistorian
        teehistorian_replayer_res_physics_5_save_timepanelty_front_4_finish_teehistorian
        teehistorian_replayer_res_physics_5_save_tune_front_0_activate_teehistorian
        teehistorian_replayer_res_physics_5_save_tune_front_1_stays_activated_teehistorian
        teehistorian_replayer_res_physics_5_save_tune_front_4_finish_teehistorian
        teehistorian_replayer_res_physics_5_save_tune_game_0_activate_teehistorian
        teehistorian_replayer_res_physics_5_save_tune_game_1_stays_activated_teehistorian
        teehistorian_replayer_res_physics_5_save_tune_game_2_deactivate_teehistorian
        teehistorian_replayer_res_physics_5_save_tune_game_3_stays_deactivated_teehistorian
        teehistorian_replayer_res_physics_5_save_tune_game_4_finish_teehistorian
        teehistorian_replayer_res_physics_5_tile_jump_refresher_teehistorian
        teehistorian_replayer_res_stronghold_playthrough_teehistorian
        teehistorian_replayer_res_tele_1_hook_tele_128_save_teehistorian
        teehistorian_replayer_res_tele_1_no_tele_weapon_shotgun_16_teehistorian
        teehistorian_replayer_res_tele_1_no_telecp_red_16_teehistorian
        teehistorian_replayer_res_tele_1_tele_blue_13_maxspeed_save_teehistorian
        teehistorian_replayer_res_tele_1_tele_blue_21_3deep_4totele_loop_teehistorian
        teehistorian_replayer_res_tele_1_tele_blue_5_maxspeed_mapborder_save_teehistorian
        teehistorian_replayer_res_tele_1_telecp_red_spawn_1_teehistorian
        teehistorian_replayer_res_tele_1_telegun_grenade_blue_teehistorian
        teehistorian_replayer_res_tele_1_telegun_grenade_tee_down_teehistorian
        teehistorian_replayer_res_tele_1_telegun_grenade_tee_fly_teehistorian
        teehistorian_replayer_res_tele_1_telegun_grenade_tee_left_teehistorian
        teehistorian_replayer_res_tele_1_telegun_grenade_tee_right_teehistorian
        teehistorian_replayer_res_tele_1_telegun_grenade_tee_up_teehistorian
        teehistorian_replayer_res_tele_1_telegun_grenade_tee_velocity_teehistorian
        teehistorian_replayer_res_tele_1_telegun_grenade_weapon_tele_160_blue_teehistorian
        teehistorian_replayer_res_tele_1_telegun_grenade_weapon_tele_161_blue_teehistorian
        teehistorian_replayer_res_tele_1_telegun_grenade_weapon_tele_161_yellow_teehistorian
        teehistorian_replayer_res_tele_1_telegun_grenade_yellow_teehistorian
        teehistorian_replayer_res_tele_1_telegun_gun_blue_teehistorian
        teehistorian_replayer_res_tele_1_telegun_gun_tee_down_teehistorian
        teehistorian_replayer_res_tele_1_telegun_gun_tee_fly_teehistorian
        teehistorian_replayer_res_tele_1_telegun_gun_tee_left_teehistorian
        teehistorian_replayer_res_tele_1_telegun_gun_tee_right_teehistorian
        teehistorian_replayer_res_tele_1_telegun_gun_tee_up_teehistorian
        teehistorian_replayer_res_tele_1_telegun_gun_weapon_tele_160_tee_teehistorian
        teehistorian_replayer_res_tele_1_telegun_gun_weapon_tele_161_blue_teehistorian
        teehistorian_replayer_res_tele_1_telegun_gun_weapon_tele_161_yellow_teehistorian
        teehistorian_replayer_res_tele_1_telegun_gun_yellow_teehistorian
        teehistorian_replayer_res_tele_1_telegun_laser_air_reflection_teehistorian
        teehistorian_replayer_res_tele_1_telegun_laser_air_teehistorian
        teehistorian_replayer_res_tele_1_telegun_laser_blue_teehistorian
        teehistorian_replayer_res_tele_1_telegun_laser_tee_fly_teehistorian
        teehistorian_replayer_res_tele_1_telegun_laser_tee_left_teehistorian
        teehistorian_replayer_res_tele_1_telegun_laser_tee_up_teehistorian
        teehistorian_replayer_res_tele_1_telegun_laser_weapon_tele_144_self_teehistorian
        teehistorian_replayer_res_tele_1_telegun_laser_weapon_tele_160_blue_teehistorian
        teehistorian_replayer_res_tele_1_telegun_laser_weapon_tele_161_yellow_teehistorian
        teehistorian_replayer_res_tele_1_telegun_laser_yellow_teehistorian
        teehistorian_replayer_res_tele_1_weapon_144_shotgun_loop_teehistorian // HandleTile
        teehistorian_replayer_res_tele_1_weapon_146_random_while_tee_tele_loop_teehistorian
        teehistorian_replayer_res_tuning_tuning_front_endless_hook_teehistorian
        teehistorian_replayer_res_tuning_tuning_front_hammer_off_teehistorian
        teehistorian_replayer_res_tuning_tuning_front_old_shotgun_teehistorian
        teehistorian_replayer_res_tuning_tuning_game_endless_hook_teehistorian
        teehistorian_replayer_res_tuning_tuning_game_hammer_off_teehistorian
        teehistorian_replayer_res_tuning_tuning_game_old_shotgun_teehistorian
        teehistorian_replayer_res_switcher_1_deep_off_3s_teehistorian
        teehistorian_replayer_res_switcher_1_deep_on_3s_teehistorian
        teehistorian_replayer_res_switcher_1_deep_on_undeep_off_teehistorian
        teehistorian_replayer_res_switcher_1_deep_on_undeep_on_teehistorian
        teehistorian_replayer_res_switcher_1_explosive_bullets_left_teehistorian
        teehistorian_replayer_res_switcher_1_explosive_bullets_on_off_teehistorian
        teehistorian_replayer_res_switcher_1_explosive_bullets_right_teehistorian
        teehistorian_replayer_res_switcher_1_explosivefreeze_bullets_left_teehistorian
        teehistorian_replayer_res_switcher_1_explosivefreeze_bullets_on_off_teehistorian
        teehistorian_replayer_res_switcher_1_explosivefreeze_bullets_right_teehistorian
        teehistorian_replayer_res_switcher_1_freeze_1s_10s_on_teehistorian
        teehistorian_replayer_res_switcher_1_freeze_1s_default_on_teehistorian
        teehistorian_replayer_res_switcher_1_freeze_1s_off_3s_teehistorian
        teehistorian_replayer_res_switcher_1_freeze_1s_on_3s_teehistorian
        teehistorian_replayer_res_switcher_1_freeze_bullets_left_teehistorian
        teehistorian_replayer_res_switcher_1_freeze_bullets_on_off_teehistorian
        teehistorian_replayer_res_switcher_1_freeze_bullets_right_teehistorian
        teehistorian_replayer_res_switcher_1_grenade_on_grenade_shield_off_teehistorian
        teehistorian_replayer_res_switcher_1_grenade_on_grenade_shield_on_teehistorian
        teehistorian_replayer_res_switcher_1_grenade_telegun_teehistorian
        teehistorian_replayer_res_switcher_1_gun_telegun_teehistorian
        teehistorian_replayer_res_switcher_1_heart_off_3s_teehistorian
        teehistorian_replayer_res_switcher_1_heart_on_3s_teehistorian
        teehistorian_replayer_res_switcher_1_jump_1_teehistorian
        teehistorian_replayer_res_switcher_1_jump_multiple_teehistorian
        teehistorian_replayer_res_switcher_1_livefreeze_on_unlivefreeze_off_teehistorian
        teehistorian_replayer_res_switcher_1_livefreeze_on_unlivefreeze_on_teehistorian
        teehistorian_replayer_res_switcher_1_ninja_on_ninja_shield_on_teehistorian
        teehistorian_replayer_res_switcher_1_ninja_on_shield_off_teehistorian
        teehistorian_replayer_res_switcher_1_ninja_on_shield_on_teehistorian
        teehistorian_replayer_res_switcher_1_ninja_on_teehistorian
        teehistorian_replayer_res_switcher_1_rifle_on_rifle_shield_off_teehistorian
        teehistorian_replayer_res_switcher_1_rifle_on_shield_off_teehistorian
        teehistorian_replayer_res_switcher_1_rifle_on_teehistorian
        teehistorian_replayer_res_switcher_1_rifle_telegun_teehistorian
        teehistorian_replayer_res_switcher_1_shotgun_on_shield_off_teehistorian
        teehistorian_replayer_res_switcher_1_shotgun_on_shotgun_shield_on_teehistorian
        teehistorian_replayer_res_switcher_1_unfreeze_bullets_deep_teehistorian
        teehistorian_replayer_res_switcher_1_unfreeze_bullets_freeze_hook_teehistorian
        teehistorian_replayer_res_switcher_1_unfreeze_bullets_right_teehistorian
        teehistorian_replayer_res_physics_7_stopper_dragger_medium_hookground_teehistorian
        teehistorian_replayer_res_physics_7_stopper_dragger_medium_stronghook_teehistorian
        teehistorian_replayer_res_physics_7_stopper_dragger_medium_up_teehistorian
        teehistorian_replayer_res_physics_7_stopper_dragger_medium_weakhook_teehistorian
        teehistorian_replayer_res_physics_7_stopper_dragger_strong_hookground_teehistorian
        teehistorian_replayer_res_physics_7_stopper_dragger_strong_stronghook_teehistorian
        teehistorian_replayer_res_physics_7_stopper_dragger_strong_up_teehistorian
        teehistorian_replayer_res_physics_7_stopper_dragger_strong_weakhook_teehistorian
        teehistorian_replayer_res_physics_7_stopper_dragger_weak_hookground_teehistorian
        teehistorian_replayer_res_physics_7_stopper_dragger_weak_stronghook_teehistorian
        teehistorian_replayer_res_physics_7_stopper_dragger_weak_up_teehistorian
        teehistorian_replayer_res_physics_7_stopper_dragger_weak_weakhook_teehistorian
        teehistorian_replayer_res_physics_7_stopper_hook_down_left_weak_teehistorian
        teehistorian_replayer_res_physics_7_stopper_hook_wall_left_teehistorian
        teehistorian_replayer_res_physics_7_stopper_hook_wall_up_teehistorian
        teehistorian_replayer_res_physics_7_stopper_jetpack_skip_down_teehistorian
        teehistorian_replayer_res_physics_7_stopper_walldragger_medium_up_teehistorian
        teehistorian_replayer_res_physics_7_stopper_walldragger_strong_up_teehistorian
        teehistorian_replayer_res_physics_7_stopper_walldragger_weak_up_teehistorian

        // Maybe due to recording tee positions happened (before spawn logic)
        // https://github.com/ddnet/ddnet/commit/6c452c15cfe1802a956f8e12e0bbb6b3d1984232
        teehistorian_replayer_res_compat_11_0_1_player_teehistorian
        teehistorian_replayer_res_compat_11_0_2_player_teehistorian
        teehistorian_replayer_res_compat_11_0_3_player_teehistorian
        teehistorian_replayer_res_compat_11_0_player_teehistorian
        teehistorian_replayer_res_compat_11_1_1_player_teehistorian
        teehistorian_replayer_res_compat_11_1_2_player_teehistorian
        teehistorian_replayer_res_compat_11_1_3_player_teehistorian
        teehistorian_replayer_res_compat_11_1_4_player_teehistorian
        teehistorian_replayer_res_compat_11_1_5_player_teehistorian
        teehistorian_replayer_res_compat_11_1_6_player_teehistorian
        teehistorian_replayer_res_compat_11_1_7_player_teehistorian
        teehistorian_replayer_res_compat_11_1_8_player_teehistorian
        teehistorian_replayer_res_compat_11_1_9_player_teehistorian
        teehistorian_replayer_res_compat_11_1_player_teehistorian
        teehistorian_replayer_res_compat_11_2_1_player_teehistorian
        teehistorian_replayer_res_compat_11_2_player_teehistorian
        teehistorian_replayer_res_compat_11_3_1_player_teehistorian
        teehistorian_replayer_res_compat_11_3_player_teehistorian
        teehistorian_replayer_res_compat_11_4_1_player_teehistorian
        teehistorian_replayer_res_compat_11_4_2_player_teehistorian
        teehistorian_replayer_res_compat_11_4_3_player_teehistorian
        teehistorian_replayer_res_compat_11_4_4_player_teehistorian
        teehistorian_replayer_res_compat_11_4_5_player_teehistorian
        teehistorian_replayer_res_compat_11_4_6_player_teehistorian
        teehistorian_replayer_res_compat_11_4_player_teehistorian
        teehistorian_replayer_res_compat_11_5_1_player_teehistorian
        teehistorian_replayer_res_compat_11_5_player_teehistorian
        teehistorian_replayer_res_compat_11_6_1_player_teehistorian
        teehistorian_replayer_res_compat_11_6_player_teehistorian
        teehistorian_replayer_res_compat_11_7_1_player_teehistorian
        teehistorian_replayer_res_compat_11_7_2_player_teehistorian
        teehistorian_replayer_res_compat_11_7_player_teehistorian
        teehistorian_replayer_res_compat_11_8_player_teehistorian
        teehistorian_replayer_res_compat_11_9_player_teehistorian
        teehistorian_replayer_res_compat_12_0_1_player_teehistorian
        teehistorian_replayer_res_compat_12_0_player_teehistorian
        teehistorian_replayer_res_compat_12_1_player_teehistorian
        teehistorian_replayer_res_compat_12_2_player_teehistorian
        teehistorian_replayer_res_compat_12_3_1_player_teehistorian
        teehistorian_replayer_res_compat_12_3_player_teehistorian
        teehistorian_replayer_res_compat_12_4_1_player_teehistorian
        teehistorian_replayer_res_compat_12_4_2_player_teehistorian
        teehistorian_replayer_res_compat_12_4_3_player_teehistorian
        teehistorian_replayer_res_compat_12_4_player_teehistorian
        teehistorian_replayer_res_compat_12_5_player_teehistorian
        teehistorian_replayer_res_compat_12_6_1_player_teehistorian
        teehistorian_replayer_res_compat_12_6_player_teehistorian
        teehistorian_replayer_res_compat_12_7_1_player_teehistorian
        teehistorian_replayer_res_compat_12_7_2_player_teehistorian
        teehistorian_replayer_res_compat_12_7_player_teehistorian
        teehistorian_replayer_res_compat_12_8_1_player_teehistorian
        teehistorian_replayer_res_compat_12_8_player_teehistorian
        teehistorian_replayer_res_compat_12_9_1_player_teehistorian
        teehistorian_replayer_res_compat_12_9_2_player_teehistorian
        teehistorian_replayer_res_compat_13_0_1_player_teehistorian
        teehistorian_replayer_res_compat_13_0_player_teehistorian
        teehistorian_replayer_res_compat_13_1_player_teehistorian
        teehistorian_replayer_res_compat_13_2_1_player_teehistorian
        teehistorian_replayer_res_compat_13_2_2_player_teehistorian
        teehistorian_replayer_res_compat_13_2_player_teehistorian
        teehistorian_replayer_res_compat_14_1_player_teehistorian
        teehistorian_replayer_res_compat_14_2_player_teehistorian
        teehistorian_replayer_res_compat_15_2_1_player_teehistorian
        teehistorian_replayer_res_compat_15_2_2_player_teehistorian
        teehistorian_replayer_res_compat_15_2_3_player_teehistorian
        teehistorian_replayer_res_compat_15_2_4_player_teehistorian
        teehistorian_replayer_res_compat_15_2_5_player_teehistorian
        teehistorian_replayer_res_compat_15_2_player_teehistorian
        teehistorian_replayer_res_compat_15_3_1_player_teehistorian
        teehistorian_replayer_res_compat_15_3_2_player_teehistorian
        teehistorian_replayer_res_compat_15_3_player_teehistorian
        teehistorian_replayer_res_compat_15_4_player_teehistorian
        teehistorian_replayer_res_compat_15_5_1_player_teehistorian
        teehistorian_replayer_res_compat_15_5_2_player_teehistorian
        teehistorian_replayer_res_compat_15_5_3_player_teehistorian
        teehistorian_replayer_res_compat_15_5_4_player_teehistorian
        teehistorian_replayer_res_compat_15_5_player_teehistorian
        teehistorian_replayer_res_compat_15_6_1_player_teehistorian
        teehistorian_replayer_res_compat_15_6_2_player_teehistorian
        teehistorian_replayer_res_compat_15_6_player_teehistorian
        teehistorian_replayer_res_compat_15_7_player_teehistorian
        teehistorian_replayer_res_compat_15_8_1_player_1_teehistorian
        teehistorian_replayer_res_compat_15_8_player_1_teehistorian
        teehistorian_replayer_res_compat_15_9_1_player_1_teehistorian
        teehistorian_replayer_res_compat_15_9_player_1_teehistorian
        "
    .split_whitespace()
    .map(|s| s.to_string())
    .collect();
}

#[derive(Deserialize)]
struct FinishTee {
    players: String,
    time: f32,
}

#[derive(Deserialize)]
struct FinishTeam {
    team: i32,
    players: Vec<String>,
    time: f32, // in seconds
}

#[derive(Deserialize)]
#[serde(untagged)]
enum Finish {
    FinishTee(FinishTee),
    FinishTeam(FinishTeam),
}

#[derive(Deserialize)]
struct TestConfig {
    finishes: Vec<Finish>,
}

impl TestConfig {
    fn database_events(&self) -> Vec<database::Finishes> {
        self.finishes
            .iter()
            .map(|finish| match finish {
                Finish::FinishTee(tee) => database::Finishes::FinishTee(database::FinishTee {
                    name: tee.players.clone(),
                    time: Duration::from_secs_f32(tee.time),
                }),
                Finish::FinishTeam(team) => database::Finishes::FinishTeam(database::FinishTeam {
                    team: team.team,
                    names: team.players.clone(),
                    time: Duration::from_secs_f32(team.time),
                }),
            })
            .collect()
    }
}

fn expect_failure(path: &str) -> bool {
    let test_name = format!("teehistorian_{}", path.replace(['/', '.'], "_"));
    EXPECT_FAILURES.contains(&test_name)
}

fn prepare_demo_dir(teehistorian: &str, prefix: &str) -> PathBuf {
    // get file name without extension
    let mut th_file = teehistorian.rsplit('/');
    let th_file_name = th_file.next().unwrap();
    let th_folder_name = th_file.next().unwrap();
    let demo_name = format!(
        "demos/twgame-unittest/{}_{}_{}.demo",
        prefix,
        th_folder_name,
        &th_file_name[..th_file_name.len() - 13]
    );

    // setup directories
    let dirs = BaseDirectories::with_prefix("ddnet").unwrap();
    dirs.place_data_file(demo_name)
        .expect("failed to create directories")
}

fn _write_demo(teehistorian: &str, prefix: &str, demo: &[u8]) {
    let out_dir = prepare_demo_dir(teehistorian, prefix);
    fs::write(out_dir, demo).unwrap();
}

#[test_resources("replayer/res/*/*.teehistorian")]
fn twgame(teehistorian: &str) {
    test_teehistorian(teehistorian, "twgame", false);
}

#[test_resources("replayer/res/*/*.teehistorian")]
fn ddnet(teehistorian: &str) {
    test_teehistorian(teehistorian, "ddnet", true);
}

// https://stackoverflow.com/a/43093371
use std::sync::Once;
static INIT: Once = Once::new();
/// Setup function that is only run once, even if called multiple times.
fn setup_logger() {
    INIT.call_once(|| {
        env_logger::Builder::new()
            .filter_level(log::LevelFilter::Trace)
            .format_level(false)
            .format_target(false)
            .format_timestamp(None)
            .is_test(true)
            .init()
    });
}

fn test_teehistorian(teehistorian: &str, prefix: &str, compat: bool) {
    setup_logger();

    let th_path = format!("../{teehistorian}"); // up one level, because of workspace
    println!(
        "Teehistorian: {}",
        fs::canonicalize(&th_path).unwrap().display()
    );
    let f = File::open(th_path).unwrap();

    let mut th = ThCompat::parse(ThBufReader::new(f)).unwrap();
    println!("{}", String::from_utf8_lossy(th.header().unwrap()));
    let header_raw = th.header().unwrap();
    let header = ThHeader::from_buf(header_raw);

    let map_name = format!("res/maps/{}.map", header.map_name);
    println!("loading map: {map_name}");

    let bytes = std::fs::read(&map_name).unwrap(); // Vec<u8>
    let mut hasher = Sha256::new();
    hasher.update(bytes);
    let map_sha256 = hasher.finalize();

    let map = fs::read(&map_name).unwrap();
    let mut map = twmap::TwMap::parse(&map).unwrap();
    let map = Map::try_from(&mut map).unwrap();
    let map = Rc::new(map);

    let mut world = twgame::DdnetReplayerWorld::new(map, compat);

    // gather tele compat data if available
    let prng_compat_file = format!("../{teehistorian}.prng.json");
    let prng_compat_data = {
        if let Ok(content) = fs::read_to_string(&prng_compat_file) {
            if !compat {
                world.supply_prng_compat_data(&content)
            }
            Some(content)
        } else {
            None
        }
    };

    // parse expected bugs file
    let expected_bugs_file = format!("../{teehistorian}.bugs.json");
    let expected_bugs: Vec<Bug> = if let Ok(content) = fs::read_to_string(&expected_bugs_file) {
        serde_json::from_str(&content).unwrap()
    } else {
        vec![]
    };

    //let demo_out = Cursor::new(Vec::new());
    let demo_out_file = prepare_demo_dir(teehistorian, prefix);
    let demo_out = File::create(&demo_out_file).unwrap();
    let mut demo = Box::new(
        DemoWriter::new(
            demo_out,
            DemoKind::Server,
            &header.start_time,
            "0.6 TwGame", // TODO
            &header.map_name,
            None,
            DemoMapHash::Sha256(<[u8; 32]>::from(map_sha256)),
            0,
        )
        .unwrap(),
    );

    let replayer = ThReplayer::new(header_raw, &mut world);
    let finishes = {
        let database_events_file = format!("../{teehistorian}.json");
        if let Ok(content) = fs::read_to_string(database_events_file) {
            let content: TestConfig = serde_json::from_str(&content).unwrap();
            content.database_events()
        } else {
            vec![]
        }
    };
    #[allow(deprecated)] // only place this is allowed to be called :D
    world.with_expected_finishes(finishes);
    println!("Start validation");
    let expected_valid = !expect_failure(teehistorian);
    let output = if expected_valid {
        "expected test to pass"
    } else {
        "expected test failure. Test is passing now. It can be removed from exception array!"
    };
    replayer.validate(&mut world, &mut th, Some(demo.as_mut()));
    let correct = world.is_correct();
    if !correct && expected_valid {
        //TODO: write_demo(teehistorian, demo_out.get_ref());
        //TODO: otherwise remove demo?
    }
    // TODO: removeme, when I can only write the demo to fs on unexpected result
    if correct {
        fs::remove_file(&demo_out_file).unwrap();
    }
    assert_eq!(correct, expected_valid, "{}", output);
    if compat {
        let new_compat_data = world
            .retrieve_prng_compat_data()
            .expect("compat data was collected");

        println!("Prng Compat Data:");
        println!("{}", new_compat_data);
        if OVERWRITE_EXPECTED_RESULT_FILES
            && !new_compat_data.is_empty()
            && &new_compat_data != prng_compat_data.as_ref().unwrap_or(&String::new())
        {
            // write content of compat data for the next run for convenience.
            fs::write(&prng_compat_file, &new_compat_data).unwrap();
        }
        // only assert compat data if the run was successfully reproduced
        if correct {
            if let Some(prng_compat_data) = prng_compat_data {
                assert_eq!(new_compat_data, prng_compat_data);
            } else {
                assert!(new_compat_data.is_empty())
            }
        }
    }
    // assert that we detected the expected bugs
    let bugs = world.retrieve_bugs();
    let got_bugs = serde_json::to_string(&bugs).unwrap();
    println!("got bugs: {}", got_bugs);
    if OVERWRITE_EXPECTED_RESULT_FILES && !bugs.is_empty() && bugs != expected_bugs {
        // write content of bugs for the next test run for convenience
        fs::write(&expected_bugs_file, got_bugs).unwrap();
    }
    assert_eq!(expected_bugs, bugs);
}
