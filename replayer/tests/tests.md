# Physics test files

## `physics_test_1.map`

1. jump.teehistorian: player enters and jumps a single time
2. double-jump.teehistorian: player double-jumps multiple times on the spawn location
3. walk-left.teehistorian: player walks left a bit, halts and walks left until the wall
4. walk-right.teehistorian: player walks right a bit, halts and walks right until the wall
5. jump-left-right.teehistorian: player jumps left with different speed and then right with different speed
6. unhookable.teehistorian: player hooks unhookable block left and right
7. hookthrough.teehistorian: test hookthrough block (as blocker in air and hookthrough down)
8. old-hookthrough.teehistorian: test old hookthrough block from above
9. hook-nomove.teehistorian: test hook left and right withough pressing a or d while hooking
