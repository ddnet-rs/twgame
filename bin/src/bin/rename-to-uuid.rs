use clap::Parser;
use serde::Deserialize;
use std::fs;
use std::fs::File;
use std::path::Path;
use teehistorian::{Th, ThBufReader};
use uuid::Uuid;

/// Teehistorian to uuid
///
/// Gets a directory of files and renames them to the game-uuid outputs it to specified path
#[derive(Debug, Parser)]
#[command(author, version, about)]
struct Opt {
    /// path to map file to do physics simulation on
    in_path: String,
    out_path: String,
}

#[derive(Deserialize, Debug)]
struct ThHeader {
    pub game_uuid: Uuid,
}
fn teehistorian_parse_header(filename: &Path) -> Result<ThHeader, String> {
    let f = File::open(filename).map_err(|err| err.to_string())?;
    match Th::parse(ThBufReader::new(f)) {
        Ok(mut th_reader) => {
            // it is a teehistorian file, now try to extract the header
            match th_reader.header() {
                Ok(header) => {
                    // now try to parse the header
                    match serde_json::from_slice::<ThHeader>(header) {
                        Ok(th_header) => Ok(th_header),
                        Err(err) => Err(format!(
                            "{:?} Error parsing header with serde: {}",
                            filename, err
                        )),
                    }
                }
                Err(teehistorian::Error::IoError(err)) => Err(err.to_string()),
                Err(err) => {
                    // non-parseable header
                    Err(format!("{:?} Error parsing header: {}", filename, err))
                }
            }
        }
        Err(teehistorian::Error::IoError(err)) => Err(err.to_string()),
        Err(err) => {
            // non-parseable header
            Err(format!("{:?} Error parsing header: {}", filename, err))
        }
    }
}
fn traverse_teehistorian_files(path: &str, out: &str) {
    let new_teehistorians = walkdir::WalkDir::new(path)
        .into_iter()
        // filter out errors
        .filter_map(|e| match e {
            Ok(f) => Some(f),
            Err(err) => {
                println!("Error directory {}", err);
                None
            }
        });
    for path in new_teehistorians {
        match teehistorian_parse_header(path.path()) {
            Ok(th) => {
                fs::copy(path.path(), format!("{out}/{}.teehistorian", th.game_uuid)).unwrap();
            }
            Err(err) => println!("traverse_teehistorian_files: {}", err),
        }
    }
}
fn main() {
    // parse arguments
    let opt = Opt::parse();

    traverse_teehistorian_files(&opt.in_path, &opt.out_path);
}
