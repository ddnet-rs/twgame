use chrono::{TimeZone, Utc};
use clap::Parser;
use rayon::prelude::*;
use serde::Deserialize;
use std::fs::File;
use std::path::Path;
use teehistorian::{Chunk, Error, ThBufReader, ThCompat, ThStream};
use teehistorian_replayer::ThReplayer;
use twgame_core::console::Command;
use twgame_core::database::Finishes;
use twgame_core::net_msg::ClNetMessage;
use twgame_core::replay::{GameValidator, ReplayerChecker, ReplayerCheckerHelper};
use twgame_core::Snapper;
use twgame_core::{Game, Input};
use twsnap::time::Instant;
use twsnap::Snap;
use vek::Vec2;

struct Player {
    name: String,
    team: i32,
}

impl Player {
    fn new() -> Self {
        Self {
            name: "".to_string(),
            team: 0,
        }
    }
}

struct Team {
    num_members: u32,
    practice: bool,
    bug: bool,
}

impl Team {
    fn new() -> Self {
        Self {
            num_members: 0,
            practice: false,
            bug: false,
        }
    }
}

struct Bug8199GameWorld {
    th_header: ThHeader,
    cur_time: Instant,
    players: [Player; 64],
    teams: [Team; 64],
}

impl Bug8199GameWorld {
    fn new(th_header: ThHeader) -> Self {
        Self {
            th_header,
            cur_time: Instant::zero(),
            players: std::array::from_fn(|_| Player::new()),
            teams: std::array::from_fn(|_| Team::new()),
        }
    }

    fn print_player(&self, player: &Player, id: u32, reason: &str) {
        println!(
            "version='{}', server_start_time='{}', game_uuid={}, time='{}', player_id={}, player={:?}, map={:?}, team={}, reason={}",
            self.th_header.server_version,
            self.th_header.start_time,
            self.th_header.game_uuid,
            self.cur_time,
            id,
            player.name,
            self.th_header.map_name,
            player.team,
            reason,
        );
    }
}

impl Game for Bug8199GameWorld {
    fn player_join(&mut self, id: u32) {
        self.players[id as usize].name.clear();
    }

    // no need to implement anything here
    fn player_ready(&mut self, _id: u32) {}
    fn player_input(&mut self, _id: u32, _input: &Input) {}

    fn player_leave(&mut self, id: u32) {
        // check if last player leaving the team -> Team might get reset
        let old_id = self.players[id as usize].team;
        self.players[id as usize].team = 0;
        if old_id != 0 {
            let old_team = &mut self.teams[old_id as usize];
            assert!(old_team.num_members > 0);
            old_team.num_members -= 1;
            if old_team.num_members == 0 {
                old_team.bug = false;
            }
        }
    }

    fn on_net_msg(&mut self, id: u32, msg: &ClNetMessage) {
        let (info, reason) = match msg {
            ClNetMessage::ClStartInfo(info) => (info, "start_name"),
            ClNetMessage::ClChangeInfo(info) => (info, "change_name"),
            _ => return,
        };
        let name = String::from_utf8_lossy(info.name);
        let p = &mut self.players[id as usize];
        if name != p.name {
            p.name = name.to_string();
            if self.teams[p.team as usize].bug {
                let p = &self.players[id as usize];
                self.print_player(p, id, reason);
            }
        }
    }

    fn on_command(&mut self, id: u32, command: &Command) {
        if matches!(command, Command::Team0Mode) {
            let team_id = self.players[id as usize].team;
            let team = &mut self.teams[team_id as usize];
            if team.practice {
                team.bug = true;
                for (player_id, player) in self.players.iter().enumerate() {
                    if player.team == team_id {
                        if player_id as u32 == id {
                            self.print_player(player, player_id as u32, "activate_bug");
                        } else {
                            self.print_player(player, player_id as u32, "initial_team");
                        }
                    }
                }
            }
        }
    }

    fn swap_tees(&mut self, _id1: u32, _id2: u32) {}

    fn tick(&mut self, cur_time: Instant) {
        self.cur_time = cur_time;
    }

    fn is_empty(&self) -> bool {
        true
    }
}

impl ReplayerChecker for Bug8199GameWorld {
    fn check_tees(
        &mut self,
        cur_time: Instant,
        tees: &[Option<twgame_core::replay::ReplayerTeeInfo>],
        demo: twgame_core::replay::DemoChatPtr,
    ) {
        (self as &mut dyn ReplayerCheckerHelper).check_tees(cur_time, tees, demo);
    }
    fn finalize(&mut self) {}
    fn on_finish(&mut self, _now: Instant, _finish: &Finishes) {}
    fn on_teehistorian_chunk(&mut self, _now: Instant, _chunk: &Chunk) {}
    fn on_teehistorian_header(&mut self, _header: &[u8]) {}
}

impl Snapper for Bug8199GameWorld {
    fn snap(&self, _snapshot: &mut Snap) {}
}

impl GameValidator for Bug8199GameWorld {
    fn set_player_team(&mut self, id: u32, team: i32) {
        if self.players[id as usize].team != 0 {
            let old_id = self.players[id as usize].team;
            let old_team = &mut self.teams[old_id as usize];
            assert!(old_team.num_members > 0);
            old_team.num_members -= 1;
            if old_team.num_members == 0 {
                old_team.bug = false;
            }
        }
        self.players[id as usize].team = team;
        if team != 0 {
            let new_team = &mut self.teams[team as usize];
            new_team.num_members += 1;
            if new_team.bug {
                self.print_player(&self.players[id as usize], id, "join_team");
            }
        }
    }

    fn tee_pos(&self, _id: u32) -> Option<Vec2<i32>> {
        None
    }

    fn max_tee_id(&self) -> u32 {
        0
    }

    fn set_tee_pos(&mut self, _id: u32, _pos: Option<Vec2<i32>>) {}

    fn player_team(&self, id: u32) -> i32 {
        self.players[id as usize].team
    }
}

fn bug_8199_info(th_header: ThHeader, th: &mut ThCompat<ThBufReader<File>>) {
    let mut world = Bug8199GameWorld::new(th_header);

    let replayer = ThReplayer::new(th.header().unwrap(), &mut world);
    replayer.validate(&mut world, th, None);
}

#[derive(Debug, Parser)]
/// Written for investigate of abuse of https://github.com/ddnet/ddnet/pull/8199
#[command(author, version, about)]
struct Opt {
    /// directory to search for empty teehistorian files.
    #[arg(default_value = ".")]
    dir: String,
}

#[derive(Deserialize, Debug)]
pub struct ThHeader {
    pub game_uuid: String,
    pub server_version: String,
    pub start_time: String,
    pub map_name: String,
}

fn bug_8199(mut th: ThCompat<ThBufReader<File>>) -> bool {
    let mut players: [i32; 64] = std::array::from_fn(|_| 0);
    let mut practice: [bool; 64] = std::array::from_fn(|_| false);
    loop {
        match th.next_chunk() {
            Ok(Chunk::PlayerTeam { cid, team }) => {
                let id = cid as usize;
                players[id] = team;
            }
            Ok(Chunk::ConsoleCommand(cmd)) => {
                if cmd.cid < 0 {
                    continue;
                }
                let id = cmd.cid as usize;
                let team = players[id] as usize;
                if cmd.cmd == b"team0mode" && practice[team] {
                    return true;
                }
            }
            Ok(Chunk::TeamPractice {
                team,
                practice: is_practice,
            }) => {
                practice[team as usize] = is_practice != 0;
            }
            Ok(_) => {}
            Err(Error::Eof) => {
                // ignore
                return false;
            }
            Err(_err) => {
                // also ignore other errors here
                //println!("game_uuid={}, filename={}, error={}", uuid, filename, err);
                return false;
            }
        }
    }
}

fn is_teehistorian(entry: &Path) -> bool {
    matches!(entry.extension(), Some(e) if e.to_string_lossy().to_lowercase() == "teehistorian")
}

fn in_time_range(filename: &str) -> bool {
    // get new file attributes and check if we want to (re)parse the header
    let Ok(file_metadata) = std::fs::metadata(filename) else {
        return false;
    };
    let Ok(file_modified) = file_metadata.modified() else {
        return false;
    };
    let file_modified = chrono::DateTime::<chrono::Utc>::from(file_modified);

    let from = Utc.with_ymd_and_hms(2024, 4, 7, 0, 0, 0).unwrap();
    let to = Utc.with_ymd_and_hms(2024, 4, 13, 0, 0, 0).unwrap();
    from < file_modified && file_modified < to
}

fn main() {
    let opt = Opt::parse();

    rayon::ThreadPoolBuilder::new()
        .num_threads(num_cpus::get() * 2)
        .build_global()
        .unwrap();

    walkdir::WalkDir::new(opt.dir)
        .into_iter()
        .par_bridge()
        .filter_map(|e| match e {
            Ok(f) => Some(f),
            Err(err) => {
                eprintln!("Error directory {}", err);
                None
            }
        })
        .filter(|e| !e.path().is_dir() && is_teehistorian(e.path()))
        .filter_map(
            |file| match file.into_path().into_os_string().into_string() {
                Ok(f) => Some(f),
                Err(err) => {
                    eprintln!("Convert to utf-8 error in file {:?}", err);
                    None
                }
            },
        )
        .filter(|filename| in_time_range(filename))
        .for_each(|filename| {
            let f = if let Ok(f) = File::open(&filename).map_err(|err| err.to_string()) {
                f
            } else {
                return;
            };
            match ThCompat::parse(ThBufReader::new(f)) {
                Ok(mut th_reader) => {
                    // it is a teehistorian file, now try to extract the header
                    match th_reader.header() {
                        Ok(header) => {
                            let th_header = match serde_json::from_slice::<ThHeader>(header) {
                                Ok(th_header) => th_header,
                                Err(err) => {
                                    eprintln!(
                                        "{} Error parsing header with serde: {}",
                                        filename, err
                                    );
                                    return;
                                }
                            };
                            if bug_8199(th_reader) {
                                // print the info
                                let f = File::open(&filename).unwrap();
                                let mut th = ThCompat::parse(ThBufReader::new(f)).unwrap();
                                bug_8199_info(th_header, &mut th);
                            }
                        }
                        Err(teehistorian::Error::IoError(err)) => eprintln!("error: {err}"),
                        Err(err) => {
                            // non-parseable header
                            eprintln!("{} Error parsing header: {}", filename, err);
                        }
                    }
                }
                Err(err) => {
                    eprintln!("{} error: {}", filename, err);
                }
            }
        });
}
