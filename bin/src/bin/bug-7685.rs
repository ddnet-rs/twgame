//! It was possible to change teams in solo team and cheat ranks
//!
//! Bug was introduced in https://github.com/ddnet/ddnet/pull/7685
//! Bug was fixed in https://github.com/ddnet/ddnet/pull/7685
//! Commit range:
//! Precondition:
//!  * sv_team 3
//!  * or sv_solo_team 1??
use rusqlite::{params, Connection};
use serde::Deserialize;
use std::collections::{HashMap, HashSet};
use std::fs;
use std::fs::File;
use std::io::BufWriter;
use std::rc::Rc;
use std::str::FromStr;
use teehistorian::{Chunk, ThBufReader, ThCompat, ThStream};
use teehistorian_replayer::ThReplayer;
use twgame::Map;
use twmap::TwMap;
use twsnap::compat::ddnet::{DemoKind, DemoMapHash, DemoWriter};
use uuid::Uuid;
use xdg::BaseDirectories;

fn get_all_paths(conn: &Connection) -> rusqlite::Result<HashMap<u32, String>> {
    let mut stmt = conn.prepare("SELECT id, name FROM path;")?;
    let paths: Result<_, _> = stmt
        .query_map([], |row| Ok((row.get(0)?, row.get(1)?)))?
        .collect();
    paths
}

fn setup_db(conn: &Connection) -> rusqlite::Result<HashMap<[u8; 32], bool>> {
    conn.execute(
        "CREATE TABLE IF NOT EXISTS maps (\
            sha256 BLOB NOT NULL,\
            solo INTEGER NOT NULL\
        ) STRICT;",
        [],
    )?;

    let mut hm = HashMap::new();
    let mut stmt = conn.prepare("SELECT sha256, solo FROM maps").unwrap();
    let mut results = stmt.query([]).unwrap();
    while let Some(row) = results.next().unwrap() {
        let sha256: [u8; 32] = row.get(0).unwrap();
        let solo: i32 = row.get(1).unwrap();
        hm.insert(sha256, solo != 0);
    }

    Ok(hm)
}

fn get_th(filename: &str) -> ThCompat<ThBufReader<File>> {
    let f = File::open(filename).unwrap();
    ThCompat::parse(ThBufReader::new(f)).unwrap()
}

#[derive(Deserialize, Debug)]
struct ThHeader {
    pub config: HashMap<String, String>,
}

fn find_bad_th(mut th: ThCompat<ThBufReader<File>>) -> bool {
    let mut connected: HashSet<i32> = HashSet::new();
    let mut teams: HashMap<i32, i32> = HashMap::new();
    let mut teams_occupied: HashSet<i32> = HashSet::new();
    while let Ok(chunk) = th.next_chunk() {
        match chunk {
            Chunk::Join { cid } => {
                connected.insert(cid);
                teams.insert(cid, cid);
            }
            Chunk::Drop(p) => {
                if connected.contains(&p.cid) {
                    connected.remove(&p.cid);
                    teams_occupied.remove(&teams[&p.cid]);
                    teams.remove(&p.cid);
                }
            }
            Chunk::PlayerTeam { cid, team } => {
                if connected.contains(&cid) {
                    if teams_occupied.contains(&team) {
                        return true;
                    }
                    teams_occupied.remove(&teams[&cid]);
                    teams_occupied.insert(team);
                    teams.insert(cid, team);
                } else {
                    //assert_eq!(t.team, 0);
                }
            }
            _ => {}
        }
    }
    false
}

fn main() {
    // all uuids from ranks during the time
    let potential_uuids: HashSet<Uuid> = fs::read_to_string("game_uuids.txt")
        .unwrap() //.unwrap_or_else(|_| String::new())
        .split_ascii_whitespace()
        .map(|s| Uuid::from_str(s).unwrap())
        .collect();

    // setup directories
    let dirs = BaseDirectories::with_prefix("twgame-replayer").unwrap();
    let db = dirs
        .place_data_file("index.sqlite")
        .expect("failed to create directories");
    println!("Using index at {db:?}");

    let tmp = dirs
        .place_data_file("bug-7685.sqlite")
        .expect("failed to create directories");
    println!("Using bug-7685 at {db:?}");
    let tmp_conn = Connection::open(tmp).unwrap();
    let mut existing = setup_db(&tmp_conn).unwrap();

    let conn = Connection::open(db).unwrap();

    let paths = get_all_paths(&conn).unwrap();

    let mut stmt = conn
        .prepare(
            "SELECT \
                path_id,\
                uuid,\
                datetime(date, 'unixepoch'),\
                map_name,\
                map_sha256,\
                game_info \
            FROM teehistorian \
            WHERE \
                date > strftime('%s', '2023-12-16') AND \
                date < strftime('%s', '2023-12-22')",
        )
        .unwrap();

    let mut results = stmt.query([]).unwrap();
    while let Some(row) = results.next().unwrap() {
        let path_id: u32 = row.get(0).unwrap();
        let uuid: [u8; 16] = row.get(1).unwrap();
        let uuid = Uuid::from_bytes(uuid);
        if potential_uuids.contains(&uuid) {
            continue;
        }
        let path = format!("{}/{}.teehistorian", paths.get(&path_id).unwrap(), uuid);
        let date: String = row.get(2).unwrap();
        let map_name: String = row.get(3).unwrap();
        let map_sha: [u8; 32] = row.get(4).unwrap();
        let sha_hex = hex::encode(map_sha);
        let game_info: Option<String> = row.get(5).unwrap();
        let th = if let Some(solo) = existing.get(&map_sha) {
            if !solo {
                continue;
            }
            get_th(&path)
        } else {
            // find out if it is solo
            let mut th = get_th(&path);
            let header = th.header().unwrap();
            let header = serde_json::from_slice::<ThHeader>(header).unwrap();
            let solo = header.config.get("sv_team") == Some(&"3".to_owned())
                || header.config.get("sv_solo_server") == Some(&"1".to_owned());
            existing.insert(map_sha, solo);
            tmp_conn
                .execute(
                    "INSERT INTO maps (sha256, solo) VALUES (?, ?);",
                    params![map_sha, solo,],
                )
                .unwrap();
            if !solo {
                continue;
            }
            th
        };
        if !find_bad_th(th) {
            continue;
        }
        let load_map_name = format!("ddnet-map-archive/sha256/{}.map", sha_hex);
        println!("Load map: {}", load_map_name);
        let map = fs::read(&load_map_name).unwrap();
        let mut map = TwMap::parse(&map).unwrap();

        let mut th = get_th(&path);
        let demo_out = format!("bug-7685/{}.demo", uuid);
        let to = BufWriter::new(File::create(&demo_out).unwrap());
        let mut demo = DemoWriter::new(
            to,
            DemoKind::Server,
            &date,
            "0.6 TwGame", // TODO
            &map_name,
            None,
            DemoMapHash::Sha256(hex::decode(sha_hex).unwrap().try_into().unwrap()),
            0,
        )
        .unwrap();

        let map = Map::try_from(&mut map).unwrap();
        let map = Rc::new(map);
        let mut world = twgame::DdnetReplayerWorld::new(map, false);

        let header = th.header().unwrap();

        let replayer = ThReplayer::new(header, &mut world);
        replayer.validate(&mut world, &mut th, Some(&mut demo));

        println!(
            "{path} {date} {map_name} {} {game_info:?}",
            hex::encode(map_sha)
        );
    }
}
